<?php
class MY_Loader extends CI_Loader {
    public function template_top_nav($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('top_navigation/top_nav_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('top_navigation/top_nav_footer', $vars, $return);

        return $content;
    else:
        $this->view('top_navigation/top_nav_header', $vars);
        $this->view($template_name, $vars);
        $this->view('top_navigation/top_nav_footer', $vars);
    endif;
    }

    public function template_left_nav($template_name, $vars = array(), $return = FALSE)
    {
        if($return):
        $content  = $this->view('left_navigation/left_nav_header', $vars, $return);
        $content .= $this->view($template_name, $vars, $return);
        $content .= $this->view('left_navigation/left_nav_footer', $vars, $return);
        return $content;
    else:
        $this->view('left_navigation/left_nav_header', $vars);
        $this->view($template_name, $vars);
        $this->view('left_navigation/left_nav_footer', $vars);
    endif;
    }
}
?>