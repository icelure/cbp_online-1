<link rel="stylesheet" href="<?php echo css_url(); ?>plan_style.css">
<div class="container">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Plans
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Plans</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
            <!-- <div class="callout callout-info">
              <h4>Plans</h4>
              <p>Plans page coming soon.</p>
              <p><?php print_r($this->session->userdata('user'));  ?></p>
            </div> -->

            <?php
            $flashdata= $this->session->flashdata('response');
            if(!empty($flashdata)){
              if($flashdata['status'] == 'success'){
                ?>
                <div class="callout callout-success">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
              if($flashdata['status'] == 'failed'){
                ?>
                <div class="callout callout-danger">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
            }
            ?>

            <div class="plan_wrapper">
              <!-- PRICING-TABLE CONTAINER -->
              <div class="pricing-table group">
              <div style="margin-left: 18%;">
                <div class="block professional fl">
                  <h2 class="title"><?php print $plans[0]['title'] ?></h2>
                  <!-- CONTENT -->
                  <div class="plan_content">
                    <p class="price">
                      <sup>$</sup>
                      <span><?php print $plans[0]['price'] ?></span>
                      <sub>/mo.</sub>
                    </p>
                    <p class="hint"><?php print $plans[0]['subtitle'] ?></p>
                  </div>
                  <!-- /CONTENT -->
                  <!-- FEATURES -->
                  <ul class="features">
                    <li><span class="fontawesome-cog"></span><?php print $plans[0]['feature1'] ?></li>
                    <li><span class="fontawesome-star"></span><?php print $plans[0]['feature2'] ?></li>
                    <li><span class="fontawesome-dashboard"></span><?php print $plans[0]['feature3'] ?></li>
                    <li><span class="fontawesome-cloud"></span><?php print $plans[0]['feature4'] ?></li>
                  </ul>
                  <!-- /FEATURES -->
                  <!-- PT-FOOTER -->
                  <a href="<?php echo base_url().'plans/subscribe/monthly'; ?>">
                  <div class="pt-footer">
                    <p>Pay</p>
                  </div></a>
                  <!-- /PT-FOOTER -->
                </div>
                
                <div class="block business fl">
                  <h2 class="title"><?php print $plans[1]['title'] ?></h2>
                  <!-- CONTENT -->
                  <div class="plan_content">
                    <p class="price">
                      <sup>$</sup>
                      <span><?php print $plans[1]['price'] ?></span>
                      <sub>/mo.</sub>
                    </p>
                    <p class="hint"><?php print $plans[1]['subtitle'] ?></p>
                  </div>
                  <!-- /CONTENT -->

                  <!-- FEATURES -->
                  <ul class="features">
                    <li><span class="fontawesome-cog"></span><?php print $plans[1]['feature1'] ?></li>
                    <li><span class="fontawesome-star"></span><?php print $plans[1]['feature2'] ?></li>
                    <li><span class="fontawesome-dashboard"></span><?php print $plans[1]['feature3'] ?></li>
                    <li><span class="fontawesome-cloud"></span><?php print $plans[1]['feature4'] ?></li>
                  </ul>
                  <!-- /FEATURES -->

                  <!-- PT-FOOTER -->
                  <a href="<?php echo base_url().'plans/subscribe/yearly'; ?>">
                  <div class="pt-footer">
                    <p>Pay</p>
                  </div></a>
                  <!-- /PT-FOOTER -->
                </div>
              </div>
                <!-- /BUSINESS -->
              </div>
              <!-- /PRICING-TABLE -->
            </div>
          </section><!-- /.content -->
        </div>