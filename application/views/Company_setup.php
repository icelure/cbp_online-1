<?php
if (isset($company_detail)) {

    extract($company_detail);
};

if (isset($general_assumptions)) {

  extract($general_assumptions);
};

if (isset($import_duty_assumptions)) {

  extract($import_duty_assumptions);
};

if (isset($australian_payroll)){

  extract($australian_payroll);
};

if($financial_year == ""){

  $financial_year="July-Jun";

};

if($currency == "USD"){

  $currency_class = "fa-dollar";

}elseif ($currency == "EUR") {

  $currency_class = "fa-eur";

}elseif ($currency == "INR") {

  $currency_class = "fa-inr";

}else{

  $currency_class = "fa-dollar";

};

?>

<style type="text/css">

.process-step .btn:focus{outline:none}

.process{display:table;width:100%;position:relative}

.process-row{padding-top: 5px}

.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}

.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}

.process-step{display:table-cell;text-align:center;position:relative}

.process-step p{margin-top:4px}

.btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}

/*.tab-content{margin: 0 10% 0 10%;}*/

.error{color:rgba(255, 0, 0, 0.62);}

.input-group{width: 100%}

tr.selected {
  background-color: #B0BED9 !important;
}


.bordered th, .bordered td{
  padding:10px;
}

.bordered tbody tr:nth-child(odd){
  background:#eee;
  color:#000;
}

.bordered tbody tr:nth-child(even){
  color:#fff;
}

/*.nav>li>a {
  padding: 7px 3px;
}*/

.tab-header{
  background-color:#ecf0f5; border: 0px solid blue; padding: 1px;
}

.nav-pills>li{
  margin-right: 5px;
  margin-bottom: -1px;
}

.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
  color: #fff;
  background-color: #fafafa;
}

.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
  border-top-color: #60979c;
  border-bottom-color: transparent;
}

.nav-pills>li>a, .nav-pills>li>a:hover {
  background-color: #d4d7dc;
}

.nav-pills>li>a {
  border-radius: 0;
}

@media (max-width: 920px){

  #tabs {
    display: none;
  }

  .nav-pills>li {
    width: 75px;
    text-align: center;
  }

}

@media (max-width: 525px){

  #tabs {
    display: none;
  }

  .nav-pills>li {
    width: 40px;
    text-align: center;
  }

}

.image_cell {
  width:295px;
  height:246px;
  overflow:hidden;
}
.kv-avatar .file-preview-frame,.kv-avatar .file-preview-frame:hover {
    margin: 0;
    padding: 0;
    border: none;
    box-shadow: none;
    text-align: center;
}
.kv-avatar .file-input {
    display: table-cell;

}

@media (max-width: 770px){

  .form-group.col-md-6.padd{
  	padding-right:0;
  	padding-left:0;
  }

}
</style>



<section class="content-header">
  <h1>Welcome to Complete Buisness Plans<!-- <small>advanced tables</small> --></h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Welcome to Complete Business Plans</a></li>
    <!-- <li class="active">Data tables</li> -->
  </ol>
</section> <!--End content-header -->

<form role="form" method="post" action="<?php echo base_url() ?>company_setup" id="company_setting_form" enctype="multipart/form-data">
<section class="content">
  <div class="tab-header">
    <ul class="process-row nav nav-pills">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-info-sign"></i> <strong id="tabs">About</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-wrench"></i> <strong id="tabs">Setup</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu2" data-toggle="tab"><p style="color:rgba(0,0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-comment"></i> <strong id="tabs">Assumptions</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu5" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-save-file"></i> <strong id="tabs">Summary</strong></p>
        </a>
      </li>
    </ul>
 </div> <!-- End Tab header -->
 <div class="tab-content clearfix">
  <div id="menu0" class="tab-pane fade active in">
    <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
      <div class="box-header with-border">
        <ul class="list-unstyled list-inline pull-right">
          <li>
            <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
          </li>
        </ul>
        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
        <ul class="list-unstyled list-inline pull-right">
        </ul>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
      </div><!-- /.box-header -->
      <div class="box-body">

        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">About CBP</h2>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">CBP is dynamic application that will help you create a simple but effective Business Plan.</p>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">What is a Business Plan ?</h2>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">A Business Plan is a set of strategic documents that you will design to outline your plans for a start-up or current business.These documents are used to describe your business objectives,goals and strategies,as well as to provide a blueprint of your financing and marketing plans. Essentially,it provides information about what steps your will be undertaking to achieve your goals and objectives.</p>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Why create a Business Plan ?</h2>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">It has been said by many experts and financial advisors that a Business Plan is essential to the success of a business.Your business plan should provide a detailed description of your business,including the products or services you plan to provide or sell,as well as expected monthly expenditures and profits..</p>
        <P style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">If you are selling products,then you'll need to include information about the manufacture of those products.A detailed description of the market for your products or services, along with competition comparisons,should be provided as well.You'll also need to include your plans for development,distribution,staffing,funding,and more.</p>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Essential points to include in your Business Plan</h2>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">When preparing your Business Plan,the key points is to focus on researching every aspect of your business.You'll need to include detailed information about everything from the basics of your product or service to how you intend to manage daily operations.If you need financing, you'll need to make your plan both informative and captivating.By doing so, you'll be able to keep loan officers or investors interested long enough to read the bulk of your plan and hopefully they will decide to provide the financing you need based on your detailed research and assumptions.</p>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">A sound a realistic Business Plan is also your company narrative,and it represents the time and effort you have spent learning and understanding the nature of your business, and how you intend to operate and grow your business to success..</p>
        <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports  assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>
      </div><!-- /.box-body -->
      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
          <li>
            <button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button>
          </li>
        </ul>
      </div><!-- /.box-footer -->
    </div>
  </div> <!-- End Menu 0 -->
  <div id="menu1" class="tab-pane fade in">
      <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
          <ul class="list-unstyled list-inline pull-right">
            <li>
              <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
            </li>
          </ul>
          <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Setup your company details</strong></h3>
          <ul class="list-unstyled list-inline pull-right">
          </ul>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;"></h2>
        </div><!-- /.box-header -->
        <div class="box-body">
          <?php //print_r($company_detail);?>
          <div class="box-header with-border">
            <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">Company Info
            </h3>
          </div><!-- /.box-header -->
          <div class="col-md-9">
            <div class="form-group center">
              <div id="kv-avatar-errors-1" class="center-block" style="width:800px;display:none">
              <?php echo $this->session->flashdata('avatar-1');?>
              </div>
                <div class="kv-avatar center-block image_cell" style="width:200px">
                  <input id="company_logo" name="company_logo" type="file" class="file-loading">
                  <br>
                </div>
                <label for="avatar">Company Logo </label> <span style="color:red;">max (120x120) jpg, jpeg, gif and png only.</span>
            </div>
            <div class="form-group">
              <label>Start Date</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" class="form-control" id="start_date" name="start_date" value="<?php print date("m/d/Y", (strtotime($start_date))? strtotime($start_date):strtotime(date(now))) ?>" placeholder="Enter start date">
              </div>
            </div>
            <div class="form-group">
              <label>Company Name</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-pencil"></i>
                </div>
              <input type="text" class="form-control" name="company_name" id="company_name" value="<?php print $company_name; ?>" placeholder="Enter your company name">
              </div>
            </div>
            <div class="form-group">
              <label>ABN No</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-pencil"></i>
                </div>
              <input type="text" class="form-control" name="abn_no" id="abn_no" value="<?php print $abn_no; ?>" placeholder="Enter ABN no">
              </div>
            </div>
            <div class="form-group col-md-6 padd" style="padding-left:0;">
              <label>Street No</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-road"></i>
                  </div>
              <input type="text" name="street_no" id="street_no" class="form-control" value="<?php print $street_no; ?>"  placeholder="Enter street no">
              </div>
            </div>
            <div class="form-group col-md-6 padd" style="padding-right:0;">
              <label>Street Name</label>
              <div class="input-group">
                  <div class="input-group-addon">
                    <i class="fa fa-road"></i>
                  </div>
              <input type="text" name="street_name" id="street_name" class="form-control" value="<?php print $street_name; ?>" placeholder="Enter street name">
              </div>
            </div>
            <div class="form-group">
              <label>Suburb</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-building"></i>
                </div>
                <input type="text" name="suburb" id="suburb" class="form-control" value="<?php print $suburb; ?>" placeholder="Enter suburb">
              </div>
            </div>
            <div class="form-group col-md-6 padd" style="padding-left:0;">
              <label for="country">Country</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-flag"></i>
                </div>
                <select name="country" class="form-control" id="country" onfocus="populateDropDown()">
                <option value="<?php echo $country;?>" selected><?php echo $country;?></option>
                </select>
              </div>
            </div>
            <div class="form-group col-md-6 padd" style="padding-right:0;">
              <label>State/Province</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-map-marker"></i>
                </div>
                  <select name="state" class="form-control" id="state">
                    <option value="<?php echo $state;?>" selected><?php echo $state;?></option>
                  </select>
              </div>
            </div>
            <div class="form-group">
              <label>PostCode/ZipCode</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-map-marker"></i>
                </div>
              <input type="text" name="zipcode" id="zipcode" class="form-control" value="<?php print$zipcode;?>" placeholder="Enter postcode/zipcode">
              </div>
            </div>
            <div class="form-group col-md-6 padd" style="padding-left:0;">
              <label>Telephone</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-phone"></i>
                </div>
                <input type="text" name="telephone" id="telephone" class="form-control" value="<?php print $telephone; ?>"  data-inputmask='"mask": "(999) 999-9999"' data-mask>
              </div><!-- /.input group -->
            </div>
            <div class="form-group col-md-6 padd" style="padding-right:0;">
              <label>Fax</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-fax"></i>
                </div>
                <input type="text" name="fax" id="fax" class="form-control" value="<?php print $fax; ?>" placeholder="Enter Fax" data-inputmask='"mask": "(999) 999-9999"' data-mask>
              </div>
            </div>
            <div class="form-group">
              <label>Email</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-envelope"></i>
                </div>
                <input type="text" name="company_email" id="company_email" class="form-control" value="<?php print $company_email; ?>" placeholder="Enter company email">
              </div>
            </div>
            <div class="form-group">
              <label>Website</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-fax"></i>
                </div>
                <input type="text" name="website" id="website" class="form-control" value="<?php print $website; ?>" placeholder="Enter company website">
              </div>
            </div>
          </div> <!-- /.col 9 -->
        </div><!-- /.box-body -->
        <div class="box-body">
          <div class="box-header with-border">
            <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">Currency Structure/Financial system </h3>
          </div>
          <div class="col-md-9">
            <div class="form-group col-md-6 padd" style="padding-left:0;">
              <label>Currency</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-money"></i>
                </div>
              <select class="form-control" name="currency" id="currency">
                <option <?php ($currency == 'AUD') ? print "selected" : print "" ?>>AUD</option>
                <option <?php ($currency == 'USD') ? print "selected" : print "" ?>>USD</option>
                <option <?php ($currency == 'INR') ? print "selected" : print ""; ?>>INR</option>
                <option <?php ($currency == 'EUR') ? print "selected" : print "" ?>>EUR</option>
              </select>
              </div>
            </div>
            <div class="form-group col-md-6 padd" style="padding-right:0;">
              <label>Financial Year</label>
              <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name="financial_year" id="financial_year" value="<?php print $financial_year; ?>" class="form-control" readonly>
              </div>
            </div>
          </div> <!-- /.col 9 -->
        </div><!-- /.box-body -->
        <div class="box-body">
          <div class="box-header with-border">
            <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">Directors</h3>
          </div>
          <div class="col-md-9">
              <?php for ($i=0; $i<4; $i++) { ?>
              <div class="form-group col-md-6">
                <div id="kv-avatar-errors-1" class="center-block" style="width:800px;display:none">
                <?php echo $this->session->flashdata('avatar-1');?>
                </div>
                <div class="kv-avatar center-block  image_cell" style="width:200px">
                  <input id="director_logo_<?php echo $i;?>" name="director_logo_<?php echo $i;?>" type="file" class="file-loading">
                  <br>
                </div>
                <label>Director Name</label>
                <input type="text" name="director_name_<?php echo $i;?>" value="<?php echo $directors[$i]['name'] ?>" class="form-control" placeholder="Enter director name">
                <input type="hidden" name="director_id_<?php echo $i;?>" value="<?php echo $directors[$i]['id'] ?>" class="form-control" placeholder="Enter director name">
              </div>
              <?php if ($i % 2 && $i > 0){ ?>
                <div class="clearfix"></div>
              <?php } ?>
              <?php } ?>
          </div>
        </div><!-- /.box-body -->
        <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
          <li>
            <button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button>
          </li>
        </ul>
      </div><!-- /.box-footer -->
    </div>
  </div> <!-- /.Menu 1 -->
  <div id="menu2" class="tab-pane fade in">
    <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid blue; padding: 0px;">
      <div class="box-header with-border">
        <ul class="list-unstyled list-inline pull-right">
          <li>
            <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
          </li>
        </ul>
        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Enter General Business Assumptions</strong></h3>
        <ul class="list-unstyled list-inline pull-right">
        </ul>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;"></h2>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="form-group col-md-6">
          <label>Company Tax</label>
          <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-percent"></i>
                </div>
          <input type="number" class="form-control" name="company_tax" id="company_tax" value="<?php print $company_tax; ?>" placeholder="Enter Tax">
          </div>
        </div>
        <div class="form-group col-md-6">
          <label>GST/VAT</label>
          <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-percent"></i>
                </div>
          <input type="number" class="form-control" name="company_vat" id="company_vat" value="<?php print $company_vat; ?>" placeholder="GST/VAT">
          </div>
        </div>
        </div><!-- /.box-body -->
        <div class="box-body">
        <div class="box-header with-border">
          <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">General Trading & Terms</h3>
        </div><!-- /.box-header -->
        <div class="form-group col-md-6">
          <label>Receivables In Days</label>
          <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
          <select class="form-control" name="rsvbl_in_days">
            <option value="0" <?php ($rsvbl_in_days == "0") ? print "selected"  : print "" ?>>0</option>
            <option value="30" <?php ($rsvbl_in_days == "30") ? print "selected" :  print "" ?>>30</option>
            <option value="60" <?php ($rsvbl_in_days == "60") ? print "selected" :  print "" ?>>60</option>
            <option value="90" <?php ($rsvbl_in_days == "90") ? print "selected" :  print "" ?>>90</option>
            <option value="120" <?php ($rsvbl_in_days == "120") ? print "selected" :  print "" ?>>120</option>
          </select>
          </div>
        </div>
        <div class="form-group col-md-6">
          <label>Payables In Days</label>
          <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
          <select class="form-control" name="pybl_in_days">
           <option value="0" <?php ($pybl_in_days == "0") ? print "selected" :  print "" ?>>0</option>
           <option value="30" <?php ($pybl_in_days == "30") ? print "selected" :  print "" ?>>30</option>
           <option value="60" <?php ($pybl_in_days == "60") ? print "selected" :  print "" ?>>60</option>
           <option value="90" <?php ($pybl_in_days == "90") ? print "selected" :  print "" ?>>90</option>
           <option value="120" <?php ($pybl_in_days == "120") ? print "selected" :  print "" ?>>120</option>
          </select>
          </div>
        </div>
        <div class="form-group col-md-6">
          <label>GST/VAT Payable Paid in Days</label>
          <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
          <select class="form-control" name="vat_paid_in_days">
            <option value="0" <?php ($vat_paid_in_days == "0") ? print "selected" :  print "" ?>>0</option>
            <option value="30" <?php ($vat_paid_in_days == "30") ? print "selected" :  print "" ?>>30</option>
            <option value="60" <?php ($vat_paid_in_days == "60") ? print "selected" :  print "" ?>>60</option>
            <option value="90" <?php ($vat_paid_in_days == "90") ? print "selected" :  print "" ?>>90</option>
            <option value="120" <?php ($vat_paid_in_days == "120") ? print "selected" :  print "" ?>>120</option>
          </select>
          </div>
        </div>
        <div class="form-group col-md-6">
          <label>Company Tax Paid In Days</label>
          <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
          <select class="form-control" name="cmpnytx_paid_in_days">
            <option value="0" <?php ($cmpnytx_paid_in_days == "0") ? print "selected" :  print "" ?>>0</option>
            <option value="30" <?php ($cmpnytx_paid_in_days == "30") ? print "selected" :  print "" ?>>30</option>
            <option value="60" <?php ($cmpnytx_paid_in_days == "60") ? print "selected" :  print "" ?>>60</option>
            <option value="90" <?php ($cmpnytx_paid_in_days == "90") ? print "selected" :  print "" ?>>90</option>
            <option value="120" <?php ($cmpnytx_paid_in_days == "120") ? print "selected" :  print "" ?>>120</option>
          </select>
          </div>
        </div>
        </div><!-- /.box-body -->
        <div class="box-body">
        <div class="box-header with-border">
          <h3 class="box-title"style="color:#3c8dbc;font-size:25px;font-weight:bold;">Plant Property And Equipment </h3>
        </div><!-- /.box-header -->
        <div class="form-group col-md-6">
          <label>Depreciation On Equipment</label>
          <div class="input-group">
                <div class="input-group-addon">
                  <i class="fa fa-percent"></i>
                </div>
          <input type="number" class="form-control" name="depreciation_on_equipment" id="depreciation_on_equipment" value="<?php print $depreciation_on_equipment; ?>" placeholder="Enter Depreciation On Equipment">
          </div>
        </div>
      </div><!-- /.box-body -->
      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
         <li>
          <button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button>
         </li>
         <li>
          <button type="button" class="btn btn-info next-step" id="general_assumption_section">Next <i class="fa fa-chevron-right"></i></button>
         </li>
       </ul>
      </div><!-- /.footer -->
    </div>
  </div> <!-- /.Menu 2 -->
  <div id="menu5" class="tab-pane fade in" >
    <div class="box box-solid"style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid blue; padding: 0px;">
      <div class="box-header with-border">
        <ul class="list-unstyled list-inline pull-right">
          <li>
            <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
          </li>
        </ul>
        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Have you checked your entries ?</h3>
        <ul class="list-unstyled list-inline pull-right">
        </ul>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;"></h2>
      </div><!-- /.box-header -->
      <div class="box-body">
        <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Now that you have completed setting up your comapany details , you will need go to the Startup Module to evaluate your projected start up costs,and arrange your finances to start your business venture.</p>
        <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Click on  Done to continue.</p>
      </div><!-- /.box-body -->
      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
          <li>
            <input type="hidden" name="submtForm" value="cmpnySettings" >
            <input type="hidden" name="company_id" value="<?php print $id; ?>">
            <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button>
          </li>
        </ul>
      </div>
    </div>
  </div><!-- End #menu5 -->
 </div> <!-- End Tab Content -->
</section> <!--End Content -->
</form>

<script type="text/javascript" charset="utf-8">

function populateDropDown(){
  populateCountries("country", "state");
}

<?php if (isset($company_detail['id'])) { ?>

function restoreDefault() {
    $.ajax({
        type: 'POST',
        url: 'company_setup/restoreDefault/<?php echo $company_detail['id'];?>',
        success: function(){
            window.location.href = '<?php echo site_url('company_setup')?>';
        },
        error: function (jqXHR, textStatus, errorThrown) {

            showResultFailed(jqXHR.responseText);
            hideWaitingFail();
        }
    })
}


var btnReset = '<button type="button" class="btn btn-default" title="Restore default" ' +
        'onclick="restoreDefault()">' +
        '<i class="fa fa-trash"></i>' +
        '</button>';
<?php } ?>



function restoreDefaultDirector_0() {
    $.ajax({
        type: 'POST',
        url: 'company_setup/restoreDefaultDirector/<?php echo $directors[0]['id'];?>',
        success: function(){
            window.location.href = '<?php echo site_url('company_setup')?>';
        },
        error: function (jqXHR, textStatus, errorThrown) {

            showResultFailed(jqXHR.responseText);
            hideWaitingFail();
        }
    })
}

function restoreDefaultDirector_1() {
    $.ajax({
        type: 'POST',
        url: 'company_setup/restoreDefaultDirector/<?php echo $directors[1]['id'];?>',
        success: function(){
            window.location.href = '<?php echo site_url('company_setup')?>';
        },
        error: function (jqXHR, textStatus, errorThrown) {

            showResultFailed(jqXHR.responseText);
            hideWaitingFail();
        }
    })
}
function restoreDefaultDirector_2() {
    $.ajax({
        type: 'POST',
        url: 'company_setup/restoreDefaultDirector/<?php echo $directors[2]['id'];?>',
        success: function(){
            window.location.href = '<?php echo site_url('company_setup')?>';
        },
        error: function (jqXHR, textStatus, errorThrown) {

            showResultFailed(jqXHR.responseText);
            hideWaitingFail();
        }
    })
}
function restoreDefaultDirector_3() {
    $.ajax({
        type: 'POST',
        url: 'company_setup/restoreDefaultDirector/<?php echo $directors[3]['id'];?>',
        success: function(){
            window.location.href = '<?php echo site_url('company_setup')?>';
        },
        error: function (jqXHR, textStatus, errorThrown) {

            showResultFailed(jqXHR.responseText);
            hideWaitingFail();
        }
    })
}
var btnResetDirector_0 = '<button type="button" class="btn btn-default" title="Restore default" ' +
        'onclick="restoreDefaultDirector_0()">' +
        '<i class="fa fa-trash"></i>' +
        '</button>';
var btnResetDirector_1 = '<button type="button" class="btn btn-default" title="Restore default" ' +
      'onclick="restoreDefaultDirector_1()">' +
      '<i class="fa fa-trash"></i>' +
      '</button>';
var btnResetDirector_2 = '<button type="button" class="btn btn-default" title="Restore default" ' +
        'onclick="restoreDefaultDirector_2()">' +
        '<i class="fa fa-trash"></i>' +
        '</button>';
var btnResetDirector_3 = '<button type="button" class="btn btn-default" title="Restore default" ' +
        'onclick="restoreDefaultDirector_3()">' +
        '<i class="fa fa-trash"></i>' +
        '</button>';



$("#company_logo").fileinput({
    overwriteInitial: true,
    maxFileSize: 500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img class="img-circle" src="<?php echo ($company_detail['company_logo'])? base_url().'assets/uploads/company_logo/'.$company_detail['company_logo']:base_url().'assets/img/cbpLogoSmall_3x.png'; ?>" alt="Company Logo" style="width:130px;">',
    layoutTemplates: {main2: '{preview}  {remove} {browse} '<?php if ($company_detail['company_logo']) { ?> +  btnReset <?php } ?>} ,
    allowedFileExtensions: ["jpg", "png", "gif"]
});
$("#director_logo_0").fileinput({
    overwriteInitial: true,
    maxFileSize: 500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img class="img-circle" src="<?php echo ($directors[0]['image']!="")? base_url().'assets/uploads/director_logo/'.$directors[0]['image']:base_url().'assets/img/cbpLogoSmall_3x.png'; ?>" alt="Director Logo" style="width:130px;">',
    layoutTemplates: {main2: '{preview}  {remove} {browse} '<?php if ($directors[0]['image']) { ?> +  btnResetDirector_0 <?php } ?>} ,
    allowedFileExtensions: ["jpg", "png", "gif"]
});
$("#director_logo_1").fileinput({
    overwriteInitial: true,
    maxFileSize: 500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img class="img-circle" src="<?php echo ($directors[1]['image']!="")? base_url().'assets/uploads/director_logo/'.$directors[1]['image']:base_url().'assets/img/cbpLogoSmall_3x.png'; ?>" alt="Director Logo" style="width:130px;">',
    layoutTemplates: {main2: '{preview}  {remove} {browse} '<?php if ($directors[1]['image']) { ?> +  btnResetDirector_1 <?php } ?>} ,
    allowedFileExtensions: ["jpg", "png", "gif"]
});
$("#director_logo_2").fileinput({
    overwriteInitial: true,
    maxFileSize: 500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img class="img-circle" src="<?php echo ($directors[2]['image']!="")? base_url().'assets/uploads/director_logo/'.$directors[2]['image']:base_url().'assets/img/cbpLogoSmall_3x.png'; ?>" alt="Director Logo" style="width:130px;">',
    layoutTemplates: {main2: '{preview}  {remove} {browse} '<?php if ($directors[2]['image']) { ?> +  btnResetDirector_2 <?php } ?>} ,
    allowedFileExtensions: ["jpg", "png", "gif"]
});
$("#director_logo_3").fileinput({
    overwriteInitial: true,
    maxFileSize: 500,
    showClose: false,
    showCaption: false,
    browseLabel: '',
    removeLabel: '',
    browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
    removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
    removeTitle: 'Cancel or reset changes',
    elErrorContainer: '#kv-avatar-errors-1',
    msgErrorClass: 'alert alert-block alert-danger',
    defaultPreviewContent: '<img class="img-circle" src="<?php echo ($directors[3]['image']!="")? base_url().'assets/uploads/director_logo/'.$directors[3]['image']:base_url().'assets/img/cbpLogoSmall_3x.png'; ?>" alt="Director Logo" style="width:130px;">',
    layoutTemplates: {main2: '{preview}  {remove} {browse} '<?php if ($directors[3]['image']) { ?> +  btnResetDirector_3 <?php } ?>} ,
    allowedFileExtensions: ["jpg", "png", "gif"]
});

$('#start_date').daterangepicker({singleDatePicker: true});
$("[data-mask]").inputmask();

var currency_class= "fa-dollar";

$(function(){
  $("#currency").change(function(event) {
    curr = $(this).val();
    if(curr == "AUD"){

      currency_class = "fa-dollar";
      $("#financial_year").val("July-Jun");

    }else if(curr == "USD"){

      currency_class = "fa-dollar";
      $("#financial_year").val("Jan-Dec");

    }else if(curr == "INR"){

      currency_class = "fa-inr";
      $("#financial_year").val("Apr-Mar");

    }else if(curr == "EUR"){

      currency_class = "fa-eur";
      $("#financial_year").val("Apr-Mar");

    }
    $(".curr").removeClass("fa-dollar");

    $(".curr").removeClass("fa-inr");

    $(".curr").removeClass("fa-eur");

    $(".curr").addClass(currency_class);
  });
});

$("#company_setting_form").validate({

  rules:{
    company_name : "required",
    abn_no : "required",
    start_date : "required",
    street_no : "required",
    street_name : "required",
    suburb : "required",
    state : "required",
    zipcode : "required",
    country : "required",
    fax:{
      required : true
    },
    company_email:{
      required : true,
      email : true
    },
    website:{
      required : true,
      url : true
    },
    company_tax: {
      required: true
    },
    company_vat: {
      required: true
    },
    depreciation_on_equipment:{
      required: true
    }
  }
});

$('.next-step, .prev-step').on('click', function (e){

    section_id = $(this).attr('id');
    if(section_id=='comapny_detail_section'){
      $("label.error").remove();
      abn_valid = $("#abn_no").valid();
      cn_valid = $("#company_name").valid();
      sn_valid = $("#street_name").val();
      sno_valid = $("#street_no").valid();
      stdate_valid = $("#start_date").valid();
      sub_valid = $("#suburb").valid();
      state_valid = $("#state").valid();
      zp_valid = $("#zipcode").valid();
      cntr_valid = $("#country").valid();
      tph_valid = $("#telephone").valid();
      fx_valid = $("#fax").valid();
      cmp_valid = $("#company_email").valid();
      wb_valid = $("#website").valid();

      if(abn_valid && sn_valid && sno_valid && stdate_valid && sub_valid && state_valid && zp_valid && cntr_valid && tph_valid && fx_valid && cmp_valid && wb_valid && cn_valid){
      }else{
        console.log("Menu1");
        return false;
      }
    }
    if(section_id=='general_assumption_section'){
      $("label.error").remove();
      ctx_valid = $("#company_tax").valid();
      cvt_valid = $("#company_vat").valid();
      /*sii_valid = $("#sales_income_increase").valid();
      sri_valid = $("#services_income").valid();
      slc_valid = $("#sales_cost_increase").valid();
      sci_valid = $("#service_cost_increase").valid();
      mi_valid = $("#marketing_increase").valid();
      pr_valid = $("#public_reactions").valid();
      ac_valid = $("#administration_cost").valid();*/
      doe_valid = $("#depreciation_on_equipment").valid();
      if(ctx_valid && cvt_valid &&  doe_valid){
      }else{
        console.log("Menu2");
        return false;
      }
    }

   var $activeTab = $('.tab-pane.active');
   $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');

   if ( $(e.target).hasClass('next-step') ){

      var nextTab = $activeTab.next('.tab-pane').attr('id');
      $('[href="#'+ nextTab +'"]').addClass('btn-info').removeClass('btn-default');
      $('[href="#'+ nextTab +'"]').tab('show');
      $("body").scrollTop(0);
    }else{

      var prevTab = $activeTab.prev('.tab-pane').attr('id');
      $('[href="#'+ prevTab +'"]').addClass('btn-info').removeClass('btn-default');
      $('[href="#'+ prevTab +'"]').tab('show');
      $("body").scrollTop(0);
    }

  });
</script>
