
       
       
        <div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1>
              Login
              <small>@ CBP Online</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Log in</li>
            </ol>
          </section>

          <!-- Main content -->
        <section class="content">
            <div class="table-responsive">
                <div class="col-md-12">
                     <div class="box box-primary"style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid white; padding: 0px;">
                        <div class="box-header with-border">
                        <div class="login-box-msg text-red">
                        <?php echo validation_errors();; ?>
                    </div>




            <?php
            $errors = validation_errors();
            if($errors !== ""){ ?>
            <div class="callout callout-danger">
              <?php echo $errors; ?>
            </div>

            <?php } ?>

            <?php
            $flashdata= $this->session->flashdata('response');
            if(!empty($flashdata)){
              if($flashdata['status'] == 'success'){
                ?>
                <div class="callout callout-success">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
              if($flashdata['status'] == 'failed'){
                ?>
                <div class="callout callout-danger">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
            }
            ?>

            <div class="login-box" >
              <div class="login-logo">
              <a href="javascript:void(0);"><b>Login to your account</a>
              </div><!-- /.login-logo -->
              <div class="login-box-body" id="login_cont">
                <p class="login-box-msg">Sign in to start your session</p>
                <form action="<?php echo base_url() ?>login" method="post">
                  <div class="form-group has-feedback">
                    <input type="email" name="email" id="email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="row">
                    <div class="col-xs-6">
                      <div class="checkbox icheck">
                        <label>
                          <input type="checkbox" name="remember_me"> Remember Me
                        </label>
                      </div>
                    </div><!-- /.col -->
                    <div class="col-xs-4">
                      <input type="hidden" name="loginForm" value="postForm">
                      <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                    </div><!-- /.col -->
                  </div>
                </form>

                <a href="javascript:void(0);" id="fp_link">I forgot my password</a><br>










              </div><!-- /.login-box-body -->


              <div class="login-box-body" id="fp_cont" style="display:none">
                <p class="login-box-msg">Forgot your password?</p>
                <form action="<?php echo base_url() ?>login" method="post">
                  <div class="form-group has-feedback">
                    <input type="email" name="fp_email" id="fp_email" class="form-control" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                  </div>
                  <div class="row">
                    <div class="col-xs-4">
                      <input type="hidden" name="fpForm" value="postfpForm">
                      <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    </div><!-- /.col -->
                  </div>
                </form>
                <a href="javascript:void(0);" id="lg_link">Login to your account?</a><br>
              </div>















            </div><!-- /.login-box -->
          </section><!-- /.content -->
        </div><!-- /.container -->
        <script type="text/javascript">
        $(document).ready(function() {
          $("#fp_link").click(function(event) {
            $("#login_cont").hide('slow', function() {
             
            }); 
            $("#fp_cont").show('slow'); 
          });

          $("#lg_link").click(function(event) {
            $("#fp_cont").hide('slow', function() {
             
            });
             $("#login_cont").show('slow');
          });
        });
        </script>