<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datatables/dataTables.bootstrap.css">
<link rel="stylesheet" href="<?php echo css_url(); ?>bootstrap-switch.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
<style>
.pagination>.disabled>a, .pagination>.disabled>a:focus, .pagination>.disabled>a:hover, .pagination>.disabled>span, .pagination>.disabled>span:focus, .pagination>.disabled>span:hover {
    color: #777;
    cursor: not-allowed;
    background-color: #fff;
    border: 1px solid #ddd;
	padding: 6px !important;
}
.table_foot
{
    font-weight: bold;
    text-align: left;
}
</style>
<div class="col-md-12" id="expense_table-responsive">
        <button style=" margin-left: 11px; margin-top: 17px; " class="btn btn-success" onclick="add_expenses()"><i class="glyphicon glyphicon-plus"></i> <strong id="tabs"> Expense</strong></button>
        <br />
        <br />
        <table id="table" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>Expense ID</th>
                <th>Description</th>
                <th>Weekly Cost</th>
                <th>Monthly Cost</th>
                <th>Quarterly Cost</th>
                <th>Yearly Cost</th>
                <th>Actions</th>
              </tr>
            </thead>
              <tbody>
              <?php if(!empty($list)) {

                  foreach ($list as $expenses) { ?>
                      <tr>
                          <td><?php echo $expenses['id']; ?></td>
                          <td><?php echo $expenses['description']; ?></td>
                          <td>$<?php echo $expenses['weekly_cost']; ?></td>
                          <td>$<?php echo $expenses['monthly_cost']; ?></td>
                          <td>$<?php echo $expenses['quarterly_cost']; ?></td>
                          <td>$<?php echo $expenses['yearly_cost']; ?></td>
                          <td></td>
                      </tr>
                  <?php }
              }
              ?>

            </tbody>
            <tfoot class="table_foot">
                <tr>
                  <td></td>
                  <td style="text-align:right;">Total</td>
                  <td id="week_total">$<?php echo $total[0]['weekly_cost']; ?></td>
                  <td id="month_total">$<?php echo $total[0]['monthly_cost']; ?></td>
                  <td id="quat_total">$<?php echo $total[0]['quarterly_cost']; ?></td>
                  <td id="year_total">$<?php echo $total[0]['yearly_cost']; ?></td>
                  <td></td>
                </tr>
            </tfoot>
                
            </table>
	</div>
	<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Expenses Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-9">
                                <input name="description" placeholder="Expenses Description" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Weekly Cost</label>
                            <div class="col-md-9">
                                <input name="weekly_cost" id="weekly_cost" placeholder="Weekly Cost" onBlur="calculateCostFromWeekly()" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Monthly Cost</label>
                            <div class="col-md-9">
                                <input name="monthly_cost" id="monthly_cost" placeholder="Monthly Cost" onBlur="calculateCostFromMonthly()" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Quarterly Cost</label>
                            <div class="col-md-9">
                                <input name="quarterly_cost" id="quarterly_cost" placeholder="Quarterly Cost" onBlur="calculateCostFromQuaterly()" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Yearly Cost</label>
                            <div class="col-md-9">
                                <input name="yearly_cost" id="yearly_cost" placeholder="Yearly Cost" onBlur="calculateCostFromYearly()" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Purpose</label>
                            <div class="col-md-9">
                                <select name="purpose" class="form-control">
                                  <option value="Marketing">Marketing</option>
                                  <option value="Public-Relations">Public Relations</option>
                                  <option value="Administration">Administration</option>
                                  <option value="Other">Other</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="<?php echo  asset_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo  asset_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
  <script src="<?php echo  js_url(); ?>bootstrap-switch.min.js"></script>
  <script>
    var save_method; //for save method string
    $(function () {
        //datatables
        
      table = $('#table').DataTable({



                // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo site_url('expenses/ajax_list')?>",
                "type": "POST"
            },
             "aoColumnDefs": [
             {
                  "aTargets": [6],
                  "mData": null,
                  "mRender": function (data, type, full) {
                      return '<a id="edit" style=" margin-top: 17px;margin-right: 8px; " class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_expense('+ full[0] +')"><i class="glyphicon glyphicon-pencil"></i></a><a id="delete" style=" margin-top: 17px; " class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_expense('+ full[0] +')"><i class="glyphicon glyphicon-trash"></i></a>';
                  }
              }
              ]

        });
        $('#table tbody').on( 'click', 'tr', function () {
           // $(this).find('td').eq(0).text();
            //alert( 'Row index: '+table.row( this ).data());
           // var id = table.row( this ).id();
          //  alert( $(this).find('td:first').text() );
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                table.$('tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }
            $('#edit').attr('data-info', $(this).find('td:first').text());
            $('#delete').attr('data-info', $(this).find('td:first').text());
           // $('#edit').data('sample_name',$(this).find('td:first').text());
           // $('#delete').data('sample_name',$(this).find('td:first').text());


        } );

        //$('#table').DataTable();
    });

    
    
    function reload_table()
    {
        table.ajax.reload(function ( json ) {
			   reload_graph();
			}); //reload datatable ajax
    }
    //add Expenses show model

    function reload_graph(){

		 url = "<?php echo base_url(); ?>expenses/ajax_expense_summary";

    	$.ajax({
            url : url,
            type: "POST",
            success: function(data)
            {
            	var summary_data = JSON.parse(data);
            	summary = summary_data.expense_summary;
            	var html="";
				var total_y1=0;
				var total_y2=0;
				var total_y3=0;
            	
            	for( var i=0;i<summary.length; i++){
            		var purpose = summary[i]['purpose'];
					var cost_increase = get_cost_increase_perc(summary_data.cost_increase_percentage,summary[i]);
            		html+="<tr id="+summary[i]['purpose']+" onclick='update_graph(\""+purpose+"\")'><td>Total  "+summary[i]['purpose']+"</td><td>$"+summary[i]['weekly_cost']+"</td><td>$"+summary[i]['monthly_cost']+"</td><td>$"+summary[i]['quarterly_cost']+"</td>";
                              
                    var year1= parseInt(summary[i]['yearly_cost']);
                    var year2 = parseInt(year1)*cost_increase + parseInt(year1);
                    var year3 = parseInt(year2)*cost_increase + parseInt(year2);
					year1.toFixed(2);
					year2.toFixed(2);
					year3.toFixed(2);
					total_y1 = total_y1+year1;
					total_y2 = total_y2+year2;
					total_y3 = total_y3+year3;
                              
	                html+=" <td id='"+summary[i]['purpose']+"_year1' data-val='"+year1+"'>$"+year1+" </td>";
	                html+=" <td id='"+summary[i]['purpose']+"_year2' data-val='"+year2+"'>$"+year2+" </td>";
	                html+=" <td id='"+summary[i]['purpose']+"_year3' data-val='"+year3+"'>$"+year3+" </td>";
                    html+="<td></td></tr>";
            	}
                
                html+='<tr><td></td><td></td><td></td><td><b>Total:</b></td><td id="total_y1" data-val="'+total_y1.toFixed(2)+'"><b>$'+total_y1.toFixed(2)+'</b></td><td id="total_y2" data-val="'+total_y2.toFixed(2)+'"><b>$'+total_y2.toFixed(2)+'</b></td><td id="total_y3" data-val="'+total_y3.toFixed(2)+'"><b>$'+total_y3.toFixed(2)+'</b></td></tr>';
            	$("#table2 tbody").empty();
            	$("#table2 tbody").append(html);
                var total = summary_data.total;
                $("#week_total").text('$'+total['weekly_cost']);
                $("#month_total").text('$'+total['monthly_cost']);
                $("#quat_total").text('$'+total['quarterly_cost']);
                $("#year_total").text('$'+total['yearly_cost']);
				update_graph('loadchart');

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                
            }
        });

    }
	
	function get_cost_increase_perc(perc_array,obj){
		var cost = 1;
		if(obj['purpose']=='Marketing')
			cost = perc_array[0]['marketing'];
		if(obj['purpose']=='Administration')
			cost = perc_array[0]['ac'];
		if(obj['purpose']=='Public-Relations')
			cost = perc_array[0]['pr'];
		if(obj['purpose']=='Other')
			cost = perc_array[0]['other'];
		
		return cost/100;
	}

    function add_expenses()
    {
        save_method = 'add';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string
        $('#modal_form').modal('show'); // show bootstrap modal
        $('.modal-title').text('Add Expense'); // Set Title to Bootstrap modal title
    }
    // add expenses to database through ajax call

    function save()
    {
        $('#btnSave').text('saving...'); //change button text
        $('#btnSave').attr('disabled',true); //set button disable
        var url;

        if(save_method == 'add') {
            url = "<?php echo base_url(); ?>expenses/ajax_add";
        } else {
            url = "<?php echo base_url(); ?>expenses/ajax_update";
        }

        // ajax adding data to database
        $.ajax({
            url : url,
            type: "POST",
            data: $('#form').serialize(),
            dataType: "JSON",
            success: function(data)
            {

                if(data.status) //if success close modal and reload ajax table
                {
                    $('#modal_form').modal('hide');
                    reload_table();
                }

                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable


            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error adding / update data');
                $('#btnSave').text('save'); //change button text
                $('#btnSave').attr('disabled',false); //set button enable

            }
        });
    }

    function edit_expense(e_id)
    {
        var id=e_id;
        save_method = 'update';
        $('#form')[0].reset(); // reset form on modals
        $('.form-group').removeClass('has-error'); // clear error class
        $('.help-block').empty(); // clear error string

        //Ajax Load data from ajax
        $.ajax({
            url : "<?php echo base_url(); ?>expenses/ajax_edit/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                $('[name="id"]').val(data.id);
                $('[name="description"]').val(data.description);
                $('[name="weekly_cost"]').val(data.weekly_cost);
                $('[name="monthly_cost"]').val(data.monthly_cost);
                $('[name="quarterly_cost"]').val(data.quarterly_cost);
                $('[name="yearly_cost"]').val(data.yearly_cost);
                $('[name="purpose"]').val(data.purpose);
                $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                $('.modal-title').text('Edit Expense'); // Set title to Bootstrap modal title

            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error get data from ajax');
            }
        });
    }
    $('.navigateTest').click(function(){
        alert("test called");
        var serviceID = this.id;
        alert("serviceID :: " + serviceID);
    });

    function delete_expense(e_id)
    {
        if(confirm('Are you sure delete this data?'))
        {
            var id=e_id;
            // ajax delete data to database
            $.ajax({
                url : "<?php echo base_url(); ?>expenses/ajax_delete/"+id,
                type: "POST",
                dataType: "JSON",
                success: function(data)
                {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
                    alert('Error deleting data');
                }
            });

        }
    }

    function calculateCostFromYearly(){
    	var yearly = parseInt($("#yearly_cost").val());
        if(!isNaN(yearly))
        {
            $("#quarterly_cost").val(Math.floor(yearly/4).toFixed(2));
            $("#weekly_cost").val(Math.floor(yearly/52).toFixed(2));
            $("#monthly_cost").val(Math.floor(yearly/12).toFixed(2));    
        }
    	

    }

    function calculateCostFromQuaterly(){
        var quarterly = parseInt($("#quarterly_cost").val());
        if(!isNaN(quarterly))
        {
            $("#yearly_cost").val(Math.floor(quarterly*4).toFixed(2));
            $("#weekly_cost").val(Math.floor(quarterly/13).toFixed(2));
            $("#monthly_cost").val(Math.floor(quarterly/3).toFixed(2));    
        }
        

    }

    function calculateCostFromWeekly(){
        var weekly = parseInt($("#weekly_cost").val());
        if(!isNaN(weekly))
        {
            $("#yearly_cost").val(Math.floor(weekly*52).toFixed(2));
            $("#quarterly_cost").val(Math.floor(weekly*13).toFixed(2));
            $("#monthly_cost").val(Math.floor(weekly*4).toFixed(2));    
        }
        

    }
    function calculateCostFromMonthly(){
        var monthly_cost = parseInt($("#monthly_cost").val());
        if(!isNaN(monthly_cost))
        {
            $("#yearly_cost").val(Math.floor(monthly_cost*12).toFixed(2));
            $("#weekly_cost").val(Math.floor(monthly_cost/4).toFixed(2));
            $("#quarterly_cost").val(Math.floor(monthly_cost*3).toFixed(2));    
        }
        

    }        
    
  </script>