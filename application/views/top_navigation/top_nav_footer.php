</div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="container" style="text-align: center;">
          <strong>Copyright &copy; 2016-2017 <a href="<?php echo base_url(); ?>">CBP Online</a>.</strong> All rights reserved.
        </div><!-- /.container -->
      </footer>
    </div><!-- ./wrapper -->

    <!-- SlimScroll -->
    <script src="<?=asset_url()?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=asset_url()?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=asset_url()?>dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=asset_url()?>dist/js/demo.js"></script>
    
    <script src="<?=asset_url()?>plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>