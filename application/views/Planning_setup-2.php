<?php

$mission=$fxetch['t_mission'];
$vission=$fxetch['t_vission'];
$cbackground=$fxetch['t_cbackground'];
$ownership=$fxetch['t_cownership'];
$goal=$fxetch['t_goals'];
$propertyplan=$fxetch['t_propertyplan'];
$product=$fxetch['t_product'];
$services=$fxetch['t_services'];
$market=$fxetch['t_market'];
$potentiona=$fxetch['t_potentional'];
$strengts=$fxetch['t_strengts'];
$weakness=$fxetch['t_weakness'];
$daily=$fxetch['t_daily'];
$facilities=$fxetch['t_facilities'];
$keypersonal=$fxetch['t_keypersonal'];
$execuitve=$fxetch['t_execuitve'];
?>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/all.css">
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo asset_url(); ?>css/model.css">
<script src="<?php echo asset_url(); ?>js/model.js"></script>
<style type="text/css">




.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{padding-top: 5px}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
/*.tab-content{margin: 0 10% 0 10%;}*/
.error{color:rgba(255, 0, 0, 0.62);}
.input-group{width: 100%}
.img-circle {
border-radius: 50%;


}
.input-group-addon{
border:none;

}

}
tr.selected {
    background-color: #B0BED9 !important;
}
}
h1,p{
color:white;
}
.bordered th, .bordered td{
padding:10px;
}
.bordered tbody tr:nth-child(odd){
background:#eee;
color:#000;
}
.bordered tbody tr:nth-child(even){
color:#fff;

}


.nav>li>a {
padding: 7px 3px;
}


.tab-header{
    background-color:rgba(250, 250, 250, 0.60); border: 0px solid blue; padding: 1px;
}


</style>
<section class="content-header">

<h1>
Startup
<!-- <small>advanced tables</small> -->

</h1>
<ol class="breadcrumb">
<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
<li><a href="#">Startup</a></li>
<!-- <li class="active">Data tables</li> -->
</ol>
</section>






<!-- Main content -->



<section class="content">

<div class="tab-header">
    <ul class="process-row nav nav-tabs">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>About Planning</strong></p></p>
        </a>
      </li>
      <li class="nav-item" style="text-align:center;">
        <a href="#menu1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>Let's' Start planning</strong></p>
        </a>
      </li>

      <li class="nav-item">
        <a href="#menu5" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>Save my plan</strong></p>
        </a>
      </li>
    </ul>
  </div>


<div class="tab-content clearfix">
                <div id="menu0" class="tab-pane fade active in">
                    <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                        <div class="box-header with-border">
                        <ul class="list-unstyled list-inline pull-right">
                        <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                        </ul>
                        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                        <ul class="list-unstyled list-inline pull-right">
                        </ul>
                        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                </div><!-- /.box-header -->
            <div class="box-body">



                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business. The highest level of planning is developing a strategic plan. Strategic planning is one of the most important functions of a company's leaders.</p>


                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business. The highest level of planning is developing a strategic plan. Strategic planning is one of the most important functions of a company's leaders.</p>


                <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports  assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>


                </div><!-- /.box-body -->
                <div class="box-footer">
                    <ul class="list-unstyled list-inline pull-right">
                <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
                </ul>
                </div>
                </div>

                    </div>
                        <div id="menu1" class="tab-pane fade in">
                            <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                                <div class="box-header with-border">
                                    <ul class="list-unstyled list-inline pull-right">
                                    <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                                    </ul>
                                    <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                                    <ul class="list-unstyled list-inline pull-right">
                                    </ul>
                                    <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                                </div><!-- /.box-header -->
                            <div class="box-body">
                    <form role="form" method="post" action="<?php echo base_url() ?>Planning_setup" name='cmpnySettings' id="company_setting_form" enctype="multipart/form-data">
                <!-- text input -->
                <div class="row">
          <div class="form-group col-md-12">
    <div class="">
        <div class="panel-group" id="accordion">

            <div class="panel ">
                <div class="panel-heading">
                    <h4 class="panel-title"style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse1"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Mission Statement</a>
                    </h4>
                    </div>
                <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body">
                <textarea rows="4" cols="80" class="form-control" id="mission_area" name='mission'
                placeholder=" Many companies have a brief mission statement usually about 30 words or fewer outlining their reason for being in business, and their business philosophy. If you want to draft a mission statement, this is a good place to put it in the plan:" style="resize: none;"
            ><?php echo $mission;?></textarea><label id="rem" style="color:#ccc;"></label>





    </div>
      </div>
    </div>

    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Vision Statement</a>
        </h4>
      </div>
      <div id="collapse2" class="panel-collapse collapse">

        <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="vision-stat" name='vision'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $vission;?></textarea><label id="rem1" style="color:#ccc;"></label></div>
      </div>
    </div>



    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Company Background</a>
        </h4>
      </div>
      <div id="collapse3" class="panel-collapse collapse">

        <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="campany-grnd" name='companygrnd'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $cbackground;?></textarea><label id="rem2" style="color:#ccc;"></label></div>
      </div>
    </div>



    <div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Company Ownership</a>
        </h4>
      </div>
      <div id="collapse4" class="panel-collapse collapse">
       <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="campany-ownrsp" name='companyownr'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $ownership;?></textarea><label id="rem3" style="color:#ccc;"></label></div>
      </div>
    </div>


    <div class="panel">
      <div class="panel-heading">
        <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Goals And Obejectives</a>
      </h4>
      </div>
      <div id="collapse5" class="panel-collapse collapse">
      <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="goals" name='goals'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $goal;?></textarea><label id="rem4" style="color:#ccc;"></label></div>
      </div>
    </div>


    <div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Property Plant And Equipment</a>
      </h4>
      </div>
      <div id="collapse6" class="panel-collapse collapse">
        <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="prpertyplant" name='proprty'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $propertyplan;?></textarea><label id="rem5" style="color:#ccc;"></label></div>
      </div>
    </div>




    <div class="panel ">
      <div class="panel-heading">
        <h4 class="panel-title">
            <a data-toggle="collapse" data-parent="#accordion" href="#collapse7"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Products</a>
            </h4>
            </div>
        <div id="collapse7" class="panel-collapse collapse">
            <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="prodict" name='prouct'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $product;?></textarea><label id="rem6" style="color:#ccc;"></label></div>
        </div>
      </div>



    <div class="panel ">
      <div class="panel-heading">
       <h4 class="panel-title">
         <a data-toggle="collapse" data-parent="#accordion" href="#collapse8"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Services</a>
      </h4>
      </div>
      <div id="collapse8" class="panel-collapse collapse">
     <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="servicd" name='services'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $services;?></textarea><label id="rem7" style="color:#ccc;"></label></div>
      </div>
    </div>


      <div class="panel ">
      <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse9"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Market Target</a>
        </h4>
      </div>
      <div id="collapse9" class="panel-collapse collapse">
     <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="markt-target" name='market'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $market;?></textarea><label id="rem8" style="color:#ccc;"></label></div>
      </div>
      </div>


      <div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse10"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Potencial Customers</a>
      </h4>
      </div>
      <div id="collapse10" class="panel-collapse collapse">
     <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="potencial-custom" name='potencial'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $potentiona;?></textarea><label id="rem9" style="color:#ccc;"></label></div>
      </div>
      </div>


      <div class="panel ">
      <div class="panel-heading">
      <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse11"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Strenghts And Opertunities</a>
      </h4>
      </div>
      <div id="collapse11" class="panel-collapse collapse">
 <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="strengths" name='strengths'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $strengts;?></textarea><label id="rem10" style="color:#ccc;"></label></div>
      </div>
      </div>



      <div class="panel ">
      <div class="panel-heading">
      <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse12"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Weakneses And Threats</a>
      </h4>
      </div>
      <div id="collapse12" class="panel-collapse collapse">
      <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="weekness" name='weekness'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $weakness;?></textarea><label id="rem11" style="color:#ccc;"></label></div>
      </div>
      </div>


      <div class="panel ">
      <div class="panel-heading">
      <h4 class="panel-title">
<a data-toggle="collapse" data-parent="#accordion" href="#collapse13"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Daily Operations</a>
      </h4>
      </div>
      <div id="collapse13" class="panel-collapse collapse">
      <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="dailyopertion" name='dailyopertion'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $daily;?></textarea><label id="rem12" style="color:#ccc;"></label></div>
      </div>
      </div>


      <div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse14"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Facilties And Wharehousing</a>
      </h4>
      </div>
      <div id="collapse14" class="panel-collapse collapse">
      <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="facility" name='facility'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $facilities;?></textarea><label id="rem13" style="color:#ccc;"></label></div>
      </div>
      </div>




      <div class="panel">
      <div class="panel-heading">
      <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse15"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Personel</a>
      </h4>
      </div>
      <div id="collapse15" class="panel-collapse collapse">
     <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="keypersonl" name='keypersonl'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $keypersonal;?></textarea><label id="rem14" style="color:#ccc;"></label></div>
      </div>
      </div>


      <div class="panel ">
      <div class="panel-heading">
      <h4 class="panel-title">
      <a data-toggle="collapse" data-parent="#accordion" href="#collapse16"><iclass="pHeader"><img src="<?=asset_url()?>/img/key.png" alt=""> Exectutive Summary</a>
      </h4>
      </div>
      <div id="collapse16" class="panel-collapse collapse">
      <div class="panel-body"><textarea rows="4" cols="100" class="form-control" id="executive" name='execuitve'
	 placeholder="Type your text here...." style="resize: none;"
	 ><?php echo $execuitve;?></textarea><label id="rem15" style="color:#ccc;"></label></div>
      </div>
      </div>

  </div>
</div>


      </div>
        <!--  <div class="form-group col-md-12">
            <label>Text area 2</label>
            <textarea rows="4" cols="140" class="form-control">
      At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies.
      </textarea>

          </div>
          <div class="form-group col-md-12">
            <label>Textarea3</label>
			<textarea rows="4" cols="140" class="form-control">
      At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies.
      </textarea>

          </div>-->
        </div>
      </div>
     <!-- /.box-header -->



    <div class="box-body">
      <div class="row">



      </div>
    </div><!-- /.box-body -->
    <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
                <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                <li><button type="button" class="btn btn-info next-step" id="comapny_detail_section">Next <i class="fa fa-chevron-right"></i></button></li>
                </ul>
    </div>
</div>

        </div>
            <div id="menu5" class="tab-pane fade">
                <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                    <div class="box-header with-border">
                    <ul class="list-unstyled list-inline pull-right">
                    <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                    </ul>
                    <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                    <ul class="list-unstyled list-inline pull-right">
                    </ul>
                    <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                    </div><!-- /.box-header -->
                <div class="box-body">


                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business. The highest level of planning is developing a strategic plan. Strategic planning is one of the most important functions of a company's leaders.</p>


                <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports  assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>
              </div><!-- /.box-body -->
              <div class="box-footer">
                <ul class="list-unstyled list-inline pull-right">
               <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
               <li><button type="submit" name="submitforms" class="btn btn-success"><i class="fa fa-check"></i> Done!</bfxetchutton></li>
             </ul>
              </div>
            </div>

             </form>
           </div>

         </div>
       </div>
     </section><!-- /.content -->
     <script src="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
     <script src="<?php echo asset_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
     <script src="<?php echo asset_url(); ?>plugins/iCheck/icheck.min.js"></script>
     <script type="text/javascript" src="<?php echo js_url(); ?>jquery.validate.min.js"></script>
     <script type="text/javascript" src="<?php echo js_url(); ?>additional-methods.min.js"></script>
     <script type="text/javascript">



        <!--Start-->
		$('#mission_area').on('keyup keypress',function(e) { //mission_area

		var tval = $('#mission_area').val(),
		tlength = tval.length,
		set = 800,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#mission_area').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
				tval = tval+'\n';
				//console.log(tval);
				$('#mission_area').val(tval);

			}

			$('#rem').text("Remaining:"+remain+'/'+set);
		});

		$('#vision-stat').on('keyup keypress',function(e) { //mission_area

		var tval = $('#vision-stat').val(),
		tlength = tval.length,
		set = 800,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#vision-stat').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';

				$('#vision-stat').val(tval);

			}
		$('#rem1').text("Remaining:"+remain+'/'+set);
		});

		$('#campany-grnd').on('keyup keypress',function(e) { //mission_area

		var tval = $('#campany-grnd').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#campany-grnd').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';

				$('#campany-grnd').val(tval);
				}

		$('#rem2').text("Remaining:"+remain+'/'+set);
		});

		$('#campany-ownrsp').on('keyup keypress',function(e) { //mission_area

		var tval = $('#campany-ownrsp').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#campany-ownrsp').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#campany-ownrsp').val(tval);
			}
		$('#rem3').text("Remaining:"+remain+'/'+set);
		});

		$('#goals').on('keyup keypress',function(e) { //mission_area

		var tval = $('#goals').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#goals').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#goals').val(tval);
			}
		$('#rem4').text("Remaining:"+remain+'/'+set);
		});

		$('#prpertyplant').on('keyup keypress',function(e) { //mission_area

		var tval = $('#prpertyplant').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#prpertyplant').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#prpertyplant').val(tval);
			}
		$('#rem5').text("Remaining:"+remain+'/'+set);
		});

		$('#prodict').on('keyup keypress',function(e) { //mission_area

		var tval = $('#prodict').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#prodict').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#prodict').val(tval);
			}
		$('#rem6').text("Remaining:"+remain+'/'+set);
		});

		$('#servicd').on('keyup keypress',function(e) { //mission_area

		var tval = $('#servicd').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#servicd').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#servicd').val(tval);
			}
		$('#rem7').text("Remaining:"+remain+'/'+set);
		});

		$('#markt-target').on('keyup keypress',function(e) { //mission_area

		var tval = $('#markt-target').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#markt-target').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#markt-target').val(tval);
			}
		$('#rem8').text("Remaining:"+remain+'/'+set);
		});

		$('#potencial-custom').on('keyup keypress',function(e) { //mission_area

		var tval = $('#potencial-custom').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#potencial-custom').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#potencial-custom').val(tval);
			}
		$('#rem9').text("Remaining:"+remain+'/'+set);
		});

		$('#strengths').on('keyup keypress',function(e) { //mission_area

		var tval = $('#strengths').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#strengths').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#strengths').val(tval);
			}
		$('#rem10').text("Remaining:"+remain+'/'+set);
		});

		$('#weekness').on('keyup keypress',function(e) { //mission_area

		var tval = $('#weekness').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#weekness').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#weekness').val(tval);
			}
		$('#rem11').text("Remaining:"+remain+'/'+set);
		});

		$('#dailyopertion').on('keyup keypress',function(e) { //mission_area

		var tval = $('#dailyopertion').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#dailyopertion').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#dailyopertion').val(tval);
			}
		$('#rem12').text("Remaining:"+remain+'/'+set);
		});

		$('#facility').on('keyup keypress',function(e) { //mission_area

		var tval = $('#facility').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#facility').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#facility').val(tval);
			}
		$('#rem13').text("Remaining:"+remain+'/'+set);
		});

		$('#keypersonl').on('keyup keypress',function(e) { //mission_area

		var tval = $('#keypersonl').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#keypersonl').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#keypersonl').val(tval);
			}
		$('#rem14').text("Remaining:"+remain+'/'+set);
		});

		$('#executive').on('keyup keypress',function(e) { //mission_area

		var tval = $('#executive').val(),
		tlength = tval.length,
		set = 1600,
		remain = parseInt(set - tlength);
		if(remain<0)
		{
		$('#executive').val((tval).substring(0, tlength - 1));
		return;
		}
		else
		if(tlength>0)
		if(tlength % 80 == 0){
		tval = tval+'\n';
			$('#executive').val(tval);
			}
		$('#rem15').text("Remaining:"+remain+'/'+set);
		});

		<!--Ends-->



	   $(function(){



            $('.btn-circle').on('click',function(){
             $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
             $(this).addClass('btn-info').removeClass('btn-default').blur();
           });


          $('.next-step, .prev-step').on('click', function (e){
            section_id = $(this).attr('id');

            if(section_id=='comapny_detail_section'){
              /* $("label.error").remove();
              abn_valid = $("#abn_no").valid();
              cn_valid = $("#company_name").valid();
              sn_valid = $("#street_name").val();
              sno_valid = $("#street_no").valid();
              stdate_valid = $("#start_date").valid();
              sub_valid = $("#suburb").valid();
              state_valid = $("#state").valid();
              zp_valid = $("#zipcode").valid();
              cntr_valid = $("#country").valid();
              tph_valid = $("#telephone").valid();
              fx_valid = $("#fax").valid();
              cmp_valid = $("#company_email").valid();
              wb_valid = $("#website").valid(); */


            }


           var $activeTab = $('.tab-pane.active');

           $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');

           if ( $(e.target).hasClass('next-step') )
           {
            var nextTab = $activeTab.next('.tab-pane').attr('id');
            $('[href="#'+ nextTab +'"]').removeClass('btn-default');
            $('[href="#'+ nextTab +'"]').tab('show');
            $("body").scrollTop(0);
            }
            else
            {
              var prevTab = $activeTab.prev('.tab-pane').attr('id');
              $('[href="#'+ prevTab +'"]').removeClass('btn-default');
              $('[href="#'+ prevTab +'"]').tab('show');
              $("body").scrollTop(0);
            }
          });
        });
      </script>
