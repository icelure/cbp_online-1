<?php

extract($monthly_expense);
extract($general_assumptions);
extract($import_duty_assumptions);
extract($australian_payroll);
//echo '<pre/>';print_r($monthly_expense);
//print_r($everydata);
$loan_amnt=$everydata[0]['loan_amount'];
$monthly_payments=$everydata[0]['total_interest'];
$Interst_amount=$everydata[0]['annual_interest'];
$loan_lengh=$everydata[0]['loan_length'];
?>




<style type="text/css">




.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{padding-top: 5px}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
/*.tab-content{margin: 0 10% 0 10%;}*/
.error{color:rgba(255, 0, 0, 0.62);}
.input-group{width: 100%}
.img-circle {
border-radius: 50%;


}
    .input-group-addon{
    border:none;

    }


tr.selected {
    background-color: #B0BED9 !important;
}

.bordered th, .bordered td{
       padding:10px;
}
.bordered tbody tr:nth-child(odd){
   background:#eee;
color:#000;
}
.bordered tbody tr:nth-child(even){
  color:#3820ef;

}

.tab-header{
  background-color:#ecf0f5; border: 0px solid blue; padding: 1px;
}


.nav-pills>li{
    margin-right: 5px;
    margin-bottom: -1px;

}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #fafafa;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
    border-top-color: #60979c;
    border-bottom-color: transparent;
}

.nav-pills>li>a, .nav-pills>li>a:hover {
    background-color: #d4d7dc;
}

.nav-pills>li>a {
    border-radius: 0;
}

@media (max-width: 810px){
  #tabs {
    display: none;
  }
  .nav-pills>li {
    width: 75px;
    text-align: center;
  }
}

@media (max-width: 525px){
  #tabs {
    display: none;
  }
  .nav-pills>li {
    width: 40px;
    text-align: center;
  }
}

</style>

<section class="content-header">

  <h1>
    Startup
    <!-- <small>advanced tables</small> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Startup</a></li>
    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>

<section class="content">
  <div class="tab-header">
    <ul class="process-row nav nav-pills">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-info-sign"></i> <strong id="tabs">About</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-usd"></i> <strong id="tabs">Start Costs</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu2" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-hourglass"></i> <strong id="tabs">Costs</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu3" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-exclamation-sign"></i> <strong id="tabs">Capital</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu4" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-folder-close"></i> <strong id="tabs">Loan</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu5" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-save-file"></i> <strong id="tabs">Summary</strong></p>
        </a>
      </li>
    </ul>
  </div><!--end tab header -->
  <div class="tab-content clearfix">
    <div id="menu0" class="tab-pane fade active in">
      <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
          <ul class="list-unstyled list-inline pull-right">
            <li>
              <a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
            </li>
          </ul>
          <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Startup Costs</strong></h3>
          <ul class="list-unstyled list-inline pull-right">

          </ul>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">What are startup costs ?</h2>
        </div><!-- /.box-header -->
        <div class="box-body">
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Start up cost are the general cost incured when starting a new business venture.</p>
          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;"><strong>What are recurring costs ?</strong></h3>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Recurring costs are the costs that a company will pay usualy on a monthly basis,some typical costs are as follows: </p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;"> * Business Registration</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;"> * Business Stationary</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;"> * Openning Stock Purchases</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;"> * Computers,Printers,Fax Machine</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;"> * Advertising,Security Deposits,Rental Bonds</p>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Project Start-Up Costs Conservatively</h2>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Business success stories like Lala Land or To The Heart And Back were not overnight sensations. When calculating start-up costs,allow funding for a number of months  to cover expenses before you even start trading. And once you do start operating, it is likely that it will take a considerable amount of time until the business is established and self suffiicant.  Be reasonable with your revenue assumptions in the early stages and be conservative with cost projections. It’s also possible to structure a small-business loan to defer payments during the initial operating period.</p>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Separate Recurring Costs from One-Time Start-Up Costs </h2>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Separate One-Time Start-Up Costs from Recurring Costs distinguish between which costs you’will have to account for year-after-year, such as salaries and rent, and which upfront costs will be one-time charges, such as office furniture. This should allow you to establish a budget for after the start-up period. Look for opportunities to delay non-vital expenses such as office decorations until after you’ve begun getting some business.</p>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Need a loan to start ? </h2>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In many cases companies may need a  business loan to start or for any other purpose such as business equipment.When approaching banks and other lenders for money, try to include a substantial cushion for beginning operations to ensure you’ll have enough money to set up an office, take orders, hire employees if necessary, and cover other related costs.To find out thecost of a business loan try the CBP Business Online Loan Calculator the loan automaticaly updates into the relative reports for easy accounting of interstes, and monthly loan payment</p>
          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports  assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li>
              <button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button>
            </li>
          </ul>
        </div>
      </div>
    </div><!--end Menu 0 -->
    <div id="menu1" class="tab-pane fade in">
      <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.0); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
          <ul class="list-unstyled list-inline pull-right">
            <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
          </ul>
          <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
          <ul class="list-unstyled list-inline pull-right">
          </ul>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
        </div><!-- /.box-header -->
        <div class="box-body">
            <h2 style="color:#FFFFFF;font-size:22px;font-weight:light;"></h2>
            <p style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-weight:light;">Business .</p>
        </div><!-- /.box-body -->
        <div class="box-body table_div" style="">
        <!-- text input -->
          <div class="row" id="monthly_expense_row_add">
            <?php include 'person_view.php'; ?>
          </div>
        </div>
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
            <li><button type="button" class="btn btn-info next-step" id="projected_recurring">Next <i class="fa fa-chevron-right"></i></button></li>
          </ul>
        </div>
      </div>
    </div><!--end Menu 1 -->
    <div id="menu2" class="tab-pane fade">
      <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
          <ul class="list-unstyled list-inline pull-right">
            <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
          </ul>
          <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
          <ul class="list-unstyled list-inline pull-right">
          </ul>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
        </div><!-- /.box-header -->
        <div class="box-body">
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Business</p>
          <ul class="list-unstyled list-inline pull-right">
          </ul>
        </div><!-- /.box-header -->
        <div class="box-body table_div">
          <!-- text input -->
          <div class="row" id="one_time_cost_row_add">
            <?php include 'onetimecost_view.php'; ?>
          </div>
        </div>
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
           <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
           <li><button type="button" class="btn btn-info next-step" id="onetimecost">Next <i class="fa fa-chevron-right"></i></button></li>
         </ul>
        </div>
      </div>
    </div><!--end Menu 2 -->
    <div id="menu3" class="tab-pane fade">
      <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
          <div class="box-header with-border">
            <ul class="list-unstyled list-inline pull-right">
              <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
            </ul>
            <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
            <ul class="list-unstyled list-inline pull-right">
            </ul>
            <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
          </div><!-- /.box-header -->
          <div class="box-body">
            <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Business</p>
            <ul class="list-unstyled list-inline pull-right">
            </ul>
          </div><!-- /.box-header -->
          <div class="box-body">
              <!-- text input -->
              <div class="row">
                <?php include 'director_view.php'; ?>
              </div>
          </div>
          <div class="box-footer">
            <ul class="list-unstyled list-inline pull-right">
             <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
             <li><button type="button" class="btn btn-info next-step" id="director_investment">Next <i class="fa fa-chevron-right"></i></button></li>
            </ul>
          </div>
      </div>
   </div><!--end Menu 3 -->
   <div id="menu4" class="tab-pane fade">
      <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
          <ul class="list-unstyled list-inline pull-right">
            <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
          </ul>
          <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online B</strong></h3>
          <ul class="list-unstyled list-inline pull-right">
          </ul>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
        </div><!-- /.box-header -->

        <!--<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Business succ.</p>-->
        <!--start-->
        <div class="box-body">
          <p style="color:#FFFFFF;font-size:22px;font-weight:light;">
            <form role="form" method="post" action="" id="formID"  name="bank" enctype="multipart/form-data">
<div class="form-group col-md-6">

<label>Amount of the loan (<i class="fa <?php if ($currency=="AUD" || $currency=="USD"){echo "fa-dollar";}elseif($currency=="INR"){echo "fa-inr";}elseif($currency=="EUR"){echo "fa-eur";}else{echo "fa-dollar";} ?>"></i>):</label>
<div class="input-group">
<div class="input-group-addon">
<i data-rd-id="r1_pgt" class="fa <?php if ($currency=="AUD" || $currency=="USD"){echo "fa-dollar";}elseif($currency=="INR"){echo "fa-inr";}elseif($currency=="EUR"){echo "fa-eur";}else{echo "fa-dollar";} ?>"></i>
</div>
<input id="amount" name="amount" class="form-control" value="<?php echo isset($loan_amount);?>" required="true">
</div>
</div>
<div class="form-group col-md-6">
<label>Repayment period (years):</label>
<div class="input-group">
<div class="input-group-addon">
<i data-rd-id="r1_pgt" class="fa fa-calendar"></i>
</div>
<input id="apr" name="apr" class="form-control" value="<?php echo isset($loan_length);?>" required="true">
</div>

</div>
<div class="form-group col-md-6">
<label>Annual interest (%):</label>
<div class="input-group">
<div class="input-group-addon">
<i data-rd-id="r1_pgt" class="fa fa-percent"></i>
</div>
<input id="years" name="years" class="form-control" value="<?php echo isset($annual_interest); //$annual_interest;?>" required="true">
</div>
</div>
<div class="form-group col-md-6">
<label>Perodicity value:</label>

<select name="pay_periodicity" class="form-control">

<option value="12" selected="selected">Monthly</option>
 </select>
</div>



<div class="form-group col-md-6">
<label>Approximate Payments:</label>

<button class="form-control" style="background-color: rgba(53, 138, 187, 0.31);
border-color: #00acd6;text-color:white;    font-size: 17px;
font-weight: bold;" type="submit" id="btn" name="btn" > Calculate</button>


</div></form>
<div class="form-group col-md-12" style="margin-top:20px;">


<div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-calculator"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Loan Amount:</span>
                  <span class="info-box-number"><?php echo $loan_amnt;?><small></small></span>

                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
 </div>
<div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-calculator"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Monthly Payment</span>
                  <span class="info-box-number"><?php echo $monthly_payments;?><small></small></span>

                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
 </div>
<div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-calculator"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Loan Term</span>
                  <span class="info-box-number"><?php echo $loan_lengh;?><small></small></span>

                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
 </div>
<div class="col-md-6 col-sm-6 col-xs-12">
              <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="ion ion-calculator"></i></span>
                <div class="info-box-content">
                  <span class="info-box-text">Interest Rate</span>
                  <span class="info-box-number"><?php echo $Interst_amount;?><small></small></span>

                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
 </div>




</div>

<div class="form-group col-md-12"></div>
<?php if($is_post==true) { ?>
<div class="form-group col-lg-10 col-md-12 col-xs-12" style="margin: 0 auto;">

<table cellpadding=5 width=100%  align="center" class=bordered style="margin-bottom: 10px;">
<tr>
<th colspan=4 style="background: rgba(0, 0, 0, 0.36);color:#fff;">Loan Summary</th>
</tr>
<tr>
<td>Loan amount:</td>
<td><b><?php echo $loan_amount;?></b></td>
</tr>
<tr>
<td>Loan length:</td>
<td><b><?php echo $loan_length;?>&nbsp;years</b></td>
</tr>
<tr>
<td>Annual interest:</td>
<td><b><?php echo $annual_interest;?>%</b></td>
</tr>


<tr>
<td>Total paid:</td>
<td><b><?php echo $total_paid;?></b></td>
</tr>
<tr>
<td>Total interest:</td>
<td><b><?php echo $total_interest;?></b></td>
</tr>
<tr>
<td>Total periods:</td>
<td><b><?php echo $total_periods;?></b></td>
</tr>
</table>

<!-- BEGIN amortization_table -->
<div style="width:100%; overflow:auto; ">
<table id="fullsummary" class=bordered cellpadding=5 align="center" style="width:100%;>
<thead style="background: rgba(0, 0, 0, 0.36);color:#fff;">
<tr>
<th>Period</th><th>Interest Paid</th><th>Principal Paid</th><th>Remaining Balance</th>
</tr>
</thead>
<tbody>
<?php echo $amortization_table_rows;?>
</tbody>
<tfoot>
<tr>
<th style="color:#fff;">Totals:</th><th style="color:#fff;"><?php echo $total_interest;?></th><th style="color:#fff;">Total Principal</th><th style="color:#fff;"><?php echo $total_principal; ?></th><th>&nbsp;</th>
</tr></tfoot>
</table>
</div>
<!-- END amortization_table -->

</div><?php } ?>
          </p>
        </div><!-- /.box-body -->
        <!--End-->
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
            <li><button type="button" class="btn btn-info next-step" id="australion_payroll_section">Next <i class="fa fa-chevron-right"></i></button></li>
          </ul>
        </div>
      </div>
    </div><!--end Menu4 -->
    <div id="menu5" class="tab-pane fade">
      <div class="box box-warning"style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
          <ul class="list-unstyled list-inline pull-right">
          <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
          </ul>
          <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Are you sure you have accounted for all your setup costs</strong></h3>
          <ul class="list-unstyled list-inline pull-right">
          </ul>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">We advice that you check.</h2>
        </div><!-- /.box-header -->
        <div class="box-body">
          <p>Summary of your opening costs and Working Capital Balance</p>
          <div id="summary_div">
          </div>
        <div>
          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">What's next </strong></h3>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Click on the Next button and go to the Planniing Moduel</p>
        </div>
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                        <!--li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button></li-->
          </ul>
        </div>
      </div>
    </div><!--end Menu5 -->
  </div> <!--end tab content -->
</section> <!-- end section content-->
<script type="text/javascript">
  $(function () {
    reload_summary();
    //$('#start_date').daterangepicker({singleDatePicker: true});
    $("[data-mask]").inputmask();
    $('.btn-circle').on('click', function () {
        $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
        $(this).addClass('btn-info').removeClass('btn-default').blur();
    });
    $('.next-step, .prev-step').on('click', function (e) {
        var $activeTab = $('.tab-pane.active');
        $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
        if ($(e.target).hasClass('next-step'))
        {
            var nextTab = $activeTab.next('.tab-pane').attr('id');
            $('[href="#' + nextTab + '"]').removeClass('btn-default');
            $('[href="#' + nextTab + '"]').tab('show');
            $("body").scrollTop(0);
        } else
        {
            var prevTab = $activeTab.prev('.tab-pane').attr('id');
            $('[href="#' + prevTab + '"]').removeClass('btn-default');
            $('[href="#' + prevTab + '"]').tab('show');
            $("body").scrollTop(0);
        }
    });
  });
$(document).ready(function(){

  var form = document.getElementById('formID'); // form has to have ID: <form id="formID">
  form.noValidate = true;
  form.addEventListener('submit', function(event) { // listen for form submitting
    if (!event.target.checkValidity()) {
    event.preventDefault(); // dismiss the default functionality
    alert('Your Loan has already been calculated if you wish to revise you your loan,change the loan value amount, enter the term and interest rate and click "Calculate" to update .'); // error message
  }
}, false);
});
function reload_summary(){
  var url;
  url = "<?php echo site_url('startup/ajax_summary')?>";
  // ajax adding data to database
  $.ajax({
      url : url,
      type: "POST",
      data: 'Summary',
      success: function(data)
      {
          $('#summary_div').html(data);

      }
  })
}
</script>