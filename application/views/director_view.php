
    <style type="text/css">
        .input-group .input-group-addon {

        border: 1px solid #ccc;
        width: 48px !important;
    }
    </style>

	<div class="col-md-12" id="director_div">
        <button class="btn btn-success" onclick="add_director()"><i class="glyphicon glyphicon-plus"></i><strong id="tabs"> Add A Person</strong></button>
        <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> <strong id="tabs"> Reload</strong></button>
        <br />
        <br />
        <table id="table_director" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Director</th>
                    <th>Amount</th>
                    <th style="width:50px;">Action</th>
                </tr>
            </thead>
            <tbody>
            </tbody>

            <tfoot>
            <tr>
					<th>Total Capital Investment</th>
                    <th></th>
                <th></th>
            </tr>
            </tfoot>
        </table>
	</div>
<script type="text/javascript">

var save_method_director; //for save method string
var table_director;

$(document).ready(function() {

    //datatables
    table_director = $('#table_director').DataTable({
        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('director/ajax_list')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        {
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],
		"footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
			var numFormat = $.fn.dataTable.render.number( '\,', '.' ).display;
            var response = this.api().ajax.json();
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,\€,\₹]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };

            // Total over all pages
            total = api
                .column( 1 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Total over this page
            pageTotal = api
                .column( 1, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );

            // Update footer
            $( api.column( 1 ).footer() ).html(
                response['currency']+numFormat(pageTotal)
            );

        }
    });

    //datepicker
    $('.datepicker').datepicker({
        autoclose: true,
        format: "yyyy-mm-dd",
        todayHighlight: true,
        orientation: "top auto",
        todayBtn: true,
        todayHighlight: true,
    });

});



function add_director()
{
    save_method_director = 'add';
    $('#form_director')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form_director').modal('show'); // show bootstrap modal
    $('.modal-title').text('Add A Person'); // Set Title to Bootstrap modal title
}

function edit_director(id)
{
    save_method_director = 'update';
    $('#form_director')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('director/ajax_edit/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="director"]').val(data.director);
            $('[name="director_amount_paid"]').val(data.amount);
            $('#modal_form_director').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Director Cost'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
	reload_summary();
    table_director.ajax.reload(null,false); //reload datatable ajax
}

function save_director()
{
    $('#btnSave_director').text('saving...'); //change button text
    $('#btnSave_director').attr('disabled',true); //set button disable
    var url;
    if(save_method_director == 'add') {
        url = "<?php echo site_url('director/ajax_add')?>";
    } else {
        url = "<?php echo site_url('director/ajax_update')?>";
    }

    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form_director').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
                $('#modal_form_director').modal('hide');
                reload_table();
            }

            $('#btnSave_director').text('save'); //change button text
            $('#btnSave_director').attr('disabled',false); //set button enable


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSave_director').text('save'); //change button text
            $('#btnSave_director').attr('disabled',false); //set button enable

        }
    });
}

function delete_director(id)
{
    if(confirm('Are you sure delete this data?'))
    {
        // ajax delete data to database
        $.ajax({
            url : "<?php echo site_url('director/ajax_delete')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                $('#modal_form_director').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });

    }
}

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_director" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">onetimecost Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#" id="form_director" class="form-horizontal">
                    <input type="hidden" value="" name="id"/>
					<?php
						$var = $this->session->userdata;
						$user_id = $var['user']->id;
						$this->db->select('name,directors.id');
						$this->db->from('directors');
						$this->db->join('company_detail', 'company_detail.id = directors.company_id','left');
						$this->db->where('company_detail.user_id',$user_id);
						$query = $this->db->get();
						$com = $query->result();
						//echo '<pre>';print_r($com);echo '</pre>';
						?>
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Director</label>
                            <div class="col-md-9">
                                <select class="form-control" name="director" id="director">
						<option value="">Select</option>
						<?php foreach($com as $row){
						if($row->name){
						?>
							<option value="<?php echo $row->name; ?>"><?php echo $row->name; ?></option>
						<?php }}
						?>
					 </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Amount Paid</label>
                            <div class="col-md-9">


                                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa <?php if ($currency=="AUD" || $currency=="USD"){echo "fa-dollar";}elseif($currency=="INR"){echo "fa-inr";}elseif($currency=="EUR"){echo "fa-eur";}else{echo "fa-dollar";} ?>"></i>
                                      </div>
                                       <input name="director_amount_paid" placeholder="Amount Paid" class="form-control" type="text">
                                <span class="help-block"></span>
                                    </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave_director" onclick="save_director()" class="btn btn-primary">Save</button>

                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->