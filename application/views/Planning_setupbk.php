<?php 
extract($company_detail);
extract($general_assumptions);
extract($import_duty_assumptions);
extract($australian_payroll);
?>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/all.css">
<style type="text/css">
  .process-step .btn:focus{outline:none}
  .process{display:table;width:100%;position:relative}
  .process-row{display:table-row}
  .process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
  .process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
  .process-step{display:table-cell;text-align:center;position:relative}
  .process-step p{margin-top:4px}
  .btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
  .tab-content{margin: 0 10% 0 10%;}
  .error{color:rgba(255, 0, 0, 0.62);}
  .input-group{width: 100%}
  .img-circle {
    border-radius: 50%;
}
</style>
<section class="content-header">
  <h1>
    Company Settings
    <!-- <small>advanced tables</small> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Company settings</a></li>
    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>

<!-- Main content -->
<section class="content">


 <div class="row">
  <div class="process">
   <div class="process-row nav nav-tabs">
    <div class="process-step">
     <button type="button" class="btn btn-info btn-circle" data-toggle="tab" href="#menu0"><i class="fa fa-rocket fa-3x"></i></button>
     <p><strong>Get<br />Started</strong></p>
   </div>
    <div class="process-step">
     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu1"><i class="fa fa-info fa-3x"></i></button>
     <p><strong>Company<br />Setup</strong></p>
   </div>
   <div class="process-step">
     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu2"><i class="fa fa-file-text-o fa-3x"></i></button>
     <p><strong>General<br />Assumptions</strong></p>
   </div>
   <div class="process-step">
     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu3"><i class="fa fa-image fa-3x"></i></button>
     <p><strong>Import Cost<br />Assumptions</strong></p>
   </div>
   <div class="process-step">
     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu4"><i class="fa fa-cogs fa-3x"></i></button>
     <p><strong>Australian <br />Payroll</strong></p>
   </div>
   <div class="process-step">
     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu4"><i class="fa fa-cogs fa-3x"></i></button>
     <p><strong>View <br />1</strong></p>
   </div>
   <div class="process-step">
     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu4"><i class="fa fa-cogs fa-3x"></i></button>
     <p><strong>View <br />2</strong></p>
   </div>
   <div class="process-step">
     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu4"><i class="fa fa-cogs fa-3x"></i></button>
     <p><strong>View<br />3</strong></p>
   </div>
   <div class="process-step">
     <button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu5"><i class="fa fa-check fa-3x"></i></button>
     <p><strong>Save<br />result</strong></p>
   </div>
 </div>
</div>
<div class="tab-content">
<div id="menu0" class="tab-pane fade active in">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title"style="color:#FFFFFF;font-size:60px;font-weight:light;"><strong>What is a Business Plan ?</strong></h3>
      </div><!-- /.box-header -->
      <div class="box-body">
        
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo voluptatum pariatur obcaecati voluptate dolore dicta aspernatur inventore nemo adipisci facilis?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, reiciendis. Dolore error, cumque maiores quaerat autem, dolor illum dolorem enim sunt, repellat et explicabo animi porro vel! Ad dolorum, atque!</p>

          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo voluptatum pariatur obcaecati voluptate dolore dicta aspernatur inventore nemo adipisci facilis?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, reiciendis. Dolore error, cumque maiores quaerat autem, dolor illum dolorem enim sunt, repellat et explicabo animi porro vel! Ad dolorum, atque!</p>
          
        
      </div><!-- /.box-body -->
    </div>
    <ul class="list-unstyled list-inline pull-right">
   <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
  </ul>
 </div>

 <div id="menu1" class="tab-pane fade in">
  <div class="box box-warning">
    <div class="box-header with-border">
      <h3 class="box-title">Start Planning</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
      <form role="form" method="post" action="<?php echo base_url() ?>company_setup" id="company_setting_form" enctype="multipart/form-data">
        <!-- text input -->
        <div class="row">
          <div class="form-group col-md-12">
            <label>text area 1</label>
			<textarea rows="4" cols="140" class="form-control">
At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies. 
</textarea>
           
          </div>
          <div class="form-group col-md-12">
            <label>Text area 2</label>
            <textarea rows="4" cols="140" class="form-control">
At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies. 
</textarea>
			
          </div>
          <div class="form-group col-md-12">
            <label>Textarea3</label>
			<textarea rows="4" cols="140" class="form-control">
At w3schools.com you will learn how to make a website. We offer free tutorials in all web development technologies. 
</textarea>
            
          </div>
        </div>
      </div>
     <!-- /.box-header -->
 
   
  
    <div class="box-body">
      <div class="row">
      

      
      </div>
    </div><!-- /.box-body -->
</div>
<ul class="list-unstyled list-inline pull-right">
  <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
 <li><button type="button" class="btn btn-info next-step" id="comapny_detail_section">Next <i class="fa fa-chevron-right"></i></button></li>
</ul>
</div>
          <div id="menu2" class="tab-pane fade">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Goverment Liabilities</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                        <!-- text input -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>Company Tax</label>
                      <input type="text" class="form-control" name="company_tax" id="company_tax" value="<?php print $company_tax; ?>" placeholder="Enter Tax">
                    </div>
                    <div class="form-group col-md-6">
                      <label>GST/VAT</label>
                      <input type="text" class="form-control" name="company_vat" id="company_vat" value="<?php print $company_vat; ?>" placeholder="GST/VAT">
                    </div>
                  </div>
              </div>
              
              <div class="box-header with-border">
                <h3 class="box-title">General Trading & Terms</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                  <!-- textarea -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>Receivables In Days</label>
                      <select class="form-control" name="rsvbl_in_days">
                        <option value="0" <?php ($rsvbl_in_days == "0") ? print "selected"  : print "" ?>>0</option>
                        <option value="30" <?php ($rsvbl_in_days == "30") ? print "selected" :  print "" ?>>30</option>
                        <option value="60" <?php ($rsvbl_in_days == "60") ? print "selected" :  print "" ?>>60</option>
                        <option value="90" <?php ($rsvbl_in_days == "90") ? print "selected" :  print "" ?>>90</option>
                        <option value="120" <?php ($rsvbl_in_days == "120") ? print "selected" :  print "" ?>>120</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Payables In Days</label>
                      <select class="form-control" name="pybl_in_days">
                       <option value="0" <?php ($pybl_in_days == "0") ? print "selected" :  print "" ?>>0</option>
                        <option value="30" <?php ($pybl_in_days == "30") ? print "selected" :  print "" ?>>30</option>
                        <option value="60" <?php ($pybl_in_days == "60") ? print "selected" :  print "" ?>>60</option>
                        <option value="90" <?php ($pybl_in_days == "90") ? print "selected" :  print "" ?>>90</option>
                        <option value="120" <?php ($pybl_in_days == "120") ? print "selected" :  print "" ?>>120</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>GST/VAT Payable Paid in Days</label>
                      <select class="form-control" name="vat_paid_in_days">
                        <option value="0" <?php ($vat_paid_in_days == "0") ? print "selected" :  print "" ?>>0</option>
                        <option value="30" <?php ($vat_paid_in_days == "30") ? print "selected" :  print "" ?>>30</option>
                        <option value="60" <?php ($vat_paid_in_days == "60") ? print "selected" :  print "" ?>>60</option>
                        <option value="90" <?php ($vat_paid_in_days == "90") ? print "selected" :  print "" ?>>90</option>
                        <option value="120" <?php ($vat_paid_in_days == "120") ? print "selected" :  print "" ?>>120</option>
                      </select>
                    </div>
                    <div class="form-group col-md-6">
                      <label>Company Tax Paid In Days</label>
                      <select class="form-control" name="cmpnytx_paid_in_days">
                        <option value="0" <?php ($cmpnytx_paid_in_days == "0") ? print "selected" :  print "" ?>>0</option>
                        <option value="30" <?php ($cmpnytx_paid_in_days == "30") ? print "selected" :  print "" ?>>30</option>
                        <option value="60" <?php ($cmpnytx_paid_in_days == "60") ? print "selected" :  print "" ?>>60</option>
                        <option value="90" <?php ($cmpnytx_paid_in_days == "90") ? print "selected" :  print "" ?>>90</option>
                        <option value="120" <?php ($cmpnytx_paid_in_days == "120") ? print "selected" :  print "" ?>>120</option>
                      </select>
                    </div>
                  </div>
              </div><!-- /.box-body -->
              <div class="box-header with-border">
                <h3 class="box-title">Opening Cash Balances</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                  <!-- textarea -->
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label>Opening Cash Balance</label>
                      <input type="text" class="form-control" name="opening_cash_balance" id="opening_cash_balance" value="<?php print $opening_cash_balance; ?>" placeholder="Enter Opening Cash Balance">
                    </div>
                    <div class="form-group col-md-4">
                      <label>Opening Debtors Balance</label>
                      <input type="text" class="form-control" name="opening_debtors_balance" id="opening_debtors_balance" value="<?php print $opening_debtors_balance; ?>" placeholder="Enter Opening Debtors Balance">
                    </div>
                    <div class="form-group col-md-4">
                      <label>Closing Creditors Balance</label>
                      <input type="text" class="form-control" name="closing_creditors_balance" id="closing_creditors_balance" value="<?php print $closing_creditors_balance; ?>" placeholder="Enter Closing Creditors Balance">
                    </div>
                  </div>
              </div><!-- /.box-body -->
              <div class="box-header with-border">
                <h3 class="box-title">Sales/Services Income Increase Each Year </h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                  <!-- textarea -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>Sales Income Increase Each Year By</label>
                      <input type="text" class="form-control" name="sales_income_increase" id="sales_income_increase" value="<?php print $sales_income_increase; ?>"  placeholder="Enter Sales Income Increase">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Services Income Each year By</label>
                      <input type="text" class="form-control" name="services_income" id="services_income" value="<?php print $services_income; ?>" placeholder="Enter Services Income">
                    </div>
                  </div>
              </div><!-- /.box-body -->
              <div class="box-header with-border">
                <h3 class="box-title">Sales/Services Cost Increase Each Year </h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                  <!-- textarea -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>Sales Cost Increase Each Year By</label>
                      <input type="text" class="form-control" name="sales_cost_increase" id="sales_cost_increase" value="<?php print $sales_cost_increase; ?>" placeholder="Enter Sales Cost Increase">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Service Cost  Increase Each year By</label>
                      <input type="text" class="form-control" name="service_cost_increase" id="service_cost_increase" value="<?php print $service_cost_increase; ?>" placeholder="Enter Service Cost Increase">
                    </div>
                  </div>
              </div><!-- /.box-body -->
              <div class="box-header with-border">
                <h3 class="box-title">General Operating Expenses Increase Each Year</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                  <!-- textarea -->
                  <div class="row">
                    <div class="form-group col-md-6">
                      <label>Marketing Increase Each Year By</label>
                      <input type="text" class="form-control" name="marketing_increase" id="marketing_increase" value="<?php print $marketing_increase; ?>"  placeholder="Enter Marketing Increase">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Public Relations Increase Each Year By</label>
                      <input type="text" class="form-control" name="public_reactions" id="public_reactions" value="<?php print $public_reactions; ?>"  placeholder="Enter Public Relations Increase">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Administrations Cost Increase Each Year By</label>
                      <input type="text" class="form-control" name="administration_cost" id="administration_cost" value="<?php print $administration_cost; ?>" placeholder="Enter Administrations Cost Increase">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Other Increases Each Year By</label>
                      <input type="text" class="form-control" name="other_increse" id="other_increse" value="<?php print $other_increse; ?>" placeholder="Enter Other Increases Each Year By">
                    </div>
                    <div class="form-group col-md-6">
                      <label>Depreciation On Equipment</label>
                      <input type="text" class="form-control" name="depreciation_on_equipment" id="depreciation_on_equipment" value="<?php print $depreciation_on_equipment; ?>" placeholder="Enter Depreciation On Equipment">
                    </div>
                  </div>
              </div><!-- /.box-body -->
              </div>
              <ul class="list-unstyled list-inline pull-right">
               <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
               <li><button type="button" class="btn btn-info next-step" id="general_assumption_section">Next <i class="fa fa-chevron-right"></i></button></li>
             </ul>
           </div>
           <div id="menu3" class="tab-pane fade">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Import Cost Assumption</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                  
                  <div class="row">
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_imd" value="f" class="minimal" <?php ($r1_imd=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_imd" value="p" class="minimal" <?php ($r1_imd=='p' || $r1_imd==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Import Duty</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_imd" class="fa <?php ($r1_imd=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="import_duty" id="import_duty" value="<?php print $import_duty; ?>" placeholder="Enter Import Duty">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_do" value="f" class="minimal" <?php ($r1_do=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_do" value="p" class="minimal" <?php ($r1_do=='p' || $r1_do==NULL) ? print "checked" : print "" ?> >
                        Percentage
                      </label>
                    </div>
                    
                    <label>Delivery Order </label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_do" class="fa <?php ($r1_do=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="delivery_order" id="delivery_order" value="<?php print $delivery_order; ?>"  placeholder="Enter Delivery Order">
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_cmcf" value="f" class="minimal" <?php ($r1_cmcf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_cmcf" value="p" class="minimal" <?php ($r1_cmcf=='p' || $r1_cmcf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>CMR Comp Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_cmcf" class="fa <?php ($r1_cmcf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="cmr_comp_fee" id="cmr_comp_fee" value="<?php print $cmr_comp_fee; ?>" placeholder="Enter CMR Comp Fee">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_ltf" value="f" class="minimal" <?php ($r1_ltf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_ltf" value="p" class="minimal" <?php ($r1_ltf=='p'  || $r1_ltf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>LCL Transport Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_ltf" class="fa <?php ($r1_ltf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="lcl_transport_fee" id="lcl_transport_fee" value="<?php print $lcl_transport_fee; ?>" placeholder="Enter LCL Transport Fee">
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_caf" value="f" class="minimal" <?php ($r1_caf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_caf" value="p" class="minimal" <?php ($r1_caf=='p' || $r1_caf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Cargo Auto Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_caf" class="fa <?php ($r1_caf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="cargo_auto_fee" id="cargo_auto_fee" value="<?php print $cargo_auto_fee; ?>"  placeholder="Enter Cargo Auto Fee">
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_psf" value="f"  class="minimal" <?php ($r1_psf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_psf" value="p" class="minimal" <?php ($r1_psf=='p' || $r1_psf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Port Service Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_psf" class="fa <?php ($r1_psf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="port_service_fee" id="port_service_fee" value="<?php print $port_service_fee; ?>" placeholder="Enter Port Service Fee">
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_ccf" value="f" class="minimal"  <?php ($r1_ccf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_ccf" value="p" class="minimal" <?php ($r1_ccf=='p' || $r1_ccf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Customs Clearance Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_ccf" class="fa <?php ($r1_ccf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="custom_clearance_fee" value="<?php print $custom_clearance_fee; ?>"  id="custom_clearance_fee" placeholder="Enter Customs Clearance Fee">
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_tff" value="f" class="minimal" <?php ($r1_tff=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_tff" value="p" class="minimal" <?php ($r1_tff=='p' || $r1_tff==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Transport Fues Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_tff" class="fa <?php ($r1_tff=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="transport_fues_fee" value="<?php print $transport_fues_fee; ?>"  id="transport_fues_fee" placeholder="Enter Transport Fues Fee">
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_aqis" value="f" class="minimal" <?php ($r1_aqis=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_aqis" value="p" class="minimal" <?php ($r1_aqis=='p' || $r1_aqis==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>AQIS Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_aqis" class="fa <?php ($r1_aqis=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="aqis_fee" id="aqis_fee" value="<?php print $aqis_fee; ?>" placeholder="Enter AQIS Fee">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_if" value="f" class="minimal" <?php ($r1_if=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>

                        <input type="radio" name="r1_if" value="p" class="minimal" <?php ($r1_if=='p' || $r1_if==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Insurance Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_if" class="fa <?php ($r1_if=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="insurance_fee" id="insurance_fee" value="<?php print $insurance_fee; ?>" placeholder="Enter Insurance Fee">
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_dpf" value="f" class="minimal" <?php ($r1_dpf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_dpf" value="p" class="minimal" <?php ($r1_dpf=='p' || $r1_dpf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Dec Processing Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_dpf" class="fa <?php ($r1_dpf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="dec_processing_fee" id="dec_processing_fee" value="<?php print $dec_processing_fee; ?>" placeholder="Enter Dec Processing Fee">
                    </div>
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_mf" value="f" class="minimal" <?php ($r1_mf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_mf" value="p" class="minimal" <?php ($r1_mf=='p' || $r1_mf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Misc Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_mf" class="fa <?php ($r1_mf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="misc_fee" id="misc_fee" value="<?php print $misc_fee; ?>"  placeholder="Enter Misc Fee">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_of1" value="f" class="minimal" <?php ($r1_of1=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_of1" value="p" class="minimal" <?php ($r1_of1=='p' || $r1_of1==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Other Fee 1</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_of1" class="fa <?php ($r1_of1=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="other_fee_1" id="other_fee_1" value="<?php print $other_fee_1; ?>" placeholder="Enter Other Fee">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_of2" value="f" class="minimal" <?php ($r1_of2=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_of2" value="p" class="minimal" <?php ($r1_of2=='p' || $r1_of2==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Other Fee 2</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_of2"  class="fa <?php ($r1_of2=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="other_fee_2" id="other_fee_2" value="<?php print $other_fee_2; ?>"  placeholder="Enter Other Fee">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_of3" value="f" class="minimal" <?php ($r1_of3=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_of3" value="p" class="minimal" <?php ($r1_of3=='p' || $r1_of3==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Other Fee 3</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_of3" class="fa <?php ($r1_of3=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="other_fee_3" id="other_fee_3" value="<?php print $other_fee_3; ?>"  placeholder="Enter Other Fee">
                    </div>
                    
                  </div>
                  </div>
                    
                </div><!-- /.box-body -->
              </div>
              <ul class="list-unstyled list-inline pull-right">
               <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
               <li><button type="button" class="btn btn-info next-step" id="import_cost_duty_section">Next <i class="fa fa-chevron-right"></i></button></li>
             </ul>
           </div>
           <div id="menu4" class="tab-pane fade">
            <div class="box box-warning">
              <div class="box-header with-border">
                <h3 class="box-title">Australian Payroll Liabilities</h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                  
                  <div class="row">
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_pgt" value="f" class="minimal" <?php ($r1_pgt=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_pgt" value="p" class="minimal" <?php ($r1_pgt=='p' || $r1_pgt==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>PAYG Tax</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_pgt" class="fa <?php ($r1_pgt=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="payg_tax" id="payg_tax" value="<?php print $payg_tax; ?>" placeholder="Enter PAYG Tax">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_s" value="f" class="minimal" <?php ($r1_s=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_s" value="p" class="minimal" <?php ($r1_s=='p' || $r1_s==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Superannuation</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_s" class="fa <?php ($r1_s=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="superannuation" value="<?php print $superannuation; ?>"  id="superannuation" placeholder="Enter Superannuation">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_wc" value="f" class="minimal" <?php ($r1_wc=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_wc" value="p" class="minimal" <?php ($r1_wc=='p' || $r1_wc==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Work Cover</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_wc" class="fa <?php ($r1_wc=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="work_cover" id="work_cover" value="<?php print $work_cover; ?>" placeholder="Enter Work Cover">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_uf" value="f" class="minimal" <?php ($r1_uf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_uf" value="p" class="minimal" <?php ($r1_uf=='p' || $r1_uf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Union Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_uf" class="fa <?php ($r1_uf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="union_fee" id="union_fee" value="<?php print $union_fee; ?>" placeholder="Enter Union Fee">
                    </div>
                    
                  </div>

                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_hp" value="f" class="minimal" <?php ($r1_hp=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_hp" value="p" class="minimal" <?php ($r1_hp=='p' || $r1_hp==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Holiday Pay</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_hp"  class="fa <?php ($r1_hp=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="holiday_pay" id="holiday_pay" value="<?php print $holiday_pay; ?>" placeholder="Enter Holiday Pay">
                    </div>
                    
                  </div>

                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_sl" value="f" class="minimal" <?php ($r1_sl=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_sl" value="p" class="minimal" <?php ($r1_sl=='p' || $r1_sl==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Sick Leave</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_sl" class="fa <?php ($r1_sl=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="sick_leave" id="sick_leave" value="<?php print $sick_leave; ?>"  placeholder="Enter Sick Leave">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_lsf" value="f" class="minimal" <?php ($r1_lsf=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_lsf" value="p" class="minimal" <?php ($r1_lsf=='p' || $r1_lsf==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Long Service Fee</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_lsf" class="fa <?php ($r1_lsf=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="long_service_fee" id="long_service_fee" value="<?php print $long_service_fee; ?>" placeholder="Enter Long Service Fee">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_ot1" value="f" class="minimal" <?php ($r1_ot1=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_ot1" value="p" class="minimal" <?php ($r1_ot1=='p' || $r1_ot1==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Other 1</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_ot1"  class="fa <?php ($r1_ot1=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="other_ap1" id="other_ap1" value="<?php print $other_ap1; ?>" placeholder="Enter Other">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_ot2" value="f" class="minimal" <?php ($r1_ot2=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_ot2" value="p" class="minimal" <?php ($r1_ot2=='p' || $r1_ot2==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Other 2</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_ot2" class="fa <?php ($r1_ot2=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="other_ap2" id="other_ap2" value="<?php print $other_ap2; ?>"  placeholder="Enter Other">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_ot3" value="f" class="minimal" <?php ($r1_ot3=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_ot3" value="p" class="minimal" <?php ($r1_ot3=='p' || $r1_ot3==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Other 3</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i data-rd-id="r1_ot3" class="fa <?php ($r1_ot3=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="other_ap3" id="other_ap3" value="<?php print $other_ap3; ?>"  placeholder="Enter Other">
                    </div>
                    
                  </div>
                  <div class="form-group col-md-6">
                    
                    <div class="pull-right">
                      <label>
                        <input type="radio" name="r1_pri" value="f" class="minimal" <?php ($r1_pri=='f') ? print "checked" : print "" ?>>
                        Fixed Price
                      </label>
                      <label>
                        <input type="radio" name="r1_pri" value="p" class="minimal" <?php ($r1_pri=='p' || $r1_pri==NULL) ? print "checked" : print "" ?>>
                        Percentage
                      </label>
                    </div>
                    
                    <label>Pay Rate Increase Each Year B</label>
                    <div class="input-group">
                    <div class="input-group-addon">
                      <i  data-rd-id="r1_pri" class="fa <?php ($r1_pri=='f') ? print "fa-dollar" : print "fa-percent" ?>"></i>
                    </div>
                    <input type="text" class="form-control" name="payrate_increase" id="payrate_increase" value="<?php print $payrate_increase; ?>"  placeholder="Enter Pay Rate Increase">
                    </div>
                    <input type="hidden" name="submtForm" value="cmpnySettings" >
                    <input type="hidden" name="company_id" value="<?php print $id; ?>">
                    
                  </div>
                  
                  </div>
                    
                </div><!-- /.box-body -->
              </div>
              <ul class="list-unstyled list-inline pull-right">
               <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
               <li><button type="button" class="btn btn-info next-step" id="australion_payroll_section">Next <i class="fa fa-chevron-right"></i></button></li>
             </ul>
           </div>
           <div id="menu5" class="tab-pane fade">
            <div class="box box-solid">
              <div class="box-header with-border">
                <h3 class="box-title"><strong>You are ready now!</strong></h3>
              </div><!-- /.box-header -->
              <div class="box-body">
                
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo voluptatum pariatur obcaecati voluptate dolore dicta aspernatur inventore nemo adipisci facilis?Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, reiciendis. Dolore error, cumque maiores quaerat autem, dolor illum dolorem enim sunt, repellat et explicabo animi porro vel! Ad dolorum, atque!</p>
                  
                
              </div><!-- /.box-body -->
            </div>
              <ul class="list-unstyled list-inline pull-right">
               <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
               <li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button></li>
             </ul>
             </form>
           </div>
           
         </div>
       </div> 
     </section><!-- /.content -->
     <script src="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
     <script src="<?php echo asset_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
     <script src="<?php echo asset_url(); ?>plugins/iCheck/icheck.min.js"></script>
     <script type="text/javascript" src="<?php echo js_url(); ?>jquery.validate.min.js"></script>
     <script type="text/javascript" src="<?php echo js_url(); ?>additional-methods.min.js"></script>
     <script type="text/javascript">
        $(function(){


            $('#start_date').daterangepicker({singleDatePicker: true});
            $("[data-mask]").inputmask();
            $('.btn-circle').on('click',function(){
             $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
             $(this).addClass('btn-info').removeClass('btn-default').blur();
           });



    /*        $('input[type="radio"].minimal').iCheck({
              radioClass: 'iradio_minimal-blue'
            });*/
                
          $('input:radio').change(function(){
            if($(this).val()=='p'){
              $('i[data-rd-id="'+$(this).attr('name')+'"]').removeClass('fa-dollar').addClass('fa-percent');
              
            }else{
                $('i[data-rd-id="'+$(this).attr('name')+'"]').removeClass('fa-percent').addClass('fa-dollar');
            }
           
          });          

    

          $('.next-step, .prev-step').on('click', function (e){
            section_id = $(this).attr('id');

            if(section_id=='comapny_detail_section'){
              $("label.error").remove();
              abn_valid = $("#abn_no").valid();
              cn_valid = $("#company_name").valid();
              sn_valid = $("#street_name").val();
              sno_valid = $("#street_no").valid();
              stdate_valid = $("#start_date").valid();
              sub_valid = $("#suburb").valid();
              state_valid = $("#state").valid();
              zp_valid = $("#zipcode").valid();
              cntr_valid = $("#country").valid();
              tph_valid = $("#telephone").valid();
              fx_valid = $("#fax").valid();
              cmp_valid = $("#company_email").valid();
              wb_valid = $("#website").valid();


              if(abn_valid && sn_valid && sno_valid && stdate_valid && sub_valid && state_valid && zp_valid && cntr_valid && tph_valid && fx_valid && cmp_valid && wb_valid && cn_valid){
                
              }else{
                return false;
              }
              
            }

            if(section_id=='general_assumption_section'){
              $("label.error").remove();
              ctx_valid = $("#company_tax").valid();
              cvt_valid = $("#company_vat").valid();
              ocb_valid = $("#opening_cash_balance").valid();
              odb_valid = $("#opening_debtors_balance").valid();
              ccb_valid = $("#closing_creditors_balance").valid();
              sii_valid = $("#sales_income_increase").valid();
              sri_valid = $("#services_income").valid();
              slc_valid = $("#sales_cost_increase").valid();
              sci_valid = $("#service_cost_increase").valid();
              mi_valid = $("#marketing_increase").valid();
              pr_valid = $("#public_reactions").valid();
              ac_valid = $("#administration_cost").valid();
              doe_valid = $("#depreciation_on_equipment").valid();


              if(ctx_valid && cvt_valid && ocb_valid && odb_valid && ccb_valid && sii_valid && sri_valid && slc_valid && sci_valid && mi_valid && pr_valid && ac_valid && doe_valid){
                
              }else{
                console.log("asd");
                return false;
              }
            }

            if(section_id=='import_cost_duty_section'){
              $("label.error").remove();
              inputboxes = $("#menu3").find('input[type="text"]');
              var flag = 0;
              $.each(inputboxes, function(index, val) {
                  
                  if($("#"+this.id).valid()){

                  }else{
                    console.log("asd");
                    flag = flag + 1;
                  }
              });
              if(flag >= 1){
                return false;
              }
            }

            if(section_id=='australion_payroll_section'){
              $("label.error").remove();
              inputboxes = $("#menu4").find('input[type="text"]');
              var flag = 0;
              $.each(inputboxes, function(index, val) {
                  
                  if($("#"+this.id).valid()){

                  }else{
                    
                    flag = flag + 1;
                  }
              });
              if(flag >= 1){
                return false;
              }
            }


           var $activeTab = $('.tab-pane.active');

           $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');

           if ( $(e.target).hasClass('next-step') )
           {
            var nextTab = $activeTab.next('.tab-pane').attr('id');
            $('[href="#'+ nextTab +'"]').addClass('btn-info').removeClass('btn-default');
            $('[href="#'+ nextTab +'"]').tab('show');
            }
            else
            {
              var prevTab = $activeTab.prev('.tab-pane').attr('id');
              $('[href="#'+ prevTab +'"]').addClass('btn-info').removeClass('btn-default');
              $('[href="#'+ prevTab +'"]').tab('show');
            }
          });
        });
      </script>
