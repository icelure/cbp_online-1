<style>
.pagination>.disabled>a, .pagination>.disabled>a:focus, .pagination>.disabled>a:hover, .pagination>.disabled>span, .pagination>.disabled>span:focus, .pagination>.disabled>span:hover {
    color: #777;
    cursor: not-allowed;
    background-color: #fff;
    border: 1px solid #ddd;
	padding: 6px !important;
}
.table_foot
{
    font-weight: bold;
    text-align: left;
}
</style>
<div class="col-md-12" id="expense_table">
    <button style=" margin-left: 11px; margin-top: 17px; " class="btn btn-success" onclick="add_expenses()"><i class="glyphicon glyphicon-plus"></i> Add Expense</button>
    <br />
    <br />
    <table id="table" class="table table-bordered table-striped">
        <thead>
          <tr>
            <th>Expense ID</th>
            <th>Description</th>
            <th>Weekly Cost</th>
            <th>Monthly Cost</th>
            <th>Quarterly Cost</th>
            <th>Yearly Cost</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          <?php if(!empty($list)) {

              foreach ($list as $expenses) { ?>
                  <tr>
                      <td><?php echo $expenses['id']; ?></td>
                      <td><?php echo $expenses['description']; ?></td>
                      <td>$<?php echo $expenses['weekly_cost']; ?></td>
                      <td>$<?php echo $expenses['monthly_cost']; ?></td>
                      <td>$<?php echo $expenses['quarterly_cost']; ?></td>
                      <td>$<?php echo $expenses['yearly_cost']; ?></td>
                      <td></td>
                  </tr>
              <?php }
          }
          ?>
        </tbody>
        <tfoot class="table_foot">
            <tr>
              <td></td>
              <td style="text-align:right;">Total</td>
              <td id="week_total">$<?php echo $total[0]['weekly_cost']; ?></td>
              <td id="month_total">$<?php echo $total[0]['monthly_cost']; ?></td>
              <td id="quat_total">$<?php echo $total[0]['quarterly_cost']; ?></td>
              <td id="year_total">$<?php echo $total[0]['yearly_cost']; ?></td>
              <td></td>
            </tr>
        </tfoot>

    </table>
</div>