
<div class="col-md-12" style="">
<div class="row">
`

<div class="col-md-6 col-sm-12">
<canvas id="ChartDisplay"  width="300" height="150" ></canvas>

</div>



<div class="col-md-6 col-sm-12 RangeSelector"> 
<div style="padding: 15px;">
	<h4 style="text-align: center;padding: 30px 23px;display: table; margin: 0 auto;border: 1px solid gray;border-radius: 50%;width: 80px; height: 80px;background: #5bc0de; color: white; margin-top: 35px;" ><span class="ServiceIncomePers">0</span>%</h4>
	<input id="ex8" type="range" min="0" max="100" value="50" class="RangeSelectorInput" style="margin-top: 25px;"/>
	<h2 class="text-center">Service income Range</h2>
	<br><br>
	<div class="text-center"><input type="button" value="Save" class="btn btn-info" id="save"/></div>
</div>
 </div>
</div>
</div>
<div class="col-md-12" id="one_time_cost" style="">

<div class="table-responsive">
    <table id="tableimported1" class="table table-striped table-bordered">
<colgroup><col width="5%"><col width="22%"><col width="23%"><col width="6%"><col width="6%"><col width="7%"><col width="10%"><col width="10%"><col width="10%"></colgroup>
        <thead>
            <tr>

                <th>Service ID</th>
                <th>Contracters</th>
                <th>Description</th>
                <th>Weekely Income</th>
                <th>Monthly Income</th>
                <th>Quarterly Income</th>
                <th>Year 1</th>
                <th>Year 2</th>
                <th>Year 3</th>

                
            </tr>
        </thead>
        <tbody>
    </tbody>
        
        <tfoot>
        <tr>
            <th colspan="6" style="text-align: right;">Total</th><th class="SumYTotal1">0</th><th class="SumYTotal2">0</th><th class="SumYTotal3">0</th>
        </tr>
        </tfoot>
		
    </table>
</div>
</div>




<script type="text/javascript">
function number_format(number, decimals, decPoint, thousandsSep){
	decimals = decimals || 0;
	number = parseFloat(number);

	if(!decPoint || !thousandsSep){
		decPoint = '.';
		thousandsSep = ',';
	}

	var roundedNumber = Math.round( Math.abs( number ) * ('1e' + decimals) ) + '';
	var numbersString = decimals ? roundedNumber.slice(0, decimals * -1) : roundedNumber;
	var decimalsString = decimals ? roundedNumber.slice(decimals * -1) : '';
	var formattedNumber = "";

	while(numbersString.length > 3){
		formattedNumber += thousandsSep + numbersString.slice(-3)
		numbersString = numbersString.slice(0,-3);
	}

	return (number < 0 ? '-' : '') + numbersString + formattedNumber + (decimalsString ? (decPoint + decimalsString) : '');
}
       var save_method; //for save method string
        var table1;

        $(document).ready(function () {
        
       $('#save').click(function(){	
			   vi = $('.RangeSelectorInput').val();
				var datapass = 'si='+vi; 
//alert(vi);
							
				$.ajax({
			                type: 'POST',
					data: datapass,
					url: "<?php echo site_url('Upsincome/upincome'); ?>",
					datatype: 'json',
					success: 
					function(data){
					//console.log(data);
					var datas=jQuery.parseJSON(data);
					//alert(datas);
						if(datas.status=='yes'){
                                                   alert("Updated Successfully");
						   console.log("updated");
						}
						if(datas.status=='no'){
                                                    alert("Not Updated");
						   console.log("not updated");
						}
					 }
			  });

		});

            
            //datatables

            table1 = $('#tableimported1').DataTable({
                "processing": true, //Feature control the processing indicator.
                "serverSide": true, //Feature control DataTables' server-side processing mode.
                "order": [], //Initial no order.

                // Load data for the table's content from an Ajax source
                "ajax": {
                    "url": "<?php echo site_url('Importedservice1/ajax_list') ?>",
                    "type": "POST"
    
                },
                
			"drawCallback": function(d) {
			  setTimeout(function(){ CalcSerVal(d.json.Service_income,d.json.currency); }, 300);
			  $('.RangeSelectorInput').val(parseInt(d.json.Service_income));
			  $('.ServiceIncomePers').text(parseInt(d.json.Service_income));
			  $('.RangeSelectorInput').on("input change", function(){
$('#ChartDisplay').html('');
				var vi = $(this).val();
                                //var vi = $('.ServiceIncomePers').text();
				$('.ServiceIncomePers').text(vi);
                                //$("#ChartDisplay").find("div").remove();
				setTimeout(function(){ CalcSerVal(vi,d.json.currency); }, 300);
			  });


			},
                //Set column definition initialisation properties.
			"columnDefs": [
				{
					"targets": [-1], //last column
					"orderable": false, //set not orderable
				},
			],
            });

        });
function reload_tableimported1()
        {
            table1.ajax.reload(null, false); //reload datatable ajax 
        }

var myChart;
function CalcSerVal(ser,cur){
$('#ChartDisplay').html('');
	var tb = $('#tableimported1');
	ser = parseInt(ser);
	var ty1 = 0, ty2 = 0, ty3 = 0;
	tb.find('tbody tr').each(function(){
   		var y1 = parseInt($(this).find('td:eq(6)').text().replace(/,/g, '').replace(/\$|\€|\₹/g, ''));
   		var y2 = parseInt(( y1 * (ser/100) ) + y1);
   		var y3 = parseInt(( y2 * (ser/100) ) + y2);
   		ty1 += y1; ty2 += y2; ty3 += y3;
console.log(y2);
           if(isNaN(y2)) {
   		$(this).find('td:eq(7)').text(cur+number_format(y1, 0, '.', ',' ));
   		$(this).find('td:eq(8)').text(cur+number_format(y1, 0, '.', ','));
           }else{
   		$(this).find('td:eq(7)').text(cur+number_format(y2, 0, '.', ',' ));
   		$(this).find('td:eq(8)').text(cur+number_format(y3, 0, '.', ','));
           }

   		
	});
	
	$('.SumYTotal1').text(cur+number_format(ty1, 0, '.', ','));
     if(isNaN(ty2)) {
	$('.SumYTotal2').text(cur+number_format(ty1, 0, '.', ','));
	$('.SumYTotal3').text(cur+number_format(ty1, 0, '.', ','));
     }else{

	$('.SumYTotal2').text(cur+number_format(ty2, 0, '.', ','));
	$('.SumYTotal3').text(cur+number_format(ty3, 0, '.', ','));
     }
	//ChartDisplay

       
var ctx = document.getElementById("ChartDisplay");
if(myChart != undefined){
  myChart.destroy();
}
myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        
        labels: ["Year 1", "Year 2", "Year 3"],
        datasets: [{
            
            data: [ty1, ty2, ty3],
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)'
            ],
            borderWidth: 1
        }]
    },
    options: {
        legend : {
           display: false
        },
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});

	   // });
 
          //chart.render();
}


</script>
<style>
#tableimported1_filter{
float:right;
}
#tableimported1_paginate{
float:right;
}
.table>tbody>tr>td{
padding:0px !important;
}
h2{
font-family:sans-serif !important;
}

</style>