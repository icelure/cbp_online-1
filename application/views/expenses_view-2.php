
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker-bs3.css">




<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/all.css">
<link href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/datatables/css/dataTables.bootstrap.css')?>" rel="stylesheet">
<link href="<?php echo base_url('assets/bootstrap-datepicker/css/bootstrap-datepicker3.min.css')?>" rel="stylesheet">
<link rel="stylesheet" href="<?php echo asset_url(); ?>css/model.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.2.1/Chart.bundle.min.js"></script>

<style type="text/css">

#table_paginate{
float:right;
}
.pagination .paginate_button {
  padding: 0px 0px !important;
  margin: 0px 0px !important;
}
.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{padding-top:5px;}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
/*.tab-content{margin: 0 10% 0 10%;}*/
.error{color:rgba(255, 0, 0, 0.62);}
.input-group{width: 100%}
.img-circle {
border-radius: 50%;

  }
      .input-group-addon{
      border:none;

      }

  }
tr.selected {
    background-color: #B0BED9 !important;
}
}
h1,p{
color:white;
}
.bordered th, .bordered td{
padding:10px;
}
.bordered tbody tr:nth-child(odd){
background:#eee;
color:#000;
}
.bordered tbody tr:nth-child(even){
color:#fff;

}
tr.selected {
    background-color: #B0BED9 !important;
}

.nav>li>a {
padding: 7px 3px;
}


.tab-header{
    background-color:rgba(250, 250, 250, 0.60); border: 0px solid blue; padding: 1px;
}



</style>
<?php
//print_r($expense_summary); die();
?>
<section class="content-header">

  <h1>
   Monthly Expense Budget
    <!-- <small>advanced tables</small> -->

  </h1>

  <ol class="breadcrumb">

    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

    <li><a href="#"> Monthly Expense Budget</a></li>

    <!-- <li class="active">Data tables</li> -->

  </ol>

</section>



<!-- Main content -->

<section class="content">
<div class="tab-header">
    <ul class="process-row nav nav-tabs">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"> <p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>About Expenses</strong></p>
    </a>
      </li>
      <li class="nav-item">
        <a href="#menu1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>Monthly Expenses</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu2" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>Interaction</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu3" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>Summary</strong></p></a>
      </li>

    </ul>
  </div>



      <div class="tab-content clearfix">
        <div id="menu0" class="tab-pane fade active in">
                    <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                        <div class="box-header with-border">
                            <ul class="list-unstyled list-inline pull-right">
                            <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                            </ul>
                            <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                            <ul class="list-unstyled list-inline pull-right">
                            </ul>
                            <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                        </div><!-- /.box-header -->
                <div class="box-body">



                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Monthly expenese are the general operatign expnses that a comapny has to pay to keep their business open.Some expenses are paid on a weekly or qutrely basis, however in most cases they are paid on a monthly bases.Accountents and financial planners usualy project these expenenses on a monthly payment cycle</p>

                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">When starting a business it is a common sence and advisabe by business planners to allow sufficnant funds or 'WorKing capital' to cover these operating expesnes for at least six mothns until the busines becomes self sufficiant.Failing to do so can often result in business failure and bunckruptsy.</p>

                <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">Some typical monthly expenses are as follows:</h3>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Rent</p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Electricity/Gas</p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Marketing</p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Insurance</p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * MV-Petrol/Service/Registration</p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Office Stationary/Supplies</p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Teleohone/Internet</p>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Petty Cash</p>

                <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">What's next </strong></h3>
                <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this Module,you can enter a your monthly exspenses in 4 categories and calcaute weekly , monthly, quarterly, and yearly scenarios.Click on the Next button to start creating your monthly expense budget,values will be automaticaly calcualted for you,and posted to your reports.</p>

                </div><!-- /.box-body -->
                <div class="box-footer">
                  <ul class="list-unstyled list-inline pull-right">
                  <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
                  </ul>
                </div>
            </div>

    </div>
        <div id="menu1" class="tab-pane fade in">
                <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
                    <div class="box-header with-border">
                    <ul class="list-unstyled list-inline pull-right">
                    <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                    </ul>
                    <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
                    <ul class="list-unstyled list-inline pull-right">
                    </ul>
                    <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
                </div><!-- /.box-header -->
        <div class="box-body">



        <!-- text input -->
    		<div class="row" id="monthly_expense_row_add">
    			<?php include 'expenses_table.php'; ?>
    		</div>

      </div>

      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">

      <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>

     <li><button type="button" class="btn btn-info next-step" id="comapny_detail_section">Next <i class="fa fa-chevron-right"></i></button></li>

    </ul>

      </div>
  </div>


  </div>

  <div id="menu2" class="tab-pane fade">

    <div class="box box-warning">
      <div class="box-body table_div">
        <div class="row" id="expenses_summary_tab">
  				  <div class="box-body">
            <div class="box-header with-border">
                        <h3 class="box-title"style="color:#3c8dbc;font-size:26px;font-weight:bold;">Total Expenses Increase</h3>


                    <ul class="list-unstyled list-inline pull-right">
                    <li><a class="btn btn-info view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
                    </ul>
                    </div>
			      <div class="col-md-6 col-sm-12" id="chart-container">
              <canvas id="chartContainer123"  width="300" height="200" ></canvas>
            </div>
            <div class="col-md-6 col-sm-12 RangeSelector">

              <div>
                <h4 ><span class="ServiceIncomePers" id="m_perc"><?php echo $cost_increase_percentage[0]['marketing'] ?></span>%<span style="float:right; font-size:16px;">Marketing</span></h4>
                <input id="ex8" type="range" min="0" max="100" value="<?php echo $cost_increase_percentage[0]['marketing'] ?>" class="RangeSelectorInput" style="margin-top: 0px;"/>
              </div>

              <div >
                <h4  ><span class="ServiceIncomePers" id="p_perc"><?php echo $cost_increase_percentage[0]['pr'] ?></span>%<span style="float:right; font-size:16px;">Public Relations</span></h4>
                <input id="ex9" type="range" min="0" max="100" value="<?php echo $cost_increase_percentage[0]['pr'] ?>" class="RangeSelectorInput" style="margin-top: 0px;"/>
              </div>

              <div >
                <h4 ><span class="ServiceIncomePers" id="a_perc"><?php echo $cost_increase_percentage[0]['ac'] ?></span>%<span style="float:right; font-size:16px;">Administration</span></h4>
                <input id="ex10" type="range" min="0" max="100" value="<?php echo $cost_increase_percentage[0]['ac'] ?>" class="RangeSelectorInput" style="margin-top: 0px;"/>
              </div>

              <div >
                <h4  ><span class="ServiceIncomePers" id="o_perc"><?php echo $cost_increase_percentage[0]['other'] ?></span>%<span style="float:right; font-size:16px;">Other</span></h4>
                <input id="ex11" type="range" min="0" max="100" value="<?php echo $cost_increase_percentage[0]['other'] ?>" class="RangeSelectorInput" style="margin-top: 0px; margin-bottom: 10px;"/>
              </div>

             <div class="pull-right"><input type="button" value="Save" class="btn btn-info" id="save"/></div>
               </div>
                <table id="table2" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>Account</th>
                      <th>Weekly</th>
                      <th>Monthly</th>
                      <th>Quarterly</th>
                      <th>Year 1</th>
                      <th>Year 2</th>
                      <th>Year 3</th>

                    </tr>
                  </thead>
                  <tbody>
                  <?php
                  $total_year1= 0;
                  $total_year2= 0;
                  $total_year3= 0;

                  if(!empty($list)) {


                      foreach ($expense_summary as $summary) { ?>
                          <tr id="<?php echo $summary['purpose'];?>" onclick="update_graph('<?php echo $summary['purpose'];?>')">
                              <td><?php echo 'Total '. $summary['purpose']; ?></td>
                              <td>$<?php echo $summary['weekly_cost']; ?></td>
                              <td>$<?php echo $summary['monthly_cost']; ?></td>
                              <td>$<?php echo $summary['quarterly_cost']; ?></td>

                              <?php
                                if($summary['purpose']=='Marketing')
                                    $cost_increase = number_format((float) $cost_increase_percentage[0]['marketing']/100, 2, '.', '');
                                elseif($summary['purpose']=='Public-Relations')
                                    $cost_increase =number_format((float) $cost_increase_percentage[0]['pr']/100, 2, '.', '');
                                elseif($summary['purpose']=='Other')
                                      $cost_increase = number_format((float) $cost_increase_percentage[0]['other']/100, 2, '.', '');
                                elseif($summary['purpose']=='Administration')
                                  $cost_increase = number_format((float) $cost_increase_percentage[0]['ac']/100, 2, '.', '');
                                  else
                                    $cost_increase = 1;
                                $year1= $summary['yearly_cost'];
                                $year2 = intval($year1)*$cost_increase + intval($year1);
                                $year3 = intval($year2)*$cost_increase + intval($year2);

                                $total_year1 = $total_year1 + $year1;
                                $total_year2 = $total_year2 + $year2;
                                $total_year3 = $total_year3 + $year3;

                              ?>
                              <td id="<?php echo $summary['purpose'];?>_year1" data-val="<?php echo $year1; ?>">$<?php echo $year1 ?></td>
                              <td id="<?php echo $summary['purpose'];?>_year2" data-val="<?php echo $year2; ?>">$<?php echo $year2 ?></td>
                              <td id="<?php echo $summary['purpose'];?>_year3" data-val="<?php echo $year3; ?>">$<?php echo $year3 ?></td>

                          </tr>
                  <?php }
                  }
                  ?>
                  <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>Total:</b></td>
                    <td id="total_y1" data-val="<?php echo $total_year1; ?>"><b>$<?php echo $total_year1; ?></b></td>
                    <td id="total_y2" data-val="<?php echo $total_year2; ?>"><b>$<?php echo $total_year2; ?></b></td>
                    <td id="total_y3" data-val="<?php echo $total_year3; ?>"><b>$<?php echo $total_year3; ?></b></td>
                  </tr>
                  </tbody>

                </table>
            </div><!-- /.box-body -->
			  </div>
      </div>
      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
                    <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                    <li><button type="button" class="btn btn-info next-step" id="report_section">Next <i class="fa fa-chevron-right"></i></button></li>
                </ul>
      </div>
    </div>

  </div>
  <div id="menu3" class="tab-pane fade">

<div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00) border: 0px solid blue; padding: 0px;">
<div class="box box-solid"style="background-color: rgba(235, 244, 249, 0.0); border: 0px solid blue; padding: 0px;">
<div class="box-header with-border">
<h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;"><strong>Are you sure you have accounted for all your expenses? </strong></h3>


<ul class="list-unstyled list-inline pull-right">
<li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
</ul>


<h2 style="color:#3c8dbc;font-size:22px;font-weight:light;"></h2>



</div><!-- /.box-header -->
<div class="box-body">



<h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">What's next </strong></h3>
<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">If you are satisfied with all your entries then click on the Next button to got to the People Module to evlaute your payroll costs.</p>



        </div><!-- /.box-body -->
    <div class="box-footer">
      <ul class="list-unstyled list-inline pull-right">
                    <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
                    <li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button></li>
                </ul>
    </div>
                </div>





            </div>

<!--            <div id="menu3" class="tab-pane fade">

            <div class="box box-warning">
                <label class="control-label" style="color:green" id="success_msg"></label>
              <div class="box-header with-border">
              </div><!-- /.box-header -->
 				      <!-- <div class="box-body">
                <form action ="#" id="save_rersults" name="save_results">
                  <div class="form-body">
                    <div class="form-group">
                            <label class="control-label col-md-3">Comment</label>
                            <div class="col-md-9">
                                <textarea name="comment" class="form-control" ></textarea>
                                <span class="help-block"></span>
                            </div>
                            <div class="modal-footer">
                              <button type="button" id="saveResults"  class="btn btn-primary">Save</button>
                            </div>
                        </div>
                  </div>
                </form>

              </div>


              </div>

              <ul class="list-unstyled list-inline pull-right">

               <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>

               <li><button type="button" class="btn btn-info next-step" id="import_cost_duty_section">Next <i class="fa fa-chevron-right"></i></button></li>

             </ul>

           </div> -->


        </div>

   </div>

     </section><!-- /.content -->

     <script src="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>

     <script src="<?php echo asset_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>

     <script src="<?php echo asset_url(); ?>plugins/iCheck/icheck.min.js"></script>

     <script type="text/javascript" src="<?php echo js_url(); ?>jquery.validate.min.js"></script>

     <script type="text/javascript" src="<?php echo js_url(); ?>additional-methods.min.js"></script>


     <script type="text/javascript">
     var myBarChart = null;
		/*function reload_summary(){
				var url;
        url = "<?php echo site_url('startup/ajax_summary')?>";


    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: 'Summary',
        success: function(data)
        {
            $('#summary_div').html(data);

		}
    });
			}*/
		$(function(){
			// reload_summary();




   //          $('#start_date').daterangepicker({singleDatePicker: true});

   //          $("[data-mask]").inputmask();

   //          $('.btn-circle').on('click',function(){

   //           $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');

   //           $(this).addClass('btn-info').removeClass('btn-default').blur();

   //         });







    /*        $('input[type="radio"].minimal').iCheck({

              radioClass: 'iradio_minimal-blue'

            });*/



          $('input:radio').change(function(){

            if($(this).val()=='p'){

              $('i[data-rd-id="'+$(this).attr('name')+'"]').removeClass('fa-dollar').addClass('fa-percent');



            }else{

                $('i[data-rd-id="'+$(this).attr('name')+'"]').removeClass('fa-percent').addClass('fa-dollar');

            }



          });



          $("#company_setting_form").validate({

            rules:{

              company_name : "required",

              abn_no : "required",

              start_date : "required",

              street_no : "required",

              street_name : "required",

              suburb : "required",

              state : "required",

              zipcode : "required",

              country : "required",

              fax:{

                number : true

              },

              company_email:{

                required : true,

                email : true

              },

              website:{

                required : true,

                url : true

              },

              company_tax:{

                required : true

              },

              company_vat:{

                required : true

              },

              opening_cash_balance:{

                required : true

              },

              opening_debtors_balance:{

                required : true

              },

              closing_creditors_balance:{

                required : true

              },

              sales_income_increase:{

                required: true

              },

              services_income:{

                required: true

              },

              sales_cost_increase:{

                required: true

              },

              service_cost_increase:{

                required: true

              },

              marketing_increase:{

                required:true

              },

              public_reactions:{

                required: true

              },

              administration_cost:{

                required: true

              },

              depreciation_on_equipment:{

                required: true

              },

              import_duty:{

                required: true

              },

              delivery_order:{

                required: true

              },

              cmr_comp_fee:{

                required: true

              },

              lcl_transport_fee:{

                required: true

              },

              cargo_auto_fee:{

                required: true

              },

              port_service_fee:{

                required: true

              },

              custom_clearance_fee:{

                required: true

              },

              transport_fues_fee:{

                required: true

              },

              aqis_fee:{

                required: true

              },

              insurance_fee:{

                required: true

              },

              dec_processing_fee:{

                required: true

              },

              misc_fee:{

                required: true

              },

              payg_tax:{

                required: true

              },

              superannuation:{

                required: true

              },

              work_cover:{

                required: true

              },

              union_fee:{

                required: true

              },

              holiday_pay:{

                required: true

              },

              sick_leave:{

                required: true

              },

              long_service_fee:{

                required: true

              },

              payrate_increase:{

                required: true

              }

            }

          });
           $('.btn-circle').on('click',function(){
             $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
             $(this).addClass('btn-info').removeClass('btn-default').blur();
           });


          $('.next-step, .prev-step').on('click', function (e){

            section_id = $(this).attr('id');
            var $activeTab = $('.tab-pane.active');



           $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');



           if ( $(e.target).hasClass('next-step') )

           {

            var nextTab = $activeTab.next('.tab-pane').attr('id');

            $('[href="#'+ nextTab +'"]').removeClass('btn-default');

            $('[href="#'+ nextTab +'"]').tab('show');
            $("body").scrollTop(0);

            }

            else

            {

              var prevTab = $activeTab.prev('.tab-pane').attr('id');

              $('[href="#'+ prevTab +'"]').removeClass('btn-default');

              $('[href="#'+ prevTab +'"]').tab('show');
              $("body").scrollTop(0);
            }

          });

        });

      </script>
      <script type="text/javascript">

     window.onload = function(){
        update_graph("loadchart");

     }

    function update_graph(account)
    {
      if(myBarChart!=null){
        $("#chart-container").empty();
        $("#chart-container").append('<canvas id="chartContainer123"  width="300" height="200" ></canvas>');
      }

      var ctx = $("#chartContainer123");

      if(account=="loadchart"){

        var year1 = $("#total_y1").attr('data-val');
        var year2 = $("#total_y2").attr('data-val');
        var year3 = $("#total_y3").attr('data-val');
      }
      else{
        var year1 = $("#"+account+"_year1").attr('data-val');
        var year2 = $("#"+account+"_year2").attr('data-val');
        var year3 = $("#"+account+"_year3").attr('data-val');
      }



      var data = {
                    labels: ["Year One","Year Two","Year Three"],
                    datasets: [
                        {
                              label: "Expense Summary",
                              backgroundColor: [
                                  'rgba(255, 99, 132, 0.2)',
                                  'rgba(54, 162, 235, 0.2)',
                                  'rgba(255, 206, 86, 0.2)'
                              ],
                              borderColor: [
                                  'rgba(255,99,132,1)',
                                  'rgba(54, 162, 235, 1)',
                                  'rgba(255, 206, 86, 1)'
                              ],
                              borderWidth: 1,
                              data: [year1,year2,year3],
                        }
                    ]
                };

       myBarChart = new Chart(ctx, {
          type: 'bar',
          data: data,
          options: {
                    scales: {
                              xAxes: [{
                                      stacked: true
                              }],
                              yAxes: [{
                                      stacked: true
                              }]
                            }

                  }
          });



    }
    $('#table2 tbody').on( 'click', 'tr', function () {
           // $(this).find('td').eq(0).text();
            //alert( 'Row index: '+table.row( this ).data());
           // var id = table.row( this ).id();
          //  alert( $(this).find('td:first').text() );
            if ( $(this).hasClass('selected') ) {
                $(this).removeClass('selected');
            }
            else {
                $('#table2 tr.selected').removeClass('selected');
                $(this).addClass('selected');
            }


        } );
    $('#saveResults').click(function(){
           $("#success_msg").text("Comment Saved successfully!");


        } );


  </script>
  <script type="text/javascript">

  //$("#ex8").change(function(){
   $('#ex8').on("input change", function(){
     var sv = parseInt($("#ex8").val());
     $("#m_perc").text(sv);
     $("#ex8").val(sv);
     if($("#Marketing").length>0){

     var account = "Marketing";
     var years= [];
     years['year1'] = parseInt($("#Marketing_year1").attr('data-val'));
     years['year2'] = parseInt( years['year1'] * (sv/100)) + years['year1'];
     years['year3'] = parseInt( years['year2'] * (sv/100)) + years['year2'];

     $("#Marketing_year2").attr('data-val',years['year2']);
     $("#Marketing_year3").attr('data-val',years['year3']);
     $("#Marketing_year2").text('$'+years['year2']);
     $("#Marketing_year3").text('$'+years['year3']);
     calculate_total();
     update_graph(account);

     }

  });
  $('#ex9').on("input change", function(){

     var sv = parseInt($("#ex9").val());
     $("#p_perc").text(sv);
     $("#ex9").val(sv);
     if($("#Public-Relations").length>0)
     {
        var account = "Public-Relations";
        var years= [];
        years['year1'] = parseInt($("#Public-Relations_year1").attr('data-val'));
        years['year2'] = parseInt( years['year1'] * (sv/100)) + years['year1'];
        years['year3'] = parseInt( years['year2'] * (sv/100)) + years['year2'];

        $("#Public-Relations_year2").attr('data-val',years['year2']);
        $("#Public-Relations_year3").attr('data-val',years['year3']);
        $("#Public-Relations_year2").text('$'+years['year2']);
        $("#Public-Relations_year3").text('$'+years['year3']);
        calculate_total();
        update_graph(account);
    }

  });
  $('#ex10').on("input change", function(){

      var sv = parseInt($("#ex10").val());
      $("#a_perc").text(sv);
      $("#ex10").val(sv);
      if($("#Administration").length>0){
        var account = "Administration";
        var years= [];
        years['year1'] = parseInt($("#Administration_year1").attr('data-val'));
        years['year2'] = parseInt( years['year1'] * (sv/100)) + years['year1'];
        years['year3'] = parseInt( years['year2'] * (sv/100)) + years['year2'];

        $("#Administration_year2").attr('data-val',years['year2']);
        $("#Administration_year3").attr('data-val',years['year3']);
        $("#Administration_year2").text('$'+years['year2']);
        $("#Administration_year3").text('$'+years['year3']);
        calculate_total();
        update_graph(account);
      }

  });
  $('#ex11').on("input change", function(){

      var sv = parseInt($("#ex11").val());
      $("#o_perc").text(sv);
      $("#ex11").val(sv);
      if($("#Other").length>0){
        var account = "Other";
        var years= [];
        years['year1'] = parseInt($("#Other_year1").attr('data-val'));
        years['year2'] = parseInt( years['year1'] * (sv/100)) + years['year1'];
        years['year3'] = parseInt( years['year2'] * (sv/100)) + years['year2'];

        $("#Other_year2").attr('data-val',years['year2']);
        $("#Other_year3").attr('data-val',years['year3']);
        $("#Other_year2").text('$'+years['year2']);
        $("#Other_year3").text('$'+years['year3']);
        calculate_total();
        update_graph(account);
      }
  });

  $('#save').click(function(){
        var m_perc = $("#ex8").val();
        var p_perc = $("#ex9").val();
        var a_perc = $("#ex10").val();
        var o_perc = $("#ex11").val();

        $.ajax({
          type: 'GET',
          url: "<?php echo site_url('Expenses/updateExpensesIncrease'); ?>?m="+m_perc+"&p="+p_perc+"&a="+a_perc+"&o="+o_perc,

          success:
          function(data){
           alert('Saved successfully');
                     }
        });


  });

  function calculate_total(){

    var total_y1 = 0;
    var total_y2 = 0;
    var total_y3 = 0;

    total_y1= parseFloat( ($("#Marketing_year1").length>0) ? $("#Marketing_year1").attr('data-val'):0.00 ) + parseFloat( ($("#Public-Relations_year1").length>0) ? $("#Public-Relations_year1").attr('data-val'):0.00 ) + parseFloat( ($("#Administration_year1").length>0) ? $("#Administration_year1").attr('data-val'):0.00 ) + parseFloat( ($("#Other_year1").length>0) ? $("#Other_year1").attr('data-val'):0.00 );
    total_y2= parseFloat( ($("#Marketing_year2").length>0) ? $("#Marketing_year2").attr('data-val'):0.00 ) + parseFloat( ($("#Public-Relations_year2").length>0) ? $("#Public-Relations_year2").attr('data-val'):0.00 ) + parseFloat( ($("#Administration_year2").length>0) ? $("#Administration_year2").attr('data-val'):0.00 ) + parseFloat( ($("#Other_year2").length>0) ? $("#Other_year2").attr('data-val'):0.00 );
    total_y3= parseFloat( ($("#Marketing_year3").length>0) ? $("#Marketing_year3").attr('data-val'):0.00 ) + parseFloat( ($("#Public-Relations_year3").length>0) ? $("#Public-Relations_year3").attr('data-val'):0.00 ) + parseFloat( ($("#Administration_year3").length>0) ? $("#Administration_year3").attr('data-val'):0.00 ) + parseFloat( ($("#Other_year3").length>0) ? $("#Other_year3").attr('data-val'):0.00 );

    $("#total_y1").attr('data-val',total_y1.toFixed(2));
    $("#total_y2").attr('data-val',total_y2.toFixed(2));
    $("#total_y3").attr('data-val',total_y3.toFixed(2));

    $("#total_y1").text('$'+total_y1.toFixed(2));
    $("#total_y2").text('$'+total_y2.toFixed(2));
    $("#total_y3").text('$'+total_y3.toFixed(2));
  }
  </script>


