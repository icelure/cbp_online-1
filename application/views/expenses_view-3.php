<style type="text/css">

#table_paginate{
float:right;
}
.pagination .paginate_button {
  padding: 0px 0px !important;
  margin: 0px 0px !important;
}
.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{padding-top:5px;}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
.process-step{display:table-cell;text-align:center;position:relative}
.process-step p{margin-top:4px}
.btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
/*.tab-content{margin: 0 10% 0 10%;}*/
.error{color:rgba(255, 0, 0, 0.62);}
.input-group{width: 100%}
.img-circle {
border-radius: 50%;

  }
      .input-group-addon{
      border:none;

      }

  }
tr.selected {
    background-color: #B0BED9 !important;
}
}
h1,p{
color:white;
}
.bordered th, .bordered td{
padding:10px;
}
.bordered tbody tr:nth-child(odd){
background:#eee;
color:#000;
}
.bordered tbody tr:nth-child(even){
color:#fff;

}
tr.selected {
    background-color: #B0BED9 !important;
}

.nav>li>a {
    padding: 7px 3px;
}


.tab-header{
  background-color:#ecf0f5; border: 0px solid blue; padding: 1px;
}


.nav-pills>li{
    margin-right: 5px;
    margin-bottom: -1px;

}
.nav-pills>li.active>a, .nav-pills>li.active>a:focus, .nav-pills>li.active>a:hover {
    color: #fff;
    background-color: #fafafa;
}
.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
    border-top-color: #60979c;
    border-bottom-color: transparent;
}

.nav-pills>li>a, .nav-pills>li>a:hover {
    background-color: #d4d7dc;
}

.nav-pills>li>a {
    border-radius: 0;
}
@media (max-width: 810px){
  #tabs {
    display: none;
  }
  .nav-pills>li {
    width: 80px;
    text-align: center;
  }
}

@media (max-width: 525px){
  #tabs {
    display: none;
  }
  .nav-pills>li {
    width: 40px;
    text-align: center;
  }
}

.input-group .input-group-addon {
       
        border: 1px solid #ccc;
        width: 48px !important;
    }

</style>
<?php

?>
<section class="content-header">
  <h1>
   Monthly Expense Budget
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#"> Monthly Expense Budget</a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="tab-header">
    <ul class="process-row nav nav-pills">
      <li class="nav-item active">
        <a href="#menu0" data-toggle="tab"> <p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-info-sign"></i> <strong id="tabs">About</strong></p>
      </a>
      </li>
      <li class="nav-item">
        <a href="#menu1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-shopping-cart"></i> <strong id="tabs">Monthly Expenses</strong></p></a>
      </li>
      <li class="nav-item">
        <a href="#menu2" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-thumbs-up"></i> <strong id="tabs">Interaction</strong></p>
        </a>
      </li>
      <li class="nav-item">
        <a href="#menu3" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-save-file"></i> <strong id="tabs">Summary</strong></p></a>
      </li>
    </ul>
  </div>
  <div class="tab-content clearfix">
    <div id="menu0" class="tab-pane fade active in">
      <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
          <ul class="list-unstyled list-inline pull-right">
          <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
          </ul>
          <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
          <ul class="list-unstyled list-inline pull-right">
          </ul>
          <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
        </div><!-- /.box-header -->
        <div class="box-body">
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Monthly expenese are the general operatign expnses that a comapny has to pay to keep their business open.Some expenses are paid on a weekly or qutrely basis, however in most cases they are paid on a monthly bases.Accountents and financial planners usualy project these expenenses on a monthly payment cycle</p>

          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">When starting a business it is a common sence and advisabe by business planners to allow sufficnant funds or 'WorKing capital' to cover these operating expesnes for at least six mothns until the busines becomes self sufficiant.Failing to do so can often result in business failure and bunckruptsy.</p>

          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">Some typical monthly expenses are as follows:</h3>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Rent</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Electricity/Gas</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Marketing</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Insurance</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * MV-Petrol/Service/Registration</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Office Stationary/Supplies</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Teleohone/Internet</p>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;"> * Petty Cash</p>

          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">What's next </strong></h3>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this Module,you can enter a your monthly exspenses in 4 categories and calcaute weekly , monthly, quarterly, and yearly scenarios.Click on the Next button to start creating your monthly expense budget,values will be automaticaly calcualted for you,and posted to your reports.</p>

        </div><!-- /.box-body -->
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
          <li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
          </ul>
        </div>
      </div>
    </div> <!-- end menu0 -->

    <div id="menu1" class="tab-pane fade in">
        <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
            <div class="box-header with-border">
              <ul class="list-unstyled list-inline pull-right">
                <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
              </ul>
              <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
              <ul class="list-unstyled list-inline pull-right">
              </ul>
              <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
            </div><!-- /.box-header -->
        <div class="box-body">
          <!-- text input -->
    		  <div class="row" id="monthly_expense_row_add">
    			 <?php include 'expenses_table.php'; ?>
    		  </div>
        </div>
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
            <li><button type="button" class="btn btn-info next-step" id="comapny_detail_section">Next <i class="fa fa-chevron-right"></i></button></li>
          </ul>
        </div>
      </div>
    </div> <!-- end menu1 -->

  <div id="menu2" class="tab-pane fade">
    <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00); border: 0px solid blue; padding: 0px;">
      <div class="box-header with-border">
        <ul class="list-unstyled list-inline pull-right">
          <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
        </ul>
        <h3 class="box-title"style="color:rgba(0, 0, 0, 1.0);font-size:17px;font-size:22px;font-weight:thin;"><strong>Online Business Planning</strong></h3>
        <ul class="list-unstyled list-inline pull-right">
        </ul>
        <h2 style="color:#3c8dbc;font-size:22px;font-weight:bold;">Planning is the key to your success !</h2>
      </div><!-- /.box-header -->
      <div class="box-body">
        <div class="row" id="expenses_summary_tab">
          <?php include 'expanses_summary_table.php'; ?>
		    </div>
      </div>
      <div class="box-footer">
        <ul class="list-unstyled list-inline pull-right">
          <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
          <li><button type="button" class="btn btn-info next-step" id="report_section">Next <i class="fa fa-chevron-right"></i></button></li>
        </ul>
      </div>
    </div>
  </div> <!-- end menu2 -->

  <div id="menu3" class="tab-pane fade">
    <div class="box box-warning" style="background-color: rgba(250, 250, 250, 1.00) border: 0px solid blue; padding: 0px;">
      <div class="box box-solid"style="background-color: rgba(235, 244, 249, 0.0); border: 0px solid blue; padding: 0px;">
        <div class="box-header with-border">
          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;"><strong>Are you sure you have accounted for all your expenses? </strong></h3>

          <ul class="list-unstyled list-inline pull-right">
            <li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a>
            </li>
          </ul>
        </div><!-- /.box-header -->
        <div class="box-body">
          <h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;">What's next </strong></h3>
          <p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">If you are satisfied with all your entries then click on the Next button to got to the People Module to evlaute your payroll costs.</p>
        </div><!-- /.box-body -->
        <div class="box-footer">
          <ul class="list-unstyled list-inline pull-right">
            <li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
            <li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button></li>
          </ul>
        </div>
      </div>
    </div>
  </div> <!-- end menu3 -->
  </div> <!-- end tab-content -->
</section> <!-- End Section Body -->
<div class="modal fade" id="modal_form" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title">Expenses Form</h3>
      </div>
      <div class="modal-body form">
        <form action="#" id="form" class="form-horizontal">
          <input type="hidden" value="" name="id"/>
          <div class="form-body">
            <div class="form-group">
              <label class="control-label col-md-3">Description</label>
              <div class="col-md-9">
                

                <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-pencil"></i>
                                      </div>
                                    <input name="description" placeholder="Expenses Description" class="form-control" type="text">
                <span class="help-block"></span>
                                    </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Weekly Cost</label>
              <div class="col-md-9">
                  

                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                      <input name="weekly_cost" id="weekly_cost" placeholder="Weekly Cost" onBlur="calculateCostFromWeekly()" class="form-control" type="text">
                  <span class="help-block"></span>
                                    </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Monthly Cost</label>
              <div class="col-md-9">
                  

                   <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                     <input name="monthly_cost" id="monthly_cost" placeholder="Monthly Cost" onBlur="calculateCostFromMonthly()" class="form-control" type="text">
                  <span class="help-block"></span>
                                    </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Quarterly Cost</label>
              <div class="col-md-9">
                  


                   <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                     <input name="quarterly_cost" id="quarterly_cost" placeholder="Quarterly Cost" onBlur="calculateCostFromQuaterly()" class="form-control" type="text">
                  <span class="help-block"></span>
                                    </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Yearly Cost</label>
              <div class="col-md-9">
                  

                  <div class="input-group">
                                      <div class="input-group-addon">
                                          <i class="fa fa-dollar"></i>
                                      </div>
                                     <input name="yearly_cost" id="yearly_cost" placeholder="Yearly Cost" onBlur="calculateCostFromYearly()" class="form-control" type="text">
                  <span class="help-block"></span>
                                    </div>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3">Purpose</label>
              <div class="col-md-9">
                  <select name="purpose" class="form-control">
                    <option value="Marketing">Marketing</option>
                    <option value="Public-Relations">Public Relations</option>
                    <option value="Administration">Administration</option>
                    <option value="Other">Other</option>
                  </select>
                  <span class="help-block"></span>
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
          <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">

var save_method;

$(function () {
  table = $('#table').DataTable({
    "ajax": {
      "url": "<?php echo site_url('expenses/ajax_list')?>",
      "type": "POST"
    },
   "aoColumnDefs": [{
        "aTargets": [6],
        "mData": null,
        "mRender": function (data, type, full) {
            return '<a id="edit" style=" margin-top: 17px;margin-right: 8px; " class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_expense('+ full[0] +')"><i class="glyphicon glyphicon-pencil"></i></a><a id="delete" style=" margin-top: 17px; " class="btn btn-sm btn-danger" href="javascript:void(0)" title="Delete" onclick="delete_expense('+ full[0] +')"><i class="glyphicon glyphicon-trash"></i></a>';
          }
    }]
  });

  $('#table tbody').on( 'click', 'tr', function () {
      if ( $(this).hasClass('selected') ) {

        $(this).removeClass('selected');

      }else{

        table.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');

      }
      $('#edit').attr('data-info', $(this).find('td:first').text());
      $('#delete').attr('data-info', $(this).find('td:first').text());
  });
});

function reload_table()
{
    table.ajax.reload(function(json){
      reload_graph();
    });

}

function reload_graph(){

  url = "<?php echo base_url(); ?>expenses/ajax_expense_summary";
  $.ajax({

    url : url,
    type: "POST",
    success: function(data)
    {
      var summary_data = JSON.parse(data);
      summary = summary_data.expense_summary;
      var html="";
      var total_y1=0;
      var total_y2=0;
      var total_y3=0;

      for( var i=0;i<summary.length; i++){
        var purpose = summary[i]['purpose'];
        var cost_increase = get_cost_increase_perc(summary_data.  cost_increase_percentage,summary[i]);
          html+="<tr id="+summary[i]['purpose']+" onclick='update_graph(\""+purpose+"\")'><td>Total  "+summary[i]['purpose']+"</td><td>$"+summary[i]['weekly_cost']+"</td><td>$"+summary[i]['monthly_cost']+"</td><td>$"+summary[i]['quarterly_cost']+"</td>";

        var year1= parseInt(summary[i]['yearly_cost']);
        var year2 = parseInt(year1)*cost_increase + parseInt(year1);
        var year3 = parseInt(year2)*cost_increase + parseInt(year2);
        year1.toFixed(2);
        year2.toFixed(2);
        year3.toFixed(2);
        total_y1 = total_y1+year1;
        total_y2 = total_y2+year2;
        total_y3 = total_y3+year3;

        html+=" <td id='"+summary[i]['purpose']+"_year1' data-val='"+year1+"'>$"+year1+" </td>";
        html+=" <td id='"+summary[i]['purpose']+"_year2' data-val='"+year2+"'>$"+year2+" </td>";
        html+=" <td id='"+summary[i]['purpose']+"_year3' data-val='"+year3+"'>$"+year3+" </td>";
        html+="<td></td></tr>";
      }

        html+='<tr><td></td><td></td><td></td><td><b>Total:</b></td><td id="total_y1" data-val="'+total_y1.toFixed(2)+'"><b>$'+total_y1.toFixed(2)+'</b></td><td id="total_y2" data-val="'+total_y2.toFixed(2)+'"><b>$'+total_y2.toFixed(2)+'</b></td><td id="total_y3" data-val="'+total_y3.toFixed(2)+'"><b>$'+total_y3.toFixed(2)+'</b></td></tr>';
          $("#table2 tbody").empty();
          $("#table2 tbody").append(html);
            var total = summary_data.total;
          $("#week_total").text('$'+total['weekly_cost']);
          $("#month_total").text('$'+total['monthly_cost']);
          $("#quat_total").text('$'+total['quarterly_cost']);
          $("#year_total").text('$'+total['yearly_cost']);

          update_graph('loadchart');
    },
    error: function (jqXHR, textStatus, errorThrown){

        throw(errorThrown);
    }
  });
} /* reload graph */

function get_cost_increase_perc(perc_array,obj){
  var cost = 1;

  if(obj['purpose']=='Marketing')
    cost = perc_array[0]['marketing'];
  if(obj['purpose']=='Administration')
    cost = perc_array[0]['ac'];
  if(obj['purpose']=='Public-Relations')
    cost = perc_array[0]['pr'];
  if(obj['purpose']=='Other')
    cost = perc_array[0]['other'];

  return cost/100;
}

function add_expenses(){

  save_method = 'add';
  $('#form')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string
  $('#modal_form').modal('show'); // show bootstrap modal
  $('.modal-title').text('Add Expense'); // Set Title to Bootstrap modal title
}

function save(){

  $('#btnSave').text('saving...'); //change button text
  $('#btnSave').attr('disabled',true); //set button disable
  var url;

  if(save_method == 'add') {

    url = "<?php echo base_url(); ?>expenses/ajax_add";
  }else{

    url = "<?php echo base_url(); ?>expenses/ajax_update";
  }

  $.ajax({
      url : url,
      type: "POST",
      data: $('#form').serialize(),
      dataType: "JSON",
      success: function(data)
      {

          if(data.status) //if success close modal and reload ajax table
          {
              $('#modal_form').modal('hide');
              reload_table();
          }

          $('#btnSave').text('save'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable


      },
      error: function (jqXHR, textStatus, errorThrown)
      {
          alert('Error adding / update data');
          $('#btnSave').text('save'); //change button text
          $('#btnSave').attr('disabled',false); //set button enable

      }
  });
} /* end function save() */

function edit_expense(e_id){

  var id=e_id;
  save_method = 'update';
  $('#form')[0].reset(); // reset form on modals
  $('.form-group').removeClass('has-error'); // clear error class
  $('.help-block').empty(); // clear error string

  //Ajax Load data from ajax
  $.ajax({
    url : "<?php echo base_url(); ?>expenses/ajax_edit/" + id,
    type: "GET",
    dataType: "JSON",
    success: function(data)
    {
        $('[name="id"]').val(data.id);
        $('[name="description"]').val(data.description);
        $('[name="weekly_cost"]').val(data.weekly_cost);
        $('[name="monthly_cost"]').val(data.monthly_cost);
        $('[name="quarterly_cost"]').val(data.quarterly_cost);
        $('[name="yearly_cost"]').val(data.yearly_cost);
        $('[name="purpose"]').val(data.purpose);
        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
        $('.modal-title').text('Edit Expense'); // Set title to Bootstrap modal title
    },
    error: function (jqXHR, textStatus, errorThrown){

        throw(errorThrown);
    }
  });
} /* end function edit() */

function delete_expense(e_id){

  if(confirm('Are you sure delete this data?')){

    var id=e_id;
    // ajax delete data to database
    $.ajax({
        url : "<?php echo base_url(); ?>expenses/ajax_delete/"+id,
        type: "POST",
        dataType: "JSON",
        success: function(data)
        {
            //if success reload ajax table
            $('#modal_form').modal('hide');
            reload_table();
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error deleting data');
        }
    });
  }
} /* end function delete() */

function calculateCostFromYearly(){
  var yearly = parseInt($("#yearly_cost").val());
    if(!isNaN(yearly))
    {
      $("#quarterly_cost").val(Math.floor(yearly/4).toFixed(2));
      $("#weekly_cost").val(Math.floor(yearly/52).toFixed(2));
      $("#monthly_cost").val(Math.floor(yearly/12).toFixed(2));
    }
}

function calculateCostFromQuaterly(){
  var quarterly = parseInt($("#quarterly_cost").val());
    if(!isNaN(quarterly)){

        $("#yearly_cost").val(Math.floor(quarterly*4).toFixed(2));
        $("#weekly_cost").val(Math.floor(quarterly/13).toFixed(2));
        $("#monthly_cost").val(Math.floor(quarterly/3).toFixed(2));
    }
}

function calculateCostFromWeekly(){
  var weekly = parseInt($("#weekly_cost").val());
    if(!isNaN(weekly)){

      $("#yearly_cost").val(Math.floor(weekly*52).toFixed(2));
      $("#quarterly_cost").val(Math.floor(weekly*13).toFixed(2));
      $("#monthly_cost").val(Math.floor(weekly*4).toFixed(2));
    }


}
function calculateCostFromMonthly(){
  var monthly_cost = parseInt($("#monthly_cost").val());
    if(!isNaN(monthly_cost)){

      $("#yearly_cost").val(Math.floor(monthly_cost*12).toFixed(2));
      $("#weekly_cost").val(Math.floor(monthly_cost/4).toFixed(2));
      $("#quarterly_cost").val(Math.floor(monthly_cost*3).toFixed(2));
    }
}

$(function(){
  $('.next-step, .prev-step').on('click', function (e) {
    var $activeTab = $('.tab-pane.active');
    if ( $(e.target).hasClass('next-step') ){

      var nextTab = $activeTab.next('.tab-pane').attr('id');
      $('[href="#'+ nextTab +'"]').removeClass('btn-default');
      $('[href="#'+ nextTab +'"]').tab('show');
      $("body").scrollTop(0);

    }else{

      var prevTab = $activeTab.prev('.tab-pane').attr('id');
      $('[href="#'+ prevTab +'"]').removeClass('btn-default');
      $('[href="#'+ prevTab +'"]').tab('show');
      $("body").scrollTop(0);
    }
  });
  //redraw the datatables
  $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href") // activated tab
    if ((target == '#menu2')) {
       
        reload_table();
    }
  });
});

$('#table2 tbody').on( 'click', 'tr', function () {

  if ($(this).hasClass('selected')) {

      $(this).removeClass('selected');

  }else{
      $('#table2 tr.selected').removeClass('selected');
      $(this).addClass('selected');
  }
});

/*$('#saveResults').click(function(){
   $("#success_msg").text("Comment Saved successfully!");
});*/



  //$("#ex8").change(function(){
 $('#ex8').on("input change", function(){
   var sv = parseInt($("#ex8").val());
   $("#m_perc").text(sv);
   $("#ex8").val(sv);
   if($("#Marketing").length>0){

   var account = "Marketing";
   var years= [];
   years['year1'] = parseInt($("#Marketing_year1").attr('data-val'));
   years['year2'] = parseInt( years['year1'] * (sv/100)) + years['year1'];
   years['year3'] = parseInt( years['year2'] * (sv/100)) + years['year2'];

   $("#Marketing_year2").attr('data-val',years['year2']);
   $("#Marketing_year3").attr('data-val',years['year3']);
   $("#Marketing_year2").text('$'+years['year2']);
   $("#Marketing_year3").text('$'+years['year3']);
   calculate_total();
   update_graph(account);

   }

});
$('#ex9').on("input change", function(){
   var sv = parseInt($("#ex9").val());
   $("#p_perc").text(sv);
   $("#ex9").val(sv);
   if($("#Public-Relations").length>0)
   {
      var account = "Public-Relations";
      var years= [];
      years['year1'] = parseInt($("#Public-Relations_year1").attr('data-val'));
      years['year2'] = parseInt( years['year1'] * (sv/100)) + years['year1'];
      years['year3'] = parseInt( years['year2'] * (sv/100)) + years['year2'];

      $("#Public-Relations_year2").attr('data-val',years['year2']);
      $("#Public-Relations_year3").attr('data-val',years['year3']);
      $("#Public-Relations_year2").text('$'+years['year2']);
      $("#Public-Relations_year3").text('$'+years['year3']);
      calculate_total();
      update_graph(account);
  }
});
$('#ex10').on("input change", function(){
    var sv = parseInt($("#ex10").val());
    $("#a_perc").text(sv);
    $("#ex10").val(sv);
    if($("#Administration").length>0){
      var account = "Administration";
      var years= [];
      years['year1'] = parseInt($("#Administration_year1").attr('data-val'));
      years['year2'] = parseInt( years['year1'] * (sv/100)) + years['year1'];
      years['year3'] = parseInt( years['year2'] * (sv/100)) + years['year2'];

      $("#Administration_year2").attr('data-val',years['year2']);
      $("#Administration_year3").attr('data-val',years['year3']);
      $("#Administration_year2").text('$'+years['year2']);
      $("#Administration_year3").text('$'+years['year3']);
      calculate_total();
      update_graph(account);
    }
});
$('#ex11').on("input change", function(){
    var sv = parseInt($("#ex11").val());
    $("#o_perc").text(sv);
    $("#ex11").val(sv);
    if($("#Other").length>0){
      var account = "Other";
      var years= [];
      years['year1'] = parseInt($("#Other_year1").attr('data-val'));
      years['year2'] = parseInt( years['year1'] * (sv/100)) + years['year1'];
      years['year3'] = parseInt( years['year2'] * (sv/100)) + years['year2'];

      $("#Other_year2").attr('data-val',years['year2']);
      $("#Other_year3").attr('data-val',years['year3']);
      $("#Other_year2").text('$'+years['year2']);
      $("#Other_year3").text('$'+years['year3']);
      calculate_total();
      update_graph(account);
    }
});

$('#save').click(function(){
  var m_perc = $("#ex8").val();
  var p_perc = $("#ex9").val();
  var a_perc = $("#ex10").val();
  var o_perc = $("#ex11").val();

  $.ajax({
    type: 'GET',
    url: "<?php echo site_url('Expenses/updateExpensesIncrease'); ?>?m="+m_perc+"&p="+p_perc+"&a="+a_perc+"&o="+o_perc,

    success:
      function(data){
        calculate_total();
      }
  });


});

function calculate_total(){

  var total_y1 = 0;
  var total_y2 = 0;
  var total_y3 = 0;

  total_y1= parseFloat( ($("#Marketing_year1").length>0) ? $("#Marketing_year1").attr('data-val'):0.00 ) + parseFloat( ($("#Public-Relations_year1").length>0) ? $("#Public-Relations_year1").attr('data-val'):0.00 ) + parseFloat( ($("#Administration_year1").length>0) ? $("#Administration_year1").attr('data-val'):0.00 ) + parseFloat( ($("#Other_year1").length>0) ? $("#Other_year1").attr('data-val'):0.00 );
  total_y2= parseFloat( ($("#Marketing_year2").length>0) ? $("#Marketing_year2").attr('data-val'):0.00 ) + parseFloat( ($("#Public-Relations_year2").length>0) ? $("#Public-Relations_year2").attr('data-val'):0.00 ) + parseFloat( ($("#Administration_year2").length>0) ? $("#Administration_year2").attr('data-val'):0.00 ) + parseFloat( ($("#Other_year2").length>0) ? $("#Other_year2").attr('data-val'):0.00 );
  total_y3= parseFloat( ($("#Marketing_year3").length>0) ? $("#Marketing_year3").attr('data-val'):0.00 ) + parseFloat( ($("#Public-Relations_year3").length>0) ? $("#Public-Relations_year3").attr('data-val'):0.00 ) + parseFloat( ($("#Administration_year3").length>0) ? $("#Administration_year3").attr('data-val'):0.00 ) + parseFloat( ($("#Other_year3").length>0) ? $("#Other_year3").attr('data-val'):0.00 );

  $("#total_y1").attr('data-val',total_y1.toFixed(2));
  $("#total_y2").attr('data-val',total_y2.toFixed(2));
  $("#total_y3").attr('data-val',total_y3.toFixed(2));

  $("#total_y1").text('$'+total_y1.toFixed(2));
  $("#total_y2").text('$'+total_y2.toFixed(2));
  $("#total_y3").text('$'+total_y3.toFixed(2));
}
  </script>