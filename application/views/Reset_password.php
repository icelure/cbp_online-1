<div class="container">
  <!-- Content Header (Page header) -->
  <section class="content-header">
  <h1>Reset Password</h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url().'User_dashboard';?>"><i class="fa fa-dashboard"></i> Home</a></li>
    <li class="active">Reset Password</li>
  </ol>
  </section>

<style type="text/css">

#error {
    display: inline-block;
    width: 30em;
    margin-right: .5em;
    padding-top: 1px;
    color: red;
}
</style>


  <!-- Main content -->
  <section class="content">
    <div class="col-md-12">
      <div class="box box-primary">
        <div class="box-header with-border">
            <?php
              $flashdata= $this->session->flashdata('response');
              if(!empty($flashdata)){
                if($flashdata['status'] == 'success'){
            ?>
                  <div class="callout callout-success">
                    <?php echo $flashdata['message']; ?>
                  </div>
            <?php
                }
                if($flashdata['status'] == 'failed'){
            ?>
                <div class="callout callout-danger">
                  <?php echo $flashdata['message']; ?>
                </div>
            <?php
                }
              }
            ?>
            <p id="gStartTd">Reset your password</p>

        </div>
        <?php echo form_open(base_url().'login', 'method="post"');?>
        <div class="box-body no-padding">
            <div class="col-md-6" id="login_cont">
              <div class="form-group">
                  <div class="input-group">
                  <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                  </div>
                  <input type="password" name="new_password" class="form-control" placeholder="New Password">
                  </div>

                  <label id="error"><?php echo $this->session->flashdata('new_pass');?></label>
              </div>
              <div class="form-group">
                  <div class="input-group">
                  <div class="input-group-addon">
                        <i class="fa fa-key"></i>
                  </div>
                  <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password">
                  </div>

                  <label id="error"><?php echo $this->session->flashdata('confirm_pass');?></label>
              </div>
              <div class="form-group">
                <input type="hidden" name="email" value="<?php echo $email; ?>">
                <input type="hidden" name="token" value="<?php echo $token; ?>">
                <input type="hidden" name="rpForm" value="postrpForm">
                <button type="submit" name="btnChgPass" id="btnChgPass" class="btnStyle btn btn-success" style="width:200px;">
                  <b>Reset Password</b> <span class="glyphicon glyphicon-play" style="top:2px"></span>
                </button>
              </div>
              <?php echo form_close(); ?>
            </div>

            <div class="col-md-6 pull-right" >
              <span class="information">
                <p class="pHeader"><img src="<?=asset_url()?>/img/key.png" alt="">Easy registration</p>
                <p class="pContent">Companies plan every day so sign up and start writing your Business Plan today ! </p>

                <p class="pHeader"><img src="<?=asset_url()?>/img/import.png" alt=""> Responsive desigen</p>
                <p class="pContent">Business Planning on the go available on all devices.</p>

                <p class="pHeader"><img src="<?=asset_url()?>/img/area_chart.png" alt=""> Powerful and easy to use</p>
                <p class="pContent">No need for time consuming spreadsheets,let CBP Online do all your calcuations automaticaly</p>

                <p class="pHeader"><img src="<?=asset_url()?>/img/approval.png" alt=""> Printing and email ready</p>
                <p class="pContent">Impress your bank or potential investors with a profesional and realistic Business Plan</p>
              </span>
            </div>
          </div>
          <div class="box-footer">

          </div>
        </div>
      </div>
    </div>
  </section>

</div><!-- /.container -->