<div class="container">
          <!-- Content Header (Page header) -->
          <section class="content-header">
          <h1>
              Sign UP
              <small>@ CBPOnline</small>
            </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Sign UP</li>
            </ol>
          </section>

          <!-- Main content -->
          <section class="content">
          
          <?php  
          $errors = validation_errors();
          if($errors !== ""){ ?>
          <div class="callout callout-danger">
              <?php echo $errors; ?>
            </div>

          <?php } ?>

          <?php
            $flashdata= $this->session->flashdata('response');
            if(!empty($flashdata)){
              if($flashdata['status'] == 'success'){
                ?>
                <div class="callout callout-success">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
              if($flashdata['status'] == 'failed'){
                ?>
                <div class="callout callout-danger">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
            }
          ?>
          
            <div class="register-box"> <div class="register-logo">
              <a href="javscript:void(0);"><b>Create an account</a>

            </div>
            <div class="register-box-body">
              <p class="login-box-msg">Register a new membership</p>
              <form action="<?php echo base_url() ?>register" method="post">
                <div class="form-group has-feedback">
                  <input type="text" class="form-control" name="name" id="name" placeholder="Full name">
                  <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="email" class="form-control" name="cemail" id="cemail" placeholder="Confirm Email">
                  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                  <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                  <input type="password" class="form-control" name="cpassword" id="cpassword" placeholder="Confirm Password">
                  <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
                </div>
                <div class="row">
                  <div class="col-xs-8">
                    
                  </div><!-- /.col -->
                  <div class="col-xs-4">
                    <input type="hidden" name="registerForm" value="postForm">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
                  </div><!-- /.col -->
                </div>
              </form>
              <a href="<?php echo  base_url(); ?>login" class="text-center">I already have a membership</a>


<div class="box-body">



<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business. The highest level of planning is developing a strategic plan. Strategic planning is one of the most important functions of a company's leaders.</p>


<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Companies plan every day. From how many employees a retail store might need on a given day to how a Fortune 500 company will invest its profits, Planning is one of the fundamental functions of business. The highest level of planning is developing a strategic plan. Strategic planning is one of the most important functions of a company's leaders.</p>


<h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this moduel,you will start by setting up your comapany details,general business,payroll,and imports  assumptions,click on the 'Next' button to continue,please note that all values must be filled in for you to move to the next view</p>


</div><!-- /.box-body -->













            </div><!-- /.form-box -->
          </div><!-- /.register-box -->
        </section><!-- /.content -->
        </div><!-- /.container -->















