<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">-->
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/iCheck/all.css">
<style type="text/css">
.process-step .btn:focus{outline:none}
.process{display:table;width:100%;position:relative}
.process-row{display:table-row}
.process-step button[disabled]{opacity:1 !important;filter: alpha(opacity=100) !important}
.process-row:before{top:40px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}
    .process-step{display:table-cell;text-align:center;position:relative}
    .process-step p{margin-top:4px}
    .btn-circle{width:65px;height:65px;text-align:center;font-size:12px;border-radius:50%}
    .tab-content{margin: 0 10% 0 10%;}
    .error{color:rgba(255, 0, 0, 0.62);}
    .input-group{width: 100%}
    .img-circle {
        border-radius: 50%;
        
    }
    .input-group-addon{
    border:none;
        
    }
    
}
tr.selected {
    background-color: #B0BED9 !important;
}
}
h1,p{
color:white;
}
.bordered th, .bordered td{
padding:10px;
}
.bordered tbody tr:nth-child(odd){
background:#eee;
color:#000;
}
.bordered tbody tr:nth-child(even){
color:#fff;
    
}


</style>
<section class="content-header">

<h1>

Revenue By Products

<!-- <small>advanced tables</small> -->

</h1>

<ol class="breadcrumb">

<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>

<li><a href="#">Revenu By Products</a></li>

<!-- <li class="active">Data tables</li> -->

</ol>

</section>
<section class="content">

<div class="row">

<div class="process">

<div class="process-row nav nav-tabs">

<div class="process-step">

<button type="button" class="btn btn-info btn-circle" data-toggle="tab" href="#menu0"><i class="fa fa-rocket fa-3x"></i></button>

<p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>About<br />Products</strong></p>

</div>

<div class="process-step">

<button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu1"><i class="fa fa-info fa-3x"></i></button>

<p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>Local Products</strong></p>

</div>
<!--                <div class="process-step">

<button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu2"><i class="fa fa-file-text-o fa-3x"></i></button>

<p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;"><strong>Imported Products</strong></p>

</div>-->

<div class="process-step">

<button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu3"><i class="fa fa-image fa-3x"></i></button>

<p  style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;" ><strong>Summary</strong></p>

</div>

<div class="process-step">

<button type="button" class="btn btn-default btn-circle" data-toggle="tab" href="#menu4"><i class="fa fa-check fa-3x"></i></button>

<p  style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; padding: 0px;" ><strong>Done</strong></p>

</div>

</div>

</div>

<div class="tab-content">
<!-- Menu0 getStarted Start -->
<div id="menu0" class="tab-pane fade active in">
<div class="box box-solid"style="background-color: rgba(235, 244, 249, 0.10); border: 0px solid blue; padding: 0px;">
<div class="box-header with-border">
<h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;"><strong>What is Revenue By Products ?</strong></h3>
<ul class="list-unstyled list-inline pull-right">
<li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
</ul>
<h2 style="color:#3c8dbc;font-size:22px;font-weight:light;"></h2>
</div><!-- /.box-header -->
<div class="box-body">

<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">If you are establsihing a company that will be selling products,Revenue By products is  the total amount of revenue received by the company for the sale of products over a period of time,usualy calculated on a weekly bases.</p>


<h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">Creating Products</strong></h3>
<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this module,you you can add a product,add an image for the product, view a slide show of all your product, and calculate cost and projected revenue based oon a percntage markup on cost.Values are automaticaly calculated and posted to the relevant documents for easy printing and reporting.</p>

<h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Click on the Next button to start creating your products for evaluation.</p>


</div><!-- /.box-body -->
</div>
<ul class="list-unstyled list-inline pull-right">
<li><button type="button" class="btn btn-info next-step">Next <i class="fa fa-chevron-right"></i></button></li>
</ul>

</div>
<!-- Menu1 local product Start -->
<div id="menu1" class="tab-pane fade in">
<!-- Menu One End -->
<div class="box box-warning">
<div class="box-header with-border">
<h3 class="box-title">Local Products</h3>

















<-div class="container">
<div class="well well-sm">
<strong>Display</strong>
<div class="btn-group">
<a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list">
</span>List</a> <a href="#" id="grid" class="btn btn-default btn-sm"><span
class="glyphicon glyphicon-th"></span>Grid</a>
</div>
</div>
<div id="products" class="row list-group">
<div class="item  col-xs-4 col-lg-4">
<div class="thumbnail">
<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
<div class="caption">
<h4 class="group inner list-group-item-heading">
Product title</h4>
<p class="group inner list-group-item-text">
Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
<div class="row">
<div class="col-xs-12 col-md-6">
<p class="lead">
$21.000</p>
</div>
<div class="col-xs-12 col-md-6">
<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
</div>
</div>
</div>
</div>
</div>
<div class="item  col-xs-4 col-lg-4">
<div class="thumbnail">
<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
<div class="caption">
<h4 class="group inner list-group-item-heading">
Product title</h4>
<p class="group inner list-group-item-text">
Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
<div class="row">
<div class="col-xs-12 col-md-6">
<p class="lead">
$21.000</p>
</div>
<div class="col-xs-12 col-md-6">
<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
</div>
</div>
</div>
</div>
</div>
<div class="item  col-xs-4 col-lg-4">
<div class="thumbnail">
<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
<div class="caption">
<h4 class="group inner list-group-item-heading">
Product title</h4>
<p class="group inner list-group-item-text">
Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
<div class="row">
<div class="col-xs-12 col-md-6">
<p class="lead">
$21.000</p>
</div>
<div class="col-xs-12 col-md-6">
<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
</div>
</div>
</div>
</div>
</div>
<div class="item  col-xs-4 col-lg-4">
<div class="thumbnail">
<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
<div class="caption">
<h4 class="group inner list-group-item-heading">
Product title</h4>
<p class="group inner list-group-item-text">
Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
<div class="row">
<div class="col-xs-12 col-md-6">
<p class="lead">
$21.000</p>
</div>
<div class="col-xs-12 col-md-6">
<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
</div>
</div>
</div>
</div>
</div>
<div class="item  col-xs-4 col-lg-4">
<div class="thumbnail">
<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
<div class="caption">
<h4 class="group inner list-group-item-heading">
Product title</h4>
<p class="group inner list-group-item-text">
Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
<div class="row">
<div class="col-xs-12 col-md-6">
<p class="lead">
$21.000</p>
</div>
<div class="col-xs-12 col-md-6">
<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
</div>
</div>
</div>
</div>
</div>
<div class="item  col-xs-4 col-lg-4">
<div class="thumbnail">
<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />
<div class="caption">
<h4 class="group inner list-group-item-heading">
Product title</h4>
<p class="group inner list-group-item-text">
Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.</p>
<div class="row">
<div class="col-xs-12 col-md-6">
<p class="lead">
$21.000</p>
</div>
<div class="col-xs-12 col-md-6">
<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>







<div class="row">
<div class="col-md-12" id="expense_table"></div>
</div>
</div>
</div>
<ul class="list-unstyled list-inline pull-right">
<li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
<li><button type="button" class="btn btn-info next-step" id="summary_section">Next <i class="fa fa-chevron-right"></i></button></li>
</ul>
</div>
<!-- Menu Three End -->
<!-- Menu4 Summary Start -->
<div id="menu4" class="tab-pane fade">

<div class="box box-solid"style="background-color: rgba(235, 244, 249, 0.10); border: 0px solid blue; padding: 0px;">




<div class="box-header with-border">
<h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:light;"><strong>You have completed setting up your products</strong></h3>



<ul class="list-unstyled list-inline pull-right">
<li><a class="btn btn-primary view-pdf pull-right" href="<?php echo asset_url(); ?>HTML/ExpensesHelp.html">Help</a></li>
</ul>


<h2 style="color:#3c8dbc;font-size:22px;font-weight:light;"></h2>

</div><!-- /.box-header -->
<div class="box-body">



<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">For a company, this is the total amount of revenue received by the company for services rendered fo s specific period of time </p>

<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">Personal Services Income (PSI) is income mainly derived from an individual’s personal efforts and skill.Generally, consultants and contractors operate as a sole trader or work through a company,that charges fees on an hourly basis or a fixes job rate. </p>

<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this moduel , you can enter a service, based on a number of hour your worded during w weekly cycle, and wnter the rate per hour that you may chager ad you can include a call out fee fro this service .  </p>

<h3 class="box-title"style="color:#3c8dbc;font-size:22px;font-weight:bold;">What's next </strong></h3>
<p style="color:rgba(0, 0, 0, 1.0);font-size:14px;font-weight:light;">In this Module,you can enter a service,based on a number of hours you worked during a weekly cycle,and the rate per hour that you charge your customer, you can also include a call out fee for this service.Click on the Projected Service Income to enter your service or services that you intend to provide</p>






</div><!-- /.box-body -->

</div>




<ul class="list-unstyled list-inline pull-right">
<li><button type="button" class="btn btn-default prev-step"><i class="fa fa-chevron-left"></i> Back</button></li>
<li><button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Done!</button></li>
</ul>
</div>

</div>

</div>

</section><!-- /.content -->


<!-- Main content -->
<script src="<?php echo asset_url(); ?>plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?php echo asset_url(); ?>plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo asset_url(); ?>plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript">









$(function () {
  
  //$('#start_date').daterangepicker({singleDatePicker: true});
  $("[data-mask]").inputmask();
  $('.btn-circle').on('click', function () {
                      $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                      $(this).addClass('btn-info').removeClass('btn-default').blur();
                      });
  
  /*        $('input[type="radio"].minimal').iCheck({
   radioClass: 'iradio_minimal-blue'
   });*/
  
  //        $('input:radio').change(function () {
  //            if ($(this).val() == 'p') {
  //                $('i[data-rd-id="' + $(this).attr('name') + '"]').removeClass('fa-dollar').addClass('fa-percent');
  //
  //            } else {
  //                $('i[data-rd-id="' + $(this).attr('name') + '"]').removeClass('fa-percent').addClass('fa-dollar');
  //            }
  //
  //        });
  
  //        $("#company_setting_form").validate({
  //            rules: {
  //                company_name: "required",
  //                abn_no: "required",
  //                start_date: "required",
  //                street_no: "required",
  //                street_name: "required",
  //                suburb: "required",
  //                state: "required",
  //                zipcode: "required",
  //                country: "required",
  //                fax: {
  //                    number: true
  //                },
  //                company_email: {
  //                    required: true,
  //                    email: true
  //                },
  //                website: {
  //                    required: true,
  //                    url: true
  //                },
  //                company_tax: {
  //                    required: true
  //                },
  //                company_vat: {
  //                    required: true
  //                },
  //                opening_cash_balance: {
  //                    required: true
  //                },
  //                opening_debtors_balance: {
  //                    required: true
  //                },
  //                closing_creditors_balance: {
  //                    required: true
  //                },
  //                sales_income_increase: {
  //                    required: true
  //                },
  //                services_income: {
  //                    required: true
  //                },
  //                sales_cost_increase: {
  //                    required: true
  //                },
  //                service_cost_increase: {
  //                    required: true
  //                },
  //                marketing_increase: {
  //                    required: true
  //                },
  //                public_reactions: {
  //                    required: true
  //                },
  //                administration_cost: {
  //                    required: true
  //                },
  //                depreciation_on_equipment: {
  //                    required: true
  //                },
  //                import_duty: {
  //                    required: true
  //                },
  //                delivery_order: {
  //                    required: true
  //                },
  //                cmr_comp_fee: {
  //                    required: true
  //                },
  //                lcl_transport_fee: {
  //                    required: true
  //                },
  //                cargo_auto_fee: {
  //                    required: true
  //                },
  //                port_service_fee: {
  //                    required: true
  //                },
  //                custom_clearance_fee: {
  //                    required: true
  //                },
  //                transport_fues_fee: {
  //                    required: true
  //                },
  //                aqis_fee: {
  //                    required: true
  //                },
  //                insurance_fee: {
  //                    required: true
  //                },
  //                dec_processing_fee: {
  //                    required: true
  //                },
  //                misc_fee: {
  //                    required: true
  //                },
  //                payg_tax: {
  //                    required: true
  //                },
  //                superannuation: {
  //                    required: true
  //                },
  //                work_cover: {
  //                    required: true
  //                },
  //                union_fee: {
  //                    required: true
  //                },
  //                holiday_pay: {
  //                    required: true
  //                },
  //                sick_leave: {
  //                    required: true
  //                },
  //                long_service_fee: {
  //                    required: true
  //                },
  //                payrate_increase: {
  //                    required: true
  //                }
  //            }
  //        });
  
  $('.next-step, .prev-step').on('click', function (e) {
                                 //            section_id = $(this).attr('id');
                                 //
                                 //            if (section_id == 'comapny_detail_section') {
                                 //                $("label.error").remove();
                                 //                abn_valid = $("#abn_no").valid();
                                 //                cn_valid = $("#company_name").valid();
                                 //                sn_valid = $("#street_name").val();
                                 //                sno_valid = $("#street_no").valid();
                                 //                stdate_valid = $("#start_date").valid();
                                 //                sub_valid = $("#suburb").valid();
                                 //                state_valid = $("#state").valid();
                                 //                zp_valid = $("#zipcode").valid();
                                 //                cntr_valid = $("#country").valid();
                                 //                tph_valid = $("#telephone").valid();
                                 //                fx_valid = $("#fax").valid();
                                 //                cmp_valid = $("#company_email").valid();
                                 //                wb_valid = $("#website").valid();
                                 //
                                 //
                                 //                if (abn_valid && sn_valid && sno_valid && stdate_valid && sub_valid && state_valid && zp_valid && cntr_valid && tph_valid && fx_valid && cmp_valid && wb_valid && cn_valid) {
                                 //
                                 //                } else {
                                 //                    return false;
                                 //                }
                                 //
                                 //            }
                                 //
                                 //            if (section_id == 'general_assumption_section') {
                                 //                $("label.error").remove();
                                 //                ctx_valid = $("#company_tax").valid();
                                 //                cvt_valid = $("#company_vat").valid();
                                 //                ocb_valid = $("#opening_cash_balance").valid();
                                 //                odb_valid = $("#opening_debtors_balance").valid();
                                 //                ccb_valid = $("#closing_creditors_balance").valid();
                                 //                sii_valid = $("#sales_income_increase").valid();
                                 //                sri_valid = $("#services_income").valid();
                                 //                slc_valid = $("#sales_cost_increase").valid();
                                 //                sci_valid = $("#service_cost_increase").valid();
                                 //                mi_valid = $("#marketing_increase").valid();
                                 //                pr_valid = $("#public_reactions").valid();
                                 //                ac_valid = $("#administration_cost").valid();
                                 //                doe_valid = $("#depreciation_on_equipment").valid();
                                 //
                                 //
                                 //                if (ctx_valid && cvt_valid && ocb_valid && odb_valid && ccb_valid && sii_valid && sri_valid && slc_valid && sci_valid && mi_valid && pr_valid && ac_valid && doe_valid) {
                                 //
                                 //                } else {
                                 //                    console.log("asd");
                                 //                    return false;
                                 //                }
                                 //            }
                                 //
                                 //            if (section_id == 'import_cost_duty_section') {
                                 //                $("label.error").remove();
                                 //                inputboxes = $("#menu3").find('input[type="text"]');
                                 //                var flag = 0;
                                 //                $.each(inputboxes, function (index, val) {
                                 //
                                 //                    if ($("#" + this.id).valid()) {
                                 //
                                 //                    } else {
                                 //                        console.log("asd");
                                 //                        flag = flag + 1;
                                 //                    }
                                 //                });
                                 //                if (flag >= 1) {
                                 //                    return false;
                                 //                }
                                 //            }
                                 
                                 //            if (section_id == 'australion_payroll_section') {
                                 //                $("label.error").remove();
                                 //                inputboxes = $("#menu4").find('input[type="text"]');
                                 //                var flag = 0;
                                 //                $.each(inputboxes, function (index, val) {
                                 //
                                 //                    if ($("#" + this.id).valid()) {
                                 //
                                 //                    } else {
                                 //
                                 //                        flag = flag + 1;
                                 //                    }
                                 //                });
                                 //                if (flag >= 1) {
                                 //                    return false;
                                 //                }
                                 //            }
                                 
                                 
                                 var $activeTab = $('.tab-pane.active');
                                 
                                 $('.btn-circle.btn-info').removeClass('btn-info').addClass('btn-default');
                                 
                                 if ($(e.target).hasClass('next-step'))
                                 {
                                 var nextTab = $activeTab.next('.tab-pane').attr('id');
                                 $('[href="#' + nextTab + '"]').addClass('btn-info').removeClass('btn-default');
                                 $('[href="#' + nextTab + '"]').tab('show');
                                 } else
                                 {
                                 var prevTab = $activeTab.prev('.tab-pane').attr('id');
                                 $('[href="#' + prevTab + '"]').addClass('btn-info').removeClass('btn-default');
                                 $('[href="#' + prevTab + '"]').tab('show');
                                 }
                                 });
  });
</script>

