<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content-header">
  <h1>
    Manage Plans
    <!-- <small>advanced tables</small> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Plans</a></li>
    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table With Full Features</h3>
        </div>/.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>Type</th>
                <th>Title</th>
                <th>Subtitle</th>
                <th>Price</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <?php if(!empty($plans)) { 

              foreach ($plans as $plan) { ?>
               <tr>
                <td><?php print $plan['id']; ?></td>
                <td><?php print $plan['type']; ?></td>
                <td><?php print $plan['title']; ?></td>
                <td><?php print $plan['subtitle']; ?></td>
                <td><?php print $plan['price']; ?></td>
                <td><a href="<?php echo base_url(); ?>manage_plans/<?php echo $plan['type']; ?>" class="btn bg-olive">Edit</a></td>
                
              </tr>
             <?php } 
                }
             ?>
              </tbody>
              <tfoot>
                <th>ID</th>
                <th>Type</th>
                <th>Title</th>
                <th>Subtitle</th>
                <th>Price</th>
                <th>Action</th>
              </tfoot>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
  <script src="<?php echo  asset_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo  asset_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
  <script>
    $(function () {
      $('#example1').DataTable();
    });



  </script>