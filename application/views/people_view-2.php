<div class="well well-sm col-sm-12 col-md-12 col-lg-12">
    <strong>Display</strong>
        <div class="btn-group">
            <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a>
            <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
        </div>
    <button style="float: right;"  class="btn btn-success" onclick="add_person()"><i class="glyphicon glyphicon-plus"></i>Personal</button>
</div>
<div id="personal" class="row-height list-group col-sm-12 col-md-12 col-lg-12">

</div>
<script src="<?php echo base_url('assets/plugins/easypaginate/easyPaginate.js') ?>"></script>

<style>

.easyPaginateNav a {
    font-size: 18px;
    padding: 5px 10px;
    font-weight: bold;
    color: white;
    margin-left: 5px;
    background-color: #3c8dbc;
}
.easyPaginateNav{
    width: auto!important;
    text-align: left;
    clear: both;
}
.easyPaginateNav a.current {
    font-weight:bold;
    text-decoration:underline;}

.itemThumbnail{
    height:165px;
    margin-bottom:0px;
}
.itemFooter{
    padding: 5px;
    border: solid 1px #e6e6e6;
    border-top: none;
    height: 40px;
}
.itemInfo{
    border: solid 1px #e6e6e6;
    border-top: none;
    padding-left: 10%;
    height: 278px;
}


.thumbnail
{
    /*margin-bottom: 20px;*/
    padding: 0px;
    -webkit-border-radius: 0px;
    -moz-border-radius: 0px;
    border-radius: 0px;
}
.item.list-group-item
{
    float: none;
    width: 100%;
    background-color: #fff;
    margin-bottom: 10px;
}
.item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
{
    /*background: #428bca;*/
    background: #e2effb;
}
.item.list-group-item .list-group-image
{

}
.item.list-group-item .thumbnail
{
    margin-bottom: 0px;
}
.item.list-group-item .caption
{
    padding: 9px 9px 0px 9px;
}
.item.list-group-item:nth-of-type(odd)
{
    background: #eeeeee;
}

.item.list-group-item:before, .item.list-group-item:after
{
    display: table;
    content: " ";
}

.item.list-group-item img
{
    float: left;
}
.item.list-group-item:after
{
    clear: both;
}
.list-group-item-text
{
    margin: 0 0 11px;
}
.easyPaginateNav {
    max-width:100%;
    text-align:left !important;
}

.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
    color: black;
}

</style>

<script type="text/javascript">

// Validations
$(function () {
    $("#personform").validate({
        ignore: "",
        rules: {
            person_fname: {
                required: true
            },
            person_lname: {
                required: true,
            },
            person_work_hour: {
                required: true,
                number: true
            },
            person_rate: {
                required: true,
                number: true
            },
            person_subsidi: {
                number: true
            },
            person_commission: {
                number: true
            },
            person_other: {
                number: true
            },
            person_pension: {
               number: true
            },
            person_medicare: {
               number: true
            },
            person_retire: {
               number: true
            },
            person_tax: {
               number: true
            },
            person_union: {
               number: true
            },
            person_sick: {
               number: true
            },
            person_fringe: {
               number: true
            },
            person_deductions: {
               number: true
            },
            person_superannuation: {
               number: true
            },
            person_workcover: {
               number: true
            }


        }
    });
});

var save_method; //for save method string
var table;

$(document).ready(function () {
    drawCollectionView();

    $('#list').click(function (event) {
        event.preventDefault();
        $('#personal .item').removeClass('grid-group-item');
        $('#personal .item').addClass('list-group-item');
    });
    $('#grid').click(function (event) {
        event.preventDefault();
        $('#personal .item').removeClass('list-group-item');
        $('#personal .item').addClass('grid-group-item');
    });
    //datatables

    $("#btnSavePerson").click(function (e) {
        e.preventDefault();
        if ($("#personform").valid()) {
            saveperson();
        }
    });

});

function drawCollectionView()
{

    $.ajax({
        url: "<?php echo site_url('people/ajax_list') ?>/",
        type: "GET",
        dataType: "JSON",
        success: function (data)
        {
            loadcollectionview(data);
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert(errorThrown);
        }
    });
}


function loadcollectionview(person_data) {

var html = "";
$(person_data.data).each(function (key, val) {
    html += '<div class="item  col-sm-12 col-md-6 col-lg-4">'
            + '<div class="thumbnail itemThumbnail">'
                + '<img style="height:95%" class="group list-group-image img-responsive" src="' + this["thumbnail"] + '" alt="" />'
            + '</div>'
            + '<div class="itemInfo caption">'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 txt-bold">'
                        + '<label style="font-weight:600;">First Name:</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["f_name"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Last Name:</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["l_name"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Hour Worked:</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["hour_worked"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Rates / Hour :</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["rates"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Subsidies :</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["subsidies"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Commission :</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["commission"] + '</div>'
                + '</div>'
                + '<div class="row">'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">'
                        + '<label style="font-weight:600;">Other :</label>'
                    + '</div>'
                    + '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["other"] + '</div>'
                + '</div>'
            + '</div>'
            + '<div class="itemFooter" style="margin-bottom:10px;" >'
                + '<a  style="margin-left: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Edit" onclick="edit_people(' + this["id"] + ')">'
                    + '<i class="glyphicon glyphicon-pencil"></i>'
                + '</a>'
                + '<a  class="btn btn-sm btn-danger pull-right" href="javascript:void(0)" title="Delete" onclick="delete_people(' + this["id"] + ')">'
                    + '<i class="glyphicon glyphicon-trash"></i>'
                + '</a>'
                + '<a  style="margin-right: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Information" onclick="loaddetail(' + this["id"] + ')">'
                    + '<i class="glyphicon glyphicon-exclamation-sign"></i>'
                + '</a>'
            + '</div>'
         + '</div>';
});
$('#personal').html(html);
// code for pagination
$('#personal').easyPaginate({
    paginateElement: '.item',
    elementsPerPage: 9,
    firstButton: true,
    lastButton: true
            // effect: 'climb'
});
$('.item-desc').each(function () {
    if ($(this).text().length > 130) {
        $(this).text($(this).text().substring(0, 130) + "...");
    }
});
}

function add_person()
{
    $("label.error").remove();
    save_method = 'add';
    $('#personform')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#personmodal_form').modal('show'); // show bootstrap modal
    $('#personmodal_form').on('shown.bs.modal', function() {
        $('.nav a[href="#form1"]').tab('show');
    })
    $('.modal-title').text('Add People'); // Set Title to Bootstrap modal title
}

function saveperson()
{
    $('#btnSavePerson').text('saving...'); //change button text
    $('#btnSavePerson').attr('disabled', true); //set button disable
    var url;

    if (save_method == 'add') {
        url = "<?php echo site_url('people/ajax_addperson');?>";
    } else {
        url = "<?php echo site_url('people/ajax_updateperson') ?>";
    }


    // ajax adding data to database
    $.ajax({
        url: url,
        type: "POST",
        data: new FormData($('#personform')[0]),
        contentType: false,
        cache: false,
        processData: false,
        success: function (data)
        {

            var obj = jQuery.parseJSON(data);

            if (obj['status']) //if success close modal and reload ajax table
            {
                $('#personmodal_form').modal('hide');
                drawCollectionView()
            }

            $('#btnSavePerson').text('save'); //change button text
            $('#btnSavePerson').attr('disabled', false); //set button enable
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error adding / update data');
            $('#btnSavePerson').text('save'); //change button text
            $('#btnSavePerson').attr('disabled', false); //set button enable

        }
    });
}

function delete_people(id){

    if (confirm('Are you sure delete this data?')){
            // ajax delete data to database
        $.ajax({
            url: "<?php echo site_url('people/ajax_delete') ?>/" + id,
            type: "POST",
            dataType: "JSON",
            success: function (data)
            {
                //if success reload ajax table
                $('#modal_form').modal('hide');
                drawCollectionView();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert(errorThrown);

            }
        });

    }
}

function edit_people(id){

    $("label.error").remove();
    save_method = 'update';
    $('#personform')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url: "<?php echo site_url('people/ajax_edit/') ?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data)
        {
            $('[name="personid"]').val(data.id);
            $('[name="person_fname"]').val(data.f_name);
            $('[name="person_lname"]').val(data.l_name);
            $('[name="person_hour_work"]').val(data.hour_worked);
            $('[name="person_rates"]').val(data.rates);
            $('[name="person_subsidi"]').val(data.subsidies);
            $('[name="person_commission"]').val(data.commission);
            $('[name="person_other"]').val(data.other);
            $('[name="person_pension"]').val(data.pension);
            $('[name="person_medicare"]').val(data.medicare_levi);
            $('[name="person_retire"]').val(data.retirement_annuity);
            $('[name="person_tax"]').val(data.income_tax);
            $('[name="person_union"]').val(data.union_fee);
            $('[name="person_sick"]').val(data.sick_leave);
            $('[name="person_fringe"]').val(data.fringe_benefit);
            $('[name="person_deductions"]').val(data.other_deduction);
            $('[name="person_superannuation"]').val(data.superannuation);
            $('[name="person_workcover"]').val(data.work_cover);
            $('#personmodal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit People'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
           alert(errorThrown);
       }
    });
}

function loaddetail(id){

    //Ajax Load data from ajax
    $.ajax({
        url: "<?php echo site_url('people/ajax_get_personal_detail') ?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function (data)
        {

            $("#imgthumbnail").attr("src", data.thumbnail);
            $("#person_fname").html(data.f_name);
            $("#person_lname").html(data.l_name);
            $("#personid").html(data.id);
            $("#person_hour_work").html(data.hour_worked);
            $("#person_rates").html(data.rates);
            $("#weekly_salary").html(data.weekly_salary);
            $("#commission").html(data.commission);
            $("#commission_recieved").html(data.commission_recieved);
            $("#subsidies").html(data.subsidies);
            $("#subsidies_recieved").html(data.subsidies_recieved);
            $("#other").html(data.other);
            $("#other_recieved").html(data.other_recieved);
            $("#total_gross_salary").html(data.total_gross_salary);

            //tab3
            $("#gross_salary").html(data.total_gross_salary);
            $("#pension").html(data.pension);
            $("#pension_d").html(data.pension_d);
            $("#medicare").html(data.medicare);
            $("#medicare_d").html(data.medicare_d);
            $("#retirement").html(data.retirement_annuity);
            $("#retirement_d").html(data.retirement_annuity_d);
            $("#income_tax").html(data.income_tax);
            $("#income_tax_d").html(data.income_tax_d);
            $("#union_fee").html(data.union_fee);
            $("#union_fee_d").html(data.union_fee_d);
            $("#sick_leave").html(data.sick_leave);
            $("#sick_leave_d").html(data.sick_leave_d);
            $("#fringe_benefit").html(data.fringe_benefit);
            $("#fringe_benefit_d").html(data.fringe_benefit_d);
            $("#other_deduction").html(data.other_deduction);
            $("#other_deduction_d").html(data.other_deduction_d);

            $("#total_deductions_d").html(data.total_deductions_d);
            $("#net_salary_d").html(data.net_salary_d);

            //tab4
            $("#gross_salary_4").html(data.total_gross_salary);
            $("#superannuation").html(data.superannuation);
            $("#superannuation_d").html(data.superannuation_d);
            $("#work_cover").html(data.work_cover);
            $("#work_cover_d").html(data.work_cover_d);
            $("#total_annuation_d").html(data.total_annuation_d);
            $("#total_payroll_d").html(data.total_payroll_d);

            $('#personmodal_form_Detail').modal('show');

            $('#personmodal_form_Detail').on('shown.bs.modal', function() {
                $('.nav a[href="#tab1"]').tab('show');
            })
            //}); // show bootstrap modal when complete loaded

            $('.modal-title').text('Personal Detail'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert(errorThrown);

        }
    });
    // alert(id);
}
$('#personmodal_form_Detail').on('show', function(e){
    alert(e);
});
</script>
<div class="modal fade" id="personmodal_form_Detail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="padding: 15px 15px 0 15px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="panel-white">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-4 thumbnail itemThumbnail">
                                    <img style="height:95%" class="group list-group-image img-responsive" id="imgthumbnail" src="" alt="">
                                </div>
                                <div class="col-md-8">
                                    <div class="row">
                                        <label class="control-label col-md-4">First Name :</label>
                                        <div class="control-label col-md-8" id="person_fname"></div>
                                    </div>
                                    <div class="row">
                                        <label class="control-label col-md-4">Last Name :</label>
                                        <div class="control-label col-md-8" id="person_lname"></div>
                                    </div>
                                    <div class="row">
                                        <label class="control-label col-md-4">Person ID :</label>
                                        <div class="control-label col-md-8" id="personid"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> <!--panel body -->
                </div> <!--panel white -->
                <ul class="nav nav-tabs nav-pills" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Hours/Pay Rate</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab2" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Remuneration</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab3" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Net Salary</a>
                    </li>
                    <li role="presentation">
                        <a href="#tab4" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Payroll Liability</a>
                    </li>
                </ul>
            </div> <!-- modal header -->
            <div class="modal-body form">
                <div class="row">
                    <div class="col-md-12">
                        <div class="panel panel-white">
                            <div class="panel-body">
                                <div class="tab-content" style="margin:0">
                                    <div class="tab-pane active fade in" id="tab1">
                                        <div class="row m-b-lg">
                                            <div class="col-xs-12">
                                                <label class="control-label col-xs-8">Hour Worked</label>
                                                <div class="control-label col-xs-4" id="person_hour_work"></div>
                                                <label class="control-label col-xs-8">Rater Per Hour</label>
                                                <div class="control-label col-xs-4" id="person_rates"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade in" id="tab2">
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Gross Weekly Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="weekly_salary"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Subsidies Received</label>
                                                <div class="control-label col-xs-4" id="subsidies"></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="subsidies_recieved"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Commission Received</label>
                                                <div class="control-label col-xs-4" id="commission"></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="commission_recieved"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Other Received</label>
                                                <div class="control-label col-xs-4" id="other"></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="other_recieved"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total Gross Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="total_gross_salary"></div>
                                            </div>
                                        </div>
                                    </div><!--end tab2 -->
                                    <div class="tab-pane fade in" id="tab3">
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total Gross Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><strong><p id="gross_salary"></p></strong></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"><hr></div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label pull-right">Average $</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-8 ">Total</label>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Pension</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="pension" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="pension_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Medicare Levi</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="medicare" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="medicare_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-5">
                                                <label class="control-label col-xs-12">Retirement Annuity</label>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="control-label col-xs-12"><p id="retirement" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="retirement_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-5">
                                                <label class="control-label col-xs-12">Income Tax (PAYG)</label>
                                            </div>
                                            <div class="col-xs-3">
                                                <div class="control-label col-xs-12"><p id="income_tax" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="income_tax_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Union Fee</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="union_fee" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="union_fee_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Sick Leave</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="sick_leave" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="sick_leave_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Fringe Benefit</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="fringe_benefit" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="fringe_benefit_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Other Deductions</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="other_deduction" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="other_deduction_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total Deductions</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><strong><p id="total_deductions_d"></p></strong></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"><hr></div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Net Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12">
                                                <strong><p id="net_salary_d"></p></strong></div>
                                            </div>
                                        </div>
                                    </div><!--end tab3 -->
                                    <div class="tab-pane fade in" id="tab4">
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total Gross Salary</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><strong><p id="gross_salary_4"></p></strong></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"><hr></div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label pull-right">Average $</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-8 ">Total</label>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Superannuation</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="superannuation" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="superannuation_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-4">
                                                <label class="control-label col-xs-12">Work Cover</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><p id="work_cover" class="text-right"></p></div>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12" id="work_cover_d"></div>
                                            </div>
                                        </div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-8">Total </label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12"><strong><p id="total_annuation_d"></p></strong></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"><hr></div>
                                        <div class="row m-b-lg">
                                            <div class="col-xs-8">
                                                <label class="control-label col-xs-12">Total Payroll Liabilities</label>
                                            </div>
                                            <div class="col-xs-4">
                                                <div class="control-label col-xs-12">
                                                <strong><p id="total_payroll_d"></p></strong></div>
                                            </div>
                                        </div>
                                    </div><!--end tab4 -->
                                </div><!-- tab content -->
                            </div><!--panel body -->
                        </div><!--panel-white-->
                    </div><!--col-md-12-->
                </div><!--row -->
            </div><!-- modal body -->
        </div><!-- modal content -->
    </div><!-- modal dialog -->
</div><!-- modal form -->
<div class="modal fade" id="personmodal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Add People Form</h3>
            </div>
            <div class="modal-body form">
                <div class="tab-header">
                    <ul class="nav nav-pills" style="top:40px;">
                      <li class="nav-item active">
                        <a href="#form1" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-info-sign"></i> <strong id="tabs">About</strong></p>
                        </a>
                      </li>
                      <li class="nav-item">
                        <a href="#form2" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-user"></i> <strong id="tabs">Liability</strong></p></a>
                      </li>
                      <li class="nav-item">
                        <a href="#form3" data-toggle="tab"><p style="color:rgba(0, 0, 0, 1.0); border: 0px solid blue; margin:0 5px 10px;"><i class="glyphicon glyphicon-user"></i> <strong id="tabs">Annuation</strong></p></a>
                      </li>
                    </ul>
                </div><!--end tab header-->
                <form action="#" id="personform" class="form-horizontal" enctype="multipart/form-data">
                <div class="tab-content clearfix">
                    <div id="form1" class="tab-pane fade active in">
                        <div style="border: 0px solid blue; margin: 10px;">
                        <input type="hidden" value="" name="personid"/>
                        <div class="form-body">
                            <div class="form-group">
                                <label class="control-label col-md-4">First Name</label>
                                <div class="col-md-8">
                                    <input name="person_fname" placeholder="First Name" class="form-control" type="text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Last Name</label>
                                <div class="col-md-8">
                                    <input name="person_lname" placeholder="Last Name" class="form-control" type="text" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Hour Work</label>
                                <div class="col-md-8">
                                    <input name="person_hour_work" placeholder="Hour Work" class="form-control" type="number" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Rates / Hour</label>
                                <div class="col-md-8">
                                    <input name="person_rates" placeholder="Rates/Hour" class="form-control" type="number" required>
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Subsidies Received</label>
                                <div class="col-md-8">
                                    <input name="person_subsidi" placeholder="Subsidies Received" class="form-control" type="number">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Commission Received</label>
                                <div class="col-md-8">
                                    <input name="person_commission" placeholder="Commission Received" class="form-control" type="number">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Other</label>
                                <div class="col-md-8">
                                    <input name="person_other" placeholder="Other" class="form-control" type="number">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Picture</label>
                                <div class="col-md-8">
                                    <input name="personflnFile" placeholder="Picture" class="form-control" type="file">
                                    <span class="help-block"></span>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div><!--end form1-->
                        <div id="form2" class="tab-pane fade in">
                            <div style="border: 0px solid blue; margin: 10px;">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Pension</label>
                                    <div class="col-md-8">
                                        <input name="person_pension" placeholder="Pension" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Medicare Levi</label>
                                    <div class="col-md-8">
                                        <input name="person_medicare" placeholder="Medicare Levi" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Retirement Annuity</label>
                                    <div class="col-md-8">
                                        <input name="person_retire" placeholder="Retirement Annuity" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Income Tax (PAYG)</label>
                                    <div class="col-md-8">
                                        <input name="person_tax" placeholder="Income Tax (PAYG)" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Union Fee</label>
                                    <div class="col-md-8">
                                        <input name="person_union" placeholder="Union Fee" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Sick leave</label>
                                    <div class="col-md-8">
                                        <input name="person_sick" placeholder="Sick leave" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Fringe Benefit Tax</label>
                                    <div class="col-md-8">
                                        <input name="person_fringe" placeholder="Fringe Benefit Tax" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Other Deductions</label>
                                    <div class="col-md-8">
                                        <input name="person_deductions" placeholder="Other Deductions" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div><!--end form2-->
                        <div id="form3" class="tab-pane fade in">
                            <div style="border: 0px solid blue; margin: 10px;">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-4">Super Annuation</label>
                                    <div class="col-md-8">
                                        <input name="person_superannuation" placeholder="Super Annuation" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Work Cover</label>
                                    <div class="col-md-8">
                                        <input name="person_workcover" placeholder="Work Cover" class="form-control" type="number">
                                        <span class="help-block"></span>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div><!--end form3-->
                    </div><!--end tab content-->
                    <div class="modal-footer">
                        <input type="submit" id="btnSavePerson" value="Save" class="btn btn-primary">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->