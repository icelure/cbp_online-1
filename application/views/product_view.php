<div class="well well-sm col-sm-12 col-md-12 col-lg-12">
    <strong>Display</strong>
    <div class="btn-group">
        <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a>
        <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
    </div>
    <button style="float: right;"  class="btn btn-success" onclick="add_localproduct()"><i class="glyphicon glyphicon-plus"></i><strong id="tabs">Products</strong></button>
</div>
<div id="products" class="row-height list-group col-sm-12 col-md-12 col-lg-12"> </div>

<!-- style for pagination -->
<style>
    /*pagination style*/
    .easyPaginateNav a {
        font-size: 18px;
        padding: 5px 10px;
        font-weight: bold;
        color: white;
        margin-left: 5px;
        background-color: #3c8dbc;}
    .easyPaginateNav{
        width: auto!important;
        text-align: left;
        clear: both;
    }
    .easyPaginateNav a.current {
        font-weight:bold;
        text-decoration:underline;}
    /*item design*/
    .itemThumbnail{
        height:165px;
        margin-bottom:0px;
    }
    .itemFooter{
        padding: 5px;
        border: solid 1px #e6e6e6;
        border-top: none;
        height: 40px;
    }
    .itemInfo{
        border: solid 1px #e6e6e6;
        border-top: none;
        padding-left: 10%;
        height: 278px;
    }

     /*Style for collection view*/
/*    .glyphicon { margin-right:5px; }*/
    .thumbnail
    {
        /*margin-bottom: 20px;*/
        padding: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }
    .item.list-group-item
    {
        float: none;
        width: 100%;
        background-color: #fff;
        margin-bottom: 10px;
    }
    .item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
    {
        /*background: #428bca;*/
        background: #e2effb;
    }
    .item.list-group-item .list-group-image
    {
        margin-right: 10px;
    }
    .item.list-group-item .thumbnail
    {
        margin-bottom: 0px;
    }
    .item.list-group-item .caption
    {
        padding: 9px 9px 0px 9px;
    }
    .item.list-group-item:nth-of-type(odd)
    {
        background: #eeeeee;
    }

    .item.list-group-item:before, .item.list-group-item:after
    {
        display: table;
        content: " ";
    }

    .item.list-group-item img
    {
        float: left;
    }
    .item.list-group-item:after
    {
        clear: both;
    }
    .list-group-item-text
    {
        margin: 0 0 11px;
    }

.input-group .input-group-addon {

        border: 1px solid #ccc;
        width: 48px !important;
    }

</style>

<script type="text/javascript">

            // Validations
            $(function () {
                $("#form").validate({
                    rules: {
                        product_description: {
                            required: true
                        },
                        unit_cost: {
                            required: true,
                            number: true
                        },
                        markup_on_cost: {
                            required: true,
                            number: true
                        },
                        Quantity: {
                            required: true,
                            number: true
                        }
                    }
                });
            });

            var save_method; //for save method string
            $(document).ready(function () {

                drawCollectionView();

                $('#list').click(function (event) {
                    event.preventDefault();

                    $('#products .item').addClass('list-group-item');
                });
                $('#grid').click(function (event) {
                    event.preventDefault();
                    $('#products .item').removeClass('list-group-item');
                    $('#products .item').addClass('grid-group-item');
                });
                // product validation event bind
                $("#btnSave").click(function (e) {
                    e.preventDefault();
                    if ($("#form").valid()) {
                        save();
                    }
                });
            });

            function drawCollectionView()
            {
                $.ajax({
                    url: "<?php echo site_url('Products/ajax_list') ?>/",
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        loadcollectionview(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(errorThrown);
                    }
                });
            }

            function loadcollectionview(products) {

                var html = "";
                $(products.data).each(function (key, val) {
                    html += '<div class="item  col-sm-12 col-md-6 col-lg-4">'
                                +'<div class="thumbnail itemThumbnail">'
                                    +'<img style="height:95%" class="group list-group-image img-responsive" src="' + this["localproductthumbnail"] + '" alt="" />'
                                +'</div>'
                                +'<div class="itemInfo caption">'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 txt-bold"><label style="font-weight:600;">UnitCost:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["unitcost"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">Qty:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+ this["qty"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">TotalCost:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+this["totalcost"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">Markup:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+ this["markup"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">R.R.P:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+ this["rrr"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">Total Weekly Revenue:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+ this["totalweeklyrevenue"] + '</div></div>'
                                    +'<h4 class="group inner list-group-item-heading"><strong>Description</strong>:-</h4>'
                                    +'<p  class="group inner list-group-item-text item-desc">' + this["localproductdescription"] + '</p>'
                                    +'</div>'
                                +'<div class="itemFooter" style="margin-bottom:10px;" >'
                                    +'<a  style="margin-left: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Edit" onclick="edit_product(' + this["localproductid"] + ')"><i class="glyphicon glyphicon-pencil"></i></a>'
                                    +'<a  class="btn btn-sm btn-danger pull-right" href="javascript:void(0)" title="Delete" onclick="delete_localproduct(' + this["localproductid"] + ')"><i class="glyphicon glyphicon-trash"></i></a>'
                                +'</div>'
                            +'</div>';
                });
                $('#products').html(html);
                // code for pagination
                $('#products').easyPaginate({
                    paginateElement: '.item',
                    elementsPerPage: 9,
                    firstButton: true,
                    lastButton: true
                    // effect: 'climb'
                });
                $('.item-desc').each(function(){
                    if($(this).text().length>130){
                    $(this).text($(this).text().substring(0,130)+"...");
                    }
                });
            }
</script>
