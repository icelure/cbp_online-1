<div class="container">
<!-- Content Header (Page header) -->
<section class="content-header">
<h1>
    Reset Password
        </h1>
            <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
              <li class="active">Reset Password</li>
            </ol>
</section>
<style type="text/css">

#error {
    display: inline-block;
    width: 30em;
    margin-right: .5em;
    padding-top: 1px;
    color: red;
}
</style>


<!-- Main content -->
<section class="content">
    <div class="table-responsive">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">


        <?php
            $errors = validation_errors();
            if($errors !== ""){ ?>
            <div class="callout callout-danger">
              <?php echo $errors; ?>
            </div>

            <?php } ?>

            <?php
            $flashdata= $this->session->flashdata('response');
            if(!empty($flashdata)){
              if($flashdata['status'] == 'success'){
                ?>
                <div class="callout callout-success">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
              if($flashdata['status'] == 'failed'){
                ?>
                <div class="callout callout-danger">
                  <?php echo $flashdata['message']; ?>
                </div>
                <?php
              }
            }
            ?>

            <div class="login-box">
              <div class="login-logo">
              <a href="javascript:void(0);"><b>Reset your password</a>
              </div><!-- /.login-logo -->
              <div class="login-box-body">
                <!-- <p class="login-box-msg">Sign in to start your session</p> -->
                <form action="<?php echo base_url() ?>login" method="post">
                  <div class="form-group has-feedback">
                    <input type="password" name="password" id="password" class="form-control" placeholder="Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback">
                    <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Confirm Password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>











                  <div class="row">
                    <div class="col-xs-4">
                      <input type="hidden" name="email" value="<?php echo $email; ?>">
                      <input type="hidden" name="token" value="<?php echo $token; ?>">
                      <input type="hidden" name="rpForm" value="postrpForm">
                      <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                    </div><!-- /.col -->
                  </div>
                </form>
              </div><!-- /.login-box-body -->
            </div><!-- /.login-box -->
          </section><!-- /.content -->
        </div><!-- /.container -->