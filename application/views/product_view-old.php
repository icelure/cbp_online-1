<div class="well well-sm col-sm-12 col-md-12 col-lg-12">
    <strong>Display</strong>
    <div class="btn-group">
        <a href="#" id="list" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th-list"></span>List</a> 
        <a href="#" id="grid" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-th"></span>Grid</a>
    </div>
    <button style="float: right;"  class="btn btn-success" onclick="add_localproduct()"><i class="glyphicon glyphicon-plus"></i>Product</button>
</div>
<div id="products" class="row-height list-group col-sm-12 col-md-12 col-lg-12"> </div>

<script src="<?php echo base_url('assets/jquery/jquery-2.1.4.min.js') ?>"></script>
<script src="<?php echo base_url('assets/bootstrap/js/bootstrap.min.js') ?>"></script>
<script type="text/javascript" src="<?php echo js_url(); ?>jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo js_url(); ?>additional-methods.min.js"></script>
<script src="<?php echo base_url('assets/plugins/easypaginate/easyPaginate.js') ?>"></script>
<!-- style for pagination -->
<style> 
    /*pagination style*/
    .easyPaginateNav a {    font-size: 18px;
                            padding: 5px 10px;
                            font-weight: bold;
                            color: white;
                            margin-left: 5px;
                            background-color: #3c8dbc;}
    .easyPaginateNav{
        width: auto!important;
        text-align: left;
        clear: both;
    }
    .easyPaginateNav a.current {
        font-weight:bold;
        text-decoration:underline;}
    /*item design*/
    .itemThumbnail{
        height:165px;
        margin-bottom:0px;
    }
    .itemFooter{
        padding: 5px;
        border: solid 1px #e6e6e6;
        border-top: none;
        height: 40px;
    }
    .itemInfo{
        border: solid 1px #e6e6e6;
        border-top: none;
        padding-left: 10%;
        height: 278px;
    }
     
     /*Style for collection view*/      
/*    .glyphicon { margin-right:5px; }*/
    .thumbnail
    {
        /*margin-bottom: 20px;*/
        padding: 0px;
        -webkit-border-radius: 0px;
        -moz-border-radius: 0px;
        border-radius: 0px;
    }
    .item.list-group-item
    {
        float: none;
        width: 100%;
        background-color: #fff;
        margin-bottom: 10px;
    }
    .item.list-group-item:nth-of-type(odd):hover,.item.list-group-item:hover
    {
        background: #428bca;
    }
    .item.list-group-item .list-group-image
    {
        margin-right: 10px;
    }
    .item.list-group-item .thumbnail
    {
        margin-bottom: 0px;
    }
    .item.list-group-item .caption
    {
        padding: 9px 9px 0px 9px;
    }
    .item.list-group-item:nth-of-type(odd)
    {
        background: #eeeeee;
    }

    .item.list-group-item:before, .item.list-group-item:after
    {
        display: table;
        content: " ";
    }

    .item.list-group-item img
    {
        float: left;
    }
    .item.list-group-item:after
    {
        clear: both;
    }
    .list-group-item-text
    {
        margin: 0 0 11px;
    }

     
</style>

<script type="text/javascript">

            // Validations
            $(function () {
                $("#form").validate({
                    rules: {
                        product_description: {
                            required: true
                        },
                        unit_cost: {
                            required: true,
                            number: true
                        },
                        markup_on_cost: {
                            required: true,
                            number: true
                        },
                        Quantity: {
                            required: true,
                            number: true
                        }
                    }
                });
            });
            
            var save_method; //for save method string
            $(document).ready(function () {

                drawCollectionView();

                $('#list').click(function (event) {
                    event.preventDefault();
                    
                    $('#products .item').addClass('list-group-item');
                });
                $('#grid').click(function (event) {
                    event.preventDefault();
                    $('#products .item').removeClass('list-group-item');
                    $('#products .item').addClass('grid-group-item');
                });
                // product validation event bind
                $("#btnSave").click(function (e) {
                    e.preventDefault();
                    if ($("#form").valid()) {
                        save();
                    }
                });
            });

            function drawCollectionView()
            {
                $.ajax({
                    url: "<?php echo site_url('Products/ajax_list') ?>/",
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        loadcollectionview(data);
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(errorThrown);
                    }
                });
            }
            
            function loadcollectionview(products) {
                
                var html = "";
                $(products.data).each(function (key, val) {
                    html += '<div class="item  col-sm-12 col-md-6 col-lg-4">'
                                +'<div class="thumbnail itemThumbnail">'
                                    +'<img style="height:95%" class="group list-group-image img-responsive" src="' + this["localproductthumbnail"] + '" alt="" />'                            
                                +'</div>'
                                +'<div class="itemInfo caption">'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 txt-bold"><label style="font-weight:600;">UnitCost:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">' + this["unitcost"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">Qty:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+ this["qty"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">TotalCost:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+this["totalcost"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">Markup:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+ this["markup"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">R.R.P:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+ this["rrr"] + '</div></div>'
                                    +'<div class="row"><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6"><label style="font-weight:600;">Total Weekly Revenue:</label></div><div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="font-weight: normal;">'+ this["totalweeklyrevenue"] + '</div></div>'               
                                    +'<h4 class="group inner list-group-item-heading"><strong>Description</strong>:-</h4>'
                                    +'<p  class="group inner list-group-item-text item-desc">' + this["localproductdescription"] + '</p>'                    
                                    +'</div>'
                                +'<div class="itemFooter" style="margin-bottom:10px;" >'
                                    +'<a  style="margin-left: 1%;" class="btn btn-sm btn-primary pull-right" href="javascript:void(0)" title="Edit" onclick="edit_product(' + this["localproductid"] + ')"><i class="glyphicon glyphicon-pencil"></i></a>'
                                    +'<a  class="btn btn-sm btn-danger pull-right" href="javascript:void(0)" title="Delete" onclick="delete_localproduct(' + this["localproductid"] + ')"><i class="glyphicon glyphicon-trash"></i></a>'
                                +'</div>'
                            +'</div>';
                });
                $('#products').html(html);
                // code for pagination
                $('#products').easyPaginate({
                    paginateElement: '.item',
                    elementsPerPage: 9,
                    firstButton: true,
                    lastButton: true
                    // effect: 'climb'
                });
                $('.item-desc').each(function(){
                    if($(this).text().length>130){
                    $(this).text($(this).text().substring(0,130)+"...");
                    }
                });
            }
            function add_localproduct()
            {
                $("label.error").remove();
                save_method = 'add';
                $('#form')[0].reset(); // reset form on modals
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string
                $('#modal_form').modal('show'); // show bootstrap modal
                $('.modal-title').text('Add Product'); // Set Title to Bootstrap modal title
            }

            function edit_product(id)
            {
                $("label.error").remove();
                save_method = 'update';
                $('#form')[0].reset(); // reset form on modals
                $('.form-group').removeClass('has-error'); // clear error class
                $('.help-block').empty(); // clear error string

                //Ajax Load data from ajax
                $.ajax({
                    url: "<?php echo site_url('Products/ajax_edit/') ?>/" + id,
                    type: "GET",
                    dataType: "JSON",
                    success: function (data)
                    {
                        $('[name="id"]').val(data.id);
                        $('[name="product_description"]').val(data.Description);
                        $('[name="Quantity"]').val(data.Qty);
                        $('[name="unit_cost"]').val(data.UnitCost);
                        $('[name="markup_on_cost"]').val(data.MarkUpOnCost);
                        $('[name="flnFile"]').val(data.Thumbnail);
                        $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
                        $('.modal-title').text('Edit Product'); // Set title to Bootstrap modal title

                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert(errorThrown);
                    }
                });
            }
            function save()
            {
                $('#btnSave').text('saving...'); //change button text
                $('#btnSave').attr('disabled', true); //set button disable 
                var url;

                if (save_method == 'add') {
                    url = "<?php echo site_url('Products/ajax_add') ?>";
                } else {
                    url = "<?php echo site_url('Products/ajax_update') ?>";
                }
                // ajax adding data to database
                $.ajax({
                    url: url,
                    type: "POST",
                    data: new FormData($('#form')[0]),
                    contentType: false,
                    async: false,
                    cache: false,
                    processData: false,
                    success: function (data)
                    {
                        var obj = jQuery.parseJSON(data);

                        if (obj['status']) //if success close modal and reload ajax table
                        {
                            $('#modal_form').modal('hide');
                            drawCollectionView();
                        }
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    },
                    error: function (jqXHR, textStatus, errorThrown)
                    {
                        alert('Error adding / update data');
                        $('#btnSave').text('save'); //change button text
                        $('#btnSave').attr('disabled', false); //set button enable 
                    }
                });
            }

            function delete_localproduct(id)
            {
                if (confirm('Are you sure delete this data?'))
                {
                    // ajax delete data to database
                    $.ajax({
                        url: "<?php echo site_url('Products/ajax_delete') ?>/" + id,
                        type: "POST",
                        dataType: "JSON",
                        success: function (data)
                        {
                            //if success reload ajax table
                            $('#modal_form').modal('hide');
                            drawCollectionView();
                        },
                        error: function (jqXHR, textStatus, errorThrown)
                        {
                            alert('Error deleting data');
                        }
                    });

                }
            }

</script>

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h3 class="modal-title">Product Form</h3>
            </div>
            <div class="modal-body form">
                <form action="#"  id="form" class="form-horizontal" enctype="multipart/form-data">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-3">Description</label>
                            <div class="col-md-9">
                                <input name="product_description" placeholder="Product Description" class="form-control" type="text"  required>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Quantity</label>
                            <div class="col-md-9">
                                <input name="Quantity" placeholder="Quantity" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Unit Cost</label>
                            <div class="col-md-9">                                
                                <input name="unit_cost" placeholder="Unit Cost" class="form-control" type="text">
                                <span class="help-block"></span>
                            </div>
                        </div>   


                        <div class="form-group">
                            <label class="control-label col-md-3">Markup on Cost</label>
                            <div class="col-md-9">                                
                                <input name="markup_on_cost" placeholder="Markup on Cost" class="form-control" >
                                <span class="help-block"></span>
                            </div>
                        </div>   
                        <div class="form-group">
                            <label class="control-label col-md-3">Picture</label>
                            <div class="col-md-9">                                
                                <input name="flnFile" placeholder="Markup on Cost" class="form-control" type="file">
                                <span class="help-block"></span>
                            </div>
                        </div>   
                    </div>
                    <div class="modal-footer">
                        <input type="submit" id="btnSave" value="Save" class="btn btn-primary"></button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->