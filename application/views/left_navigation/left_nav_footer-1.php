</div><!-- /.content-wrapper -->
      <footer class="main-footer" style="text-align: center;">
        <strong>Copyright &copy; 2014-2015 <a href="<?php echo base_url(); ?>">CBP Online</a>.</strong> All rights reserved.
      </footer>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


    <!-- Morris.js charts -->
    <?php if($page == 'Dashboard' || $page == 'dashboard') { ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?=asset_url()?>plugins/morris/morris.min.js"></script>
    <?php } ?>
    <!-- Sparkline -->
    <script src="<?=asset_url()?>plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?=asset_url()?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?=asset_url()?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?=asset_url()?>plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?=asset_url()?>plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?=asset_url()?>plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?=asset_url()?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?=asset_url()?>plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?=asset_url()?>plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=asset_url()?>dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?=asset_url()?>dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->

    <script src="<?=asset_url()?>dist/js/demo.js"></script>

  </body>
</html>
