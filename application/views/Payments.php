<link rel="stylesheet" href="<?php echo asset_url(); ?>plugins/datatables/dataTables.bootstrap.css">
<section class="content-header">
  <h1>
    Payments
    <!-- <small>advanced tables</small> -->
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><a href="#">Payments</a></li>
    <!-- <li class="active">Data tables</li> -->
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <!-- <div class="box-header">
          <h3 class="box-title">Data Table With Full Features</h3>
        </div>/.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>ID</th>
                <th>User Name</th>
                <th>Payer Email</th>
                <th>Transaction ID</th>
                <th>Amount</th>
                <th>Currency</th>
                <th>Status</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>
            <?php if(!empty($payments)) { 

              foreach ($payments as $pay) { ?>
               <tr>
                <td><?php print $pay['payment_id']; ?></td>
                <td><?php print $pay['name']; ?></td>
                <td><?php print $pay['payer_email']; ?></td>
                <td><?php print $pay['txn_id']; ?></td>
                <td><?php print $pay['payment_gross'];?></td>
                <td><?php print $pay['currency_code'];?></td>
                <td><?php print $pay['payment_status'];?></td>
                <td><?php print $pay['createdDate'];?></td>
              </tr>
             <?php } 
                }
             ?>
              </tbody>
              <tfoot>
                <th>ID</th>
                <th>User Name</th>
                <th>Payer Email</th>
                <th>Transaction ID</th>
                <th>Amount</th>
                <th>Currency</th>
                <th>Status</th>
                <th>Date</th>
              </tfoot>
            </table>
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </section><!-- /.content -->
  <script src="<?php echo  asset_url(); ?>plugins/datatables/jquery.dataTables.min.js"></script>
  <script src="<?php echo  asset_url(); ?>plugins/datatables/dataTables.bootstrap.min.js"></script>
  <script>
    $(function () {
      $('#example1').DataTable();
    });
  </script>