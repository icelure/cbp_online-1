<?php
Class User_model extends CI_Model
{
	public function insert_user($data)
	{
		$this->db->insert('user', $data);
		return $this->db->insert_id();
	}

	public function get_all_users(){
		$this->db->where('type','u');
		$query = $this->db->get('user');
		$users =array();

		if($query->num_rows() > 1){
			foreach ($query->result_array() as $user) {
				$users[] = $user;
			}
		}
		return $users;
	}

	public function checkEmailExist($email){
		$this->db->where('email',$email);
	    $query = $this->db->get('user');
	    if ($query->num_rows() > 0){
	        return false;
	    }
	    else{
	        return true;
	    }
	}

	public function loginProcedure($email,$password){
		$this->db->where('email',$email);
		$this->db->where('password',md5($password));
	    $query = $this->db->get('user');
	    if ($query->num_rows() > 0){
	        return $query->row();
	    }
	    else{
	        return false;
	    }	
	}

	public function verify_account($code){
		$this->db->select('id');
		$this->db->from('user');
		$this->db->where('token',$code);
		$this->db->where('status','d');
	    $query = $this->db->get();
	    if ($query->num_rows() > 0){
	    	$uid = $query->row()->id;
	    	$data = array(
               'status' => 'a',
               'token' => ''
            );
			
			$this->db->where('id', $uid);
			$this->db->update('user', $data);
			return true; 
	    }
	    else{
	        return false;
	    }
	}

	public function update_token($uid,$data){
		$this->db->where('id',$uid);
		$this->db->where('status','a');
		$response = $this->db->update('user',$data);
		return $response;
	}

	public function get_user_by_email($email){
		$this->db->where('email',$email);
		$this->db->where('status','a');
	    $query = $this->db->get('user');
	    if ($query->num_rows() > 0){
	        return $query->row()->id;
	    }
	    else{
	        return false;
	    }
	}

	public function update_password($data,$id,$token){
		$this->db->where('id',$id);
		$this->db->where('token',$token);
		$this->db->where('status','a');
	    if ($this->db->update('user',$data))
	    {
	        return true;
	    }
	    else{
	        return false;
	    }
	}
}
?>