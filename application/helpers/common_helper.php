<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sekati CodeIgniter Asset Helper
 *
 * @package		Sekati
 * @author		Jason M Horwitz
 * @copyright	Copyright (c) 2013, Sekati LLC.
 * @license		http://www.opensource.org/licenses/mit-license.php
 * @link		http://sekati.com
 * @version		v1.2.7
 * @filesource
 *
 * @usage 		$autoload['config'] = array('asset');
 * 				$autoload['helper'] = array('asset');
 * @example		<img src="<?=asset_url();?>imgs/photo.jpg" />
 * @example		<?=img('photo.jpg')?>
 *
 * @install		Copy config/asset.php to your CI application/config directory
 *				& helpers/asset_helper.php to your application/helpers/ directory.
 * 				Then add both files as autoloads in application/autoload.php:
 *
 *				$autoload['config'] = array('asset');
 * 				$autoload['helper'] = array('asset');
 *
 *				Autoload CodeIgniter's url_helper in application/config/autoload.php:
 *				$autoload['helper'] = array('url');
 *
 * @notes		Organized assets in the top level of your CodeIgniter 2.x app:
 *					- assets/
 *						-- css/
 *						-- download/
 *						-- img/
 *						-- js/
 *						-- less/
 *						-- swf/
 *						-- upload/
 *						-- xml/
 *					- application/
 * 						-- config/asset.php
 * 						-- helpers/asset_helper.php
 */
// ------------------------------------------------------------------------
// URL HELPERS
/**
 * Get asset URL
 *
 * @access  public
 * @return  string
 */
function sendMail($content,$email,$subject)
    {
        $CI =& get_instance();
        
        $config = array();
        // The mail sending protocol.
        $config['protocol'] = 'smtp';
        // SMTP Server Address for Gmail.
        $config['smtp_host'] = 'ssl://smtp.googlemail.com';
        // SMTP Port - the port that you is required
        $config['smtp_port'] = 465;
        // SMTP Username like. (abc@gmail.com)
        $config['smtp_user'] = 'pratik.padhariya1604@gmail.com';
        // SMTP Password like (abc***##)
        $config['smtp_pass'] = 'mailforfiverr';

        $config['mailtype'] = 'html';

        $config['charset'] = 'iso-8859-1';

        $config['wordwrap'] = TRUE;

        
        $CI->load->library('email', $config);
        //$CI->email->set_newline("\r\n");
        $CI->email->from('info@CBPOnline.com','CBP Online'); // change it to yours
        $CI->email->to($email);// change it to yours
        $CI->email->subject($subject);
        $CI->email->message($content);
        if($CI->email->send())
        {
            return true;
        }
        else
        {
         show_error($CI->email->print_debugger());
         exit;
         return false;
        }
    }
/* End of file common_helper.php */