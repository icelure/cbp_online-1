<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Onetimecost extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Onetimecost_model','onetimecost');
	}

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('onetimecost_view');
	}

	public function ajax_list()
	{
		$list = $this->onetimecost->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $onetimecost) {
			$no++;
			$row = array();
			$row[] = $onetimecost->one_time_cost;
			$row[] = $onetimecost->description;
			$row[] = number_format($onetimecost->amount_paid);			
			$row[] = $onetimecost->vat_gst .'%';
			$row[] = number_format($onetimecost->total_vat_gst);
			$row[] = number_format($onetimecost->total_cost);

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_onetimecost('."'".$onetimecost->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_onetimecost('."'".$onetimecost->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->onetimecost->count_all(),
						"recordsFiltered" => $this->onetimecost->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->onetimecost->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$var = $this->session->userdata;		
		$user_id = $var['user']->id;	
		$one_time_cost_type = $this->input->post('one_time_cost_type');
		$one_time_cost_description = $this->input->post('one_time_cost_description');
		$amount_paid = $this->input->post('amount_paid');		
		$vat_gst = 10;
		$total_vat_gst = (10*$amount_paid)/100;
		$total_cost = $amount_paid + $total_vat_gst;
		$data = array(
				'user_id' => $user_id,
				'description' => $this->input->post('one_time_cost_description'),				
				'amount_paid' => $this->input->post('amount_paid'),
				'one_time_cost' => $one_time_cost_type,
				'vat_gst' => $vat_gst,
				'total_vat_gst' => $total_vat_gst,
				'total_cost' => $total_cost,
			);
		$insert = $this->onetimecost->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$var = $this->session->userdata;		
		$user_id = $var['user']->id;	
		$one_time_cost_type = $this->input->post('one_time_cost_type');
		$one_time_cost_description = $this->input->post('one_time_cost_description');
		$amount_paid = $this->input->post('amount_paid');		
		$vat_gst = 10;
		$total_vat_gst = (10*$amount_paid)/100;
		$total_cost = $amount_paid + $total_vat_gst;
		$data = array(
				'user_id' => $user_id,
				'description' => $this->input->post('one_time_cost_description'),				
				'amount_paid' => $this->input->post('amount_paid'),
				'one_time_cost' => $one_time_cost_type,
				'vat_gst' => $vat_gst,
				'total_vat_gst' => $total_vat_gst,
				'total_cost' => $total_cost,
			);
		$this->onetimecost->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->onetimecost->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

}
