<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_plans extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        
        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
   		}else{
            if($this->session->userdata('user')->type == "u"){
              redirect(base_url().'Dashboard');
            }
        }
        
    }

	public function index($type='')
	{
		$this->load->model('Plan_model');
		$data['page'] = $this->uri->segment(1);
		
		if(isset($_POST['type']) && $_POST['id'] > 0){
			$this->updatePlan();
		}

		if($type=='yearly'){
			$data['plans'] = $this->Plan_model->get_all_plans('yearly');
			$this->load->template_left_nav('monthly_plan',$data);
		}elseif ($type=='monthly') {
			$data['plans'] = $this->Plan_model->get_all_plans('monthly');
			$this->load->template_left_nav('monthly_plan',$data);
		}else{
			$data['plans'] = $this->Plan_model->get_all_plans();
			$this->load->template_left_nav('Manage_Plans',$data);

		}	
	}

	public function updatePlan(){
		$this->form_validation->set_rules('id', 'ID', 'trim|required');
		$this->form_validation->set_rules('type', 'Type', 'trim|required');
		$this->form_validation->set_rules('title', 'Title ', 'trim|required');
		$this->form_validation->set_rules('price', 'Price', 'trim|required');
		
		if ($this->form_validation->run() == FALSE)
        {
            
        }
        else
        {
        	$data=array(
        		"title" => $this->input->post('title'),
        		"subtitle" => $this->input->post('subtitle'),
        		"price" => $this->input->post('price'),
        		"feature1" => $this->input->post('feature1'),
        		"feature2" => $this->input->post('feature2'),
        		"feature3" => $this->input->post('feature3'),
        		"feature4" => $this->input->post('feature4')
        	);
        	$id = $this->input->post('id');
        	$type = $this->input->post('type');

        	if($this->Plan_model->update_plan($id,$type,$data)){
        		$response=array(
  					'status'=>'success',
  					'message' => 'Plan edited successfully'
  				);
        	}else{
        		$response=array(
      					'status'=>'failed',
      					'message' => 'There is some problem in editing this plan,please try again later.'
      			);	
        	}
        	$this->session->set_flashdata('response', $response);
        	redirect(base_url().'Manage_plans');
        }
	}
}
