<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class People extends CI_Controller {

    public function __construct() {
        parent::__construct();

        if (!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url() . 'login');
        }
        $this->load->model('people_model');

        $this->load->model('profile_model');
    }

    public function index() {
        // echo "test";

        $data['page'] = $this->uri->segment(1);
        $data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
        //     var_dump($data['page']);
        $this->load->template_left_nav('People', $data);
    }

    public function ajax_list(){

        $list = $this->people_model->get_datatables();

        // Currency
        $var = $this->session->userdata;
        $user_id = $var['user']->id;
        $query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;

        $cur = '';

        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }

        $data = array();
        $no = (isset($_POST['start']))? $_POST['start']:'';
        foreach ($list as $people) {
            $no++;
            //$Total = $product->Qty * $product->UnitCost;
            $row = array();
            $row["id"] = $people->id;
            $row["thumbnail"] = $people->thumbnail;
            $row["f_name"] = $people->f_name;
            $row["l_name"] = $people->l_name;
            $row["hour_worked"] = number_format($people->hour_worked).' / hour';
            $row["rates"] = $cur .''. number_format($people->rates, 2, '.', '');
            $row["subsidies"] = number_format($people->subsidies, 2, '.', '').' %';
            $row["commission"] = number_format($people->commission, 2, '.', ''). ' %';
            $row["other"] = number_format($people->other, 2, '.', '').' %';

            $row["weekly_salary"] = $cur .''. number_format($people->hour_worked * $people->rates, 2, '.', '');
            $row["subsidies_recieved"] = $cur .''. number_format(($people->subsidies / 100) * ($people->hour_worked * $people->rates), 2, '.', '');
            $row["commission_recieved"] = $cur .''. number_format(($people->commission / 100) * ($people->hour_worked * $people->rates), 2, '.', '');
            $row["other_recieved"] = $cur .''. number_format(($people->other / 100) * ($people->hour_worked * $people->rates), 2, '.', '');

            $data[] = $row;
        }
        // echo $data[];
        $output = array(
            "draw" => (isset($_POST['draw']))? $_POST['draw']:'',
            "recordsTotal" => $this->people_model->count_all(),
            "recordsFiltered" => $this->people_model->count_filtered(),
            "data" => $data,
        );
        //output to json format


        echo json_encode($output);

    }

    public function ajax_addperson() {

        $var = $this->session->userdata;
        $user_id = $var['user']->id;
        $ThumbNail;
        $maxid = 0;

        $row = $this->db->query('SELECT MAX(id) AS `maxid` FROM `people`')->row();

        if ($row) {
            $maxid = $row->maxid + 1;
        }

        if (!($_FILES["personflnFile"]["name"] == NULL)) {
            $target_dir = "person_thumbnails/";
            if (!file_exists($target_dir)) {
                mkdir($target_dir);
            }

            $target_file = $target_dir . $user_id . "_" . $maxid . "_" . basename($_FILES["personflnFile"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["personflnFile"]["tmp_name"]);
            if ($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                //echo "File is not an image.";
                $uploadOk = 0;
            }
            if ($_FILES["personflnFile"]["size"] > 500000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            } else {
                if (move_uploaded_file($_FILES["personflnFile"]["tmp_name"], $target_file)) {
                    //echo "The file " . basename($_FILES["flnFile"]["name"]) . " has been uploaded.";
                } else {
                    //echo "Sorry, there was an error uploading your file.";
                }
                $ThumbNail = $target_file;
            }
        } else {
            $ThumbNail = 'assets/images/no_image.png';
        }

        $data = array(
            'thumbnail' => $ThumbNail,
            'f_name' => $this->input->post('person_fname'),
            'l_name' => $this->input->post('person_lname'),
            'hour_worked' => $this->input->post('person_hour_work'),
            'rates' => $this->input->post('person_rates'),
            'subsidies' => $this->input->post('person_subsidi'),
            'commission' => $this->input->post('person_commission'),
            'other' => $this->input->post('person_other'),
            'pension' => $this->input->post('person_pension'),
            'medicare_levi' => $this->input->post('person_medicare'),
            'retirement_annuity' => $this->input->post('person_retire'),
            'income_tax' => $this->input->post('person_tax'),
            'union_fee' => $this->input->post('person_union'),
            'sick_leave' => $this->input->post('person_sick'),
            'fringe_benefit' => $this->input->post('person_fringe'),
            'other_deduction' => $this->input->post('person_deductions'),
            'superannuation' => $this->input->post('person_superannuation'),
            'work_cover' => $this->input->post('person_workcover'),

        );


        $insert = $this->people_model->save($data);

        echo json_encode(array('status' => TRUE));
    }

    public function ajax_delete($id){

        $ThumbnailPath = $this->db->query('SELECT thumbnail  FROM `people` where id ='.$id)->row();

        if ($ThumbnailPath->thumbnail != "assets/images/no_image.png"){
            unlink($ThumbnailPath->thumbnail);
        }

        $this->people_model->delete_by_id($id);


        echo json_encode(array("status" => TRUE));

    }

    public function ajax_edit($id){

        $data = $this->people_model->get_by_id($id);

        echo json_encode($data);
    }

    public function ajax_updateperson(){

        $id = $this->input->post('personid');
        $ThumbnailPath = $this->db->query('SELECT thumbnail  FROM `people` where id = ' . $id)->row();
        $data;

        if (!($_FILES["personflnFile"]["name"] == NULL)) {

            $var = $this->session->userdata;
            $user_id = $var['user']->id;

            $target_dir = "person_thumbnails/";
            if (!file_exists($target_dir)) {
                mkdir($target_dir);
            }
            $target_file = $target_dir . $user_id . "_" . $id . "_" . basename($_FILES["personflnFile"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["personflnFile"]["tmp_name"]);
            if ($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                //echo "File is not an image.";
                $uploadOk = 0;
            }
            if ($_FILES["personflnFile"]["size"] > 500000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            }else{

                if ($ThumbnailPath->thumbnail != "assets/images/no_image.png"){
                    unlink($ThumbnailPath->thumbnail);
                }
                move_uploaded_file($_FILES["personflnFile"]["tmp_name"], $target_file);
            }
            $data = array(
                'thumbnail' =>$target_file,
                'f_name' => $this->input->post('person_fname'),
                'l_name' => $this->input->post('person_lname'),
                'hour_worked' => $this->input->post('person_hour_work'),
                'rates' => $this->input->post('person_rates'),
                'subsidies' => $this->input->post('person_subsidi'),
                'commission' => $this->input->post('person_commission'),
                'other' => $this->input->post('person_other'),
                'pension' => $this->input->post('person_pension'),
                'medicare_levi' => $this->input->post('person_medicare'),
                'retirement_annuity' => $this->input->post('person_retire'),
                'income_tax' => $this->input->post('person_tax'),
                'union_fee' => $this->input->post('person_union'),
                'sick_leave' => $this->input->post('person_sick'),
                'fringe_benefit' => $this->input->post('person_fringe'),
                'other_deduction' => $this->input->post('person_deductions'),
                'superannuation' => $this->input->post('person_superannuation'),
                'work_cover' => $this->input->post('person_workcover'),
            );
        } else {

            $data = array(
                'f_name' => $this->input->post('person_fname'),
                'l_name' => $this->input->post('person_lname'),
                'hour_worked' => $this->input->post('person_hour_work'),
                'rates' => $this->input->post('person_rates'),
                'subsidies' => $this->input->post('person_subsidi'),
                'commission' => $this->input->post('person_commission'),
                'other' => $this->input->post('person_other'),
                'pension' => $this->input->post('person_pension'),
                'medicare_levi' => $this->input->post('person_medicare'),
                'retirement_annuity' => $this->input->post('person_retire'),
                'income_tax' => $this->input->post('person_tax'),
                'union_fee' => $this->input->post('person_union'),
                'sick_leave' => $this->input->post('person_sick'),
                'fringe_benefit' => $this->input->post('person_fringe'),
                'other_deduction' => $this->input->post('person_deductions'),
                'superannuation' => $this->input->post('person_superannuation'),
                'work_cover' => $this->input->post('person_workcover'),
            );
        }

        $this->people_model->update(array('id' => $this->input->post('personid')), $data);

        echo json_encode(array("status" => TRUE));

    }

    public function ajax_get_personal_detail($id){

        $user_id = $this->session->userdata('user')->id;
        //$imp_product_cost = $this->importedproduct->get_import_cost($user_id);
        // Currency
        $query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;

        $cur = '';

        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }
        $people_basic = $this->people_model->get_by_id($id);

        $data['user_id'] = $user_id;
        $data['id'] = $id;
        $data['f_name'] = $people_basic->f_name;
        $data['l_name'] = $people_basic->l_name;
        $data['thumbnail'] = $people_basic->thumbnail;
        $data['hour_worked'] = number_format($people_basic->hour_worked).' / hour';
        $data['rates'] = $cur.''. number_format($people_basic->rates, 2, '.', '');
        $data['subsidies'] = number_format($people_basic->subsidies, 2, '.', '').' %';
        $data['commission'] = number_format($people_basic->commission, 2, '.', '').' %';
        $data['other'] = number_format($people_basic->other, 2, '.', '').' %';

        $data['pension'] = number_format($people_basic->pension, 2, '.', '').' %';

        $data['pension_d'] = $cur.''. number_format(($people_basic->pension / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');



        $data['medicare'] = number_format($people_basic->medicare_levi, 2, '.', '').' %';

        $data['medicare_d'] = $cur.''. number_format(($people_basic->medicare_levi / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['retirement_annuity'] = number_format($people_basic->retirement_annuity, 2, '.', '').' %';

        $data['retirement_annuity_d'] = $cur.''. number_format(($people_basic->retirement_annuity / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['income_tax'] = number_format($people_basic->income_tax, 2, '.', '').' %';

        $data['income_tax_d'] = $cur.''. number_format(($people_basic->income_tax / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['union_fee'] = number_format($people_basic->union_fee, 2, '.', '').' %';

        $data['union_fee_d'] = $cur.''. number_format(($people_basic->union_fee / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['sick_leave'] = number_format($people_basic->sick_leave, 2, '.', '').' %';

        $data['sick_leave_d'] = $cur.''. number_format(($people_basic->sick_leave / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['fringe_benefit'] = number_format($people_basic->fringe_benefit, 2, '.', '').' %';

        $data['fringe_benefit_d'] = $cur.''. number_format(($people_basic->fringe_benefit / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['other_deduction'] = number_format($people_basic->other_deduction, 2, '.', '').' %';

        $data['other_deduction_d'] = $cur.''. number_format(($people_basic->other_deduction / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['total_deductions_d'] = $cur.''. number_format(($people_basic->other_deduction / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->fringe_benefit / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->sick_leave / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->union_fee / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->income_tax / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->retirement_annuity / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->medicare_levi / 100) * ($people_basic->hour_worked * $people_basic->rates), 2, '.', '');

        $data['net_salary_d'] =$cur.''. number_format(($people_basic->hour_worked * $people_basic->rates) - (($people_basic->other_deduction / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->fringe_benefit / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->sick_leave / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->union_fee / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->income_tax / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->retirement_annuity / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->medicare_levi / 100) * ($people_basic->hour_worked * $people_basic->rates)), 2, '.', '');

        $data['superannuation'] = number_format($people_basic->superannuation, 2, '.', '').' %';
        $data['superannuation_d'] = $cur.''. number_format(($people_basic->superannuation / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['work_cover'] = number_format($people_basic->work_cover, 2, '.', '').' %';
        $data['work_cover_d'] = $cur.''. number_format(($people_basic->work_cover / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['total_annuation_d'] = $cur.''. number_format(($people_basic->work_cover / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->superannuation / 100) * ($people_basic->hour_worked * $people_basic->rates), 3, '.', '');

        $data['total_payroll_d'] = $cur.''. number_format(($people_basic->hour_worked * $people_basic->rates) - (($people_basic->work_cover / 100) * ($people_basic->hour_worked * $people_basic->rates)+($people_basic->superannuation / 100) * ($people_basic->hour_worked * $people_basic->rates)), 3, '.', '');


        $data['weekly_salary'] = $cur.''.number_format($people_basic->hour_worked * $people_basic->rates, 3, '.', '');
        $data['subsidies_recieved'] = $cur.''.number_format(($people_basic->subsidies / 100) * ($people_basic->hour_worked * $people_basic->rates), 2, '.', '');
        $data['commission_recieved'] = $cur.''.number_format(($people_basic->commission / 100) * ($people_basic->hour_worked * $people_basic->rates), 2, '.', '');
        $data['other_recieved'] = $cur.''.number_format(( $people_basic->other / 100) * ($people_basic->hour_worked * $people_basic->rates), 2, '.', '');

        $data['gross_salary'] = $cur.''.number_format((($people_basic->hour_worked * $people_basic->rates) + (($people_basic->subsidies / 100) * ($people_basic->hour_worked * $people_basic->rates)) + ($people_basic->commission / 100) * ($people_basic->hour_worked * $people_basic->rates) + (( $people_basic->other / 100) * ($people_basic->hour_worked * $people_basic->rates))), 2, '.', '');

        echo json_encode($data);

    }

    public function peoplesummary(){

        $var = $this->session->userdata;
        $user_id = $var['user']->id;
        $query = $this->db->query('select sales_income_increase from general_assumption where user_id="' . $user_id . '"');
        $Service_income = $query->row()->sales_income_increase;
        if ($Service_income === NULL) {
            $Service_income = 0;
        }
        // Currency
        $query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;

        $cur = '';

        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }

        $list = $this->people_model->get_datatables();

        $data = array();
        $no = (isset($_POST['start']))? $_POST['start']:'';
        $sumTotal = 0;
        foreach ($list as $people_basic) {
            $no++;
            $row = array();
            $row[] = '<img src=' . $people_basic->thumbnail . ' width="50" height="50">'; //$people_basic->ThumbNail;
            $row[] = $people_basic->id;
            $row[] = $people_basic->f_name;
            $row[] = $people_basic->l_name;

            $Weekly = ($people_basic->hour_worked * $people_basic->rates);
            $row[] = $cur . number_format($Weekly);

            $row[] = $cur . number_format(round((($Weekly * 52) / 12), 0)); // Monthly
            $row[] = $cur . number_format(($Weekly * 52) / 4); // Quaterly
            $row[] = $cur . number_format($Weekly * 52);    // Year1
            $row[] = number_format($Weekly * 52);   // Year2
            $row[] = number_format($Weekly * 52);   // Year3
            $sumTotal += (int) ($Weekly * 52);
            //add html for action

            $data[] = $row;
        }
        // echo $data[];
        $output = array(
            "draw" => (isset($_POST['draw']))? $_POST['draw']:'',
            "recordsTotal" => $this->people_model->count_all(),
            "recordsFiltered" => $this->people_model->count_filtered(),
            "data" => $data,
            "Service_income" => $Service_income,
            "sumTotal" => number_format($sumTotal),
            "currency" => $cur
        );
        //output to json format
        echo json_encode($output);
    }

}
