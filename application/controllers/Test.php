<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Test extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('Company_setup_model');
        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
   		}
        
    }

	public function index()
	{	
		
		
		$data['page'] = $this->uri->segment(1);
		$var = $this->session->userdata;
		//echo '<pre/>';print_r($var['user']->id);exit;
		$user_id = $var['user']->id;
		$data['monthly_expense'] = $this->db->get_where('monthly_expense_budget', array('user_id' => $user_id))->result_array();
		$data['one_time_cost'] = $this->db->get_where('one_time_cost', array('user_id' => $user_id))->result_array();
		$data['director'] = $this->db->get_where('director', array('user_id' => $user_id))->result_array();
		//echo '<pre/>';print_r($user_id);
		$this->load->template_left_nav('Test',$data);
	}
	public function get(){
		echo 'sdfsf';
	}
	
}
