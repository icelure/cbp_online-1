<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planning_setup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('Planning_setup_model');
        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
   		}

   		$this->load->model('profile_model');

    }

	public function index()

	{

		 $data=array();
		 $this->load->model('Company_setup_model');
		 $this->db->where(array('user_id'=>$this->session->userdata('user')->id));
		 $data['company_detail'] = $this->Company_setup_model->get_company_detail();

		 $data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
		 $data['page'] = $this->uri->segment(1);
   		 $data['t_mission']=$this->input->post('mission');
		 $data['t_vission']=$this->input->post('vision');
		 $data['t_cbackground']=$this->input->post('companygrnd');
		 $data['t_cownership']=$this->input->post('companyownr');
		 $data['t_goals']=$this->input->post('goals');
		 $data['t_propertyplan']=$this->input->post('proprty');
		 $data['t_product']=$this->input->post('prouct');
		 $data['t_services']=$this->input->post('services');
		 $data['t_market']=$this->input->post('market');
		 $data['t_potentional']=$this->input->post('potencial');
		 $data['t_strengts']=$this->input->post('strengths');
		 $data['t_weakness']=$this->input->post('weekness');
		 $data['t_daily']=$this->input->post('dailyopertion');
		 $data['t_facilities']=$this->input->post('facility');
		 $data['t_keypersonal']=$this->input->post('keypersonl');
		 $data['t_execuitve']=$this->input->post('execuitve');
		 $data['user_id']=$data['user']->id;

		if(isset($_POST['submitforms'])){
			$id=$data['user']->id;

			$removeKeys = array('user', 'page');

			foreach($removeKeys as $key) {
			   unset($data[$key]);
			}

			$this->setupCompany($data,$id);

			$data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
		 	$data['page'] = $this->uri->segment(1);

		}
	   $data['fxetch']=$this->Planning_setup_model->get_planning_detail($data['user_id']);

	   $this->load->template_left_nav('Planning_setup', $data);
		//$this->load->template_left_nav('Planning_setup',$data);

	}

	public function setupCompany($data,$id){

			$status=$this->Planning_setup_model->inser_update_planning_detail($data,$id);
			if($status){
			$message=$this->session->set_flashdata('success', 'Your information has been saved Successfully');
			//$this->load->view('Dashboard',$message);
			//$this->get_data($status);

			 //redirect('Dashboard');
			}
			else{

			$message=$this->session->set_flashdata('error', 'Your information has not been saved !');
			//$this->load->view('Dashboard',$message);
			redirect('Dashboard');
			}

	    }
	   public function get_data($status){

	    $data['fxetch']=$this->Planning_setup_model->get_planning_detail($status);


		$this->load->template_left_nav('Planning_setup', $data);




	   }

}
