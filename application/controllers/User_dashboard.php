<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();

        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
       	}else{
       		$curdate=strtotime(date('Y-m-d H:i:s'));
      		$plan_end_date = strtotime($this->session->userdata('user')->plan_end_date);

            if(($this->session->userdata('user')->subscribed == "n" || $curdate > $plan_end_date) && $this->session->userdata('user')->type == "u"){
              redirect(base_url().'plans');
            }
        }

        $this->load->model('profile_model');

    }

	public function index()
	{	$this->load->model('Dashboard_model');
		$data['page'] = $this->uri->segment(1);
		//$data['no_of_user'] = $this->Dashboard_model->no_of_user();
		//$data['no_of_payments'] = $this->Dashboard_model->no_of_payments();
		$this->load->model('Company_setup_model');
		$this->db->where(array('user_id'=>$this->session->userdata('user')->id));
		$data['company_detail'] = $this->Company_setup_model->get_company_detail();

		$data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
		$this->load->template_left_nav('User_dashboard',$data);
	}
}
