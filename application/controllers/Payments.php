<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {

	function __construct() {
        parent::__construct();
        
        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
   		}else{
            if($this->session->userdata('user')->type == "u"){
              redirect(base_url().'Dashboard');
            }
        }
        
    }

	public function index()
	{
		/*if(isset($_POST['registerForm']) && $_POST['registerForm']=="postForm"){
			$this->doRegister();
		}*/
		$this->load->model('Payment_model');
		$data['page'] = $this->uri->segment(1);
		$data['payments'] = $this->Payment_model->get_all_payments();
		$this->load->template_left_nav('Payments',$data);
	}
}
