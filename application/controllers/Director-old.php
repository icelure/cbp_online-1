<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Director extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('director_model','director');
	}

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('director_view');
	}

	public function ajax_list()
	{
		$list = $this->director->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $director) {
			$no++;
			$row = array();
			$row[] = $director->director;			
			$row[] = number_format($director->amount);

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_director('."'".$director->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_director('."'".$director->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->director->count_all(),
						"recordsFiltered" => $this->director->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->director->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		//echo '<pre/>';print_r($_POST);exit;
		$var = $this->session->userdata;		
		$user_id = $var['user']->id;	
		$director_name = $this->input->post('director');
		$amount_paid = $this->input->post('director_amount_paid');
		//echo $director;exit;
		$data = array(
				'user_id' => $user_id,
				'director' => $director_name,				
				'amount' => $amount_paid,				
			);
		$insert = $this->director->save($data);
		
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$var = $this->session->userdata;		
		$user_id = $var['user']->id;	
		$director_name = $this->input->post('director');
		$amount_paid = $this->input->post('director_amount_paid');
		//echo $director;exit;
		$data = array(
				'user_id' => $user_id,
				'director' => $director_name,				
				'amount' => $amount_paid,				
			);
		$this->director->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->director->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

}
