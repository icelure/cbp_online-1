<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Importedservice1 extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     * 	- or -
     * 		http://example.com/index.php/welcome/index
     * 	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct() {
        parent::__construct();
        $this->load->model('Importedservice_model', 'importedservice');
    }

    public function index() {
        // echo "test";
        //$data['page'] = $this->uri->segment(1);
        //     var_dump($data['page']);
        //   $this->load->template_top_nav('Products',$data);
    }



public function ajax_list() {
          $email = $this->session->userdata('user')->email;
        //$data = 'select services_income from gerneral_assumption where email="'.$email.'"';
        //$this->db->select('services_income');
        //$this->db->from('general_assumption');
        //$this->db->where('name', $name); 


$query = $this->db->query('select services_income from general_assumption where user_id=(select id from user where email="'.$email.'")');

 $Service_income= $query->row()->services_income;

// Currency

$query = $this->db->query('select currency from company_detail where user_id=(select id from user where email="'.$email.'")');

$currency = $query->row()->currency;

$cur = '';

if($currency == "AUD" || $currency == "USD"){
$cur = "$";
}
else if($currency == "EUR"){
$cur = "€";
}
else if($currency == "INR"){
$cur = "₹";
}

       $list = $this->importedservice->get_datatables();

        $data = array();
        $no = $_POST['start'];
       $sumTotal = 0;
        foreach ($list as $product) {
            $no++;
            // $Total = $product->Qty * $product->UnitCost;
            $row = array();

            $row[] = $product->ServiceID;
            $row[] = $product->Contractors;
            $row[] = $product->Description;
            //$row[] = number_format($product->HoursWorked);
            //$row[] = $product->RatePerHour;
             //$row[] = $product->CallOutFee;
             //$row[] = $product->SubTotal;
             //$row[] = $product->VAT_GST;
            //$row[] = $product->(HoursWorked * RatePerHour)+CallOutFee->Total
                 
             $row[] =$cur.number_format($product->Total);
             
     
             $row[] = $cur.number_format(round((($product->Total * 52) / 12),0));
             $row[] = $cur.number_format(($product->Total * 52) / 4);
             $row[] = $cur.number_format($product->Total * 52);
$row[] = number_format($product->Total * 52);
$row[] = number_format($product->Total * 52);
$sumTotal += (int)($product->Total * 52);
            //add html for action


            $data[] = $row;
        }
        // echo $data[];
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->importedservice->count_all(),
            "recordsFiltered" => $this->importedservice->count_filtered(),
            "data" => $data,
"Service_income" => $Service_income,
"sumTotal" => number_format($sumTotal), 
"currency"=>$cur

        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_editimported($id) {
        $data = $this->importedservice->get_by_id($id);

        echo json_encode($data);
    }

    public function ajax_get_imported_product($id) {

        $user_id = $this->session->userdata('user')->id;
        $imp_product_cost = $this->importedservice->get_import_cost($userid);
        $imp_prod_basic = $this->importedservice->get_by_id($id);


        // imported product basic information
        //var $ThumbNail, $Description, $Qty, $UnitCost, $TotalCost, $MarkUpOnCost, $ExchangeRate;
        //  imported product calculated information
        //var $import_duty, $delivery_order, $cmr_comp_fee, $lcl_transport_fee, $cargo_auto_fee, $port_service_fee,
        //$custom_clearance_fee, $transport_fues_fee, $aqis_fee, $insurance_fee, $dec_processing_fee, $misc_fee,
        //$other_fee_1, $other_fee_2, $other_fee_3, $MarkupOnCost;


        $data['ThumbNail'] = $imp_prod_basic->ThumbNail;
        $data['Description'] = $imp_prod_basic->Description;
        $data['Qty'] = $imp_prod_basic->Qty;
        $data['UnitCost'] = $imp_prod_basic->UnitCost;
        $data['TotalCost'] = $imp_prod_basic->Qty * $imp_prod_basic->UnitCost;
        $data['ExchangeRate'] = $imp_prod_basic->ExchangeRate;
        $data['TotalLandedCost'] = $imp_prod_basic->ExchangeRate *  $imp_prod_basic->Qty * $imp_prod_basic->UnitCost  ;
        $data['import_duty'] = ($imp_product_cost->import_duty / 100) ;//* ( $data['TotalLandedCost'] );
        $data['delivery_order'] = ($imp_product_cost->delivery_order / 100) ;//* ( $data['TotalLandedCost'] );
        $data['cmr_comp_fee'] = ($imp_product_cost->cmr_comp_fee / 100) ;//* ( $data['TotalLandedCost'] );
        $data['lcl_transport_fee'] = ($imp_product_cost->lcl_transport_fee / 100);// * ( $data['TotalLandedCost'] );
        $data['cargo_auto_fee'] = ($imp_product_cost->cargo_auto_fee / 100) ;//* ( $data['TotalLandedCost'] );
        $data['port_service_fee'] = ($imp_product_cost->port_service_fee / 100);// * ( $data['TotalLandedCost'] );
        $data['custom_clearance_fee'] = ($imp_product_cost->custom_clearance_fee / 100);// * ( $data['TotalLandedCost'] );
        $data['transport_fues_fee'] = ($imp_product_cost->transport_fues_fee / 100) ;//* ( $data['TotalLandedCost'] );
        $data['aqis_fee'] = ($imp_product_cost->aqis_fee / 100) ;//* ( $data['TotalLandedCost'] );
        $data['insurance_fee'] = ($imp_product_cost->insurance_fee / 100) ;//* ( $data['TotalLandedCost'] );
        $data['dec_processing_fee'] = ($imp_product_cost->dec_processing_fee / 100) ;//* ( $data['TotalLandedCost'] );
        $data['misc_fee'] = ($imp_product_cost->misc_fee / 100);// * ( $data['TotalLandedCost'] );
        $data['other_fee_1'] = ($imp_product_cost->other_fee_1 / 100);// * ( $data['TotalLandedCost'] );
        $data['other_fee_2'] = ($imp_product_cost->other_fee_2 / 100);// * ( $data['TotalLandedCost'] );
        $data['other_fee_3'] = ($imp_product_cost->other_fee_3 / 100);// * ( $data['TotalLandedCost'] );
        $data['TotalProductCost'] = 0;
//                $data['TotalLandedCost'] + $data['import_duty'] + $data['delivery_order'] + $data['cmr_comp_fee'] + $data['lcl_transport_fee'] +
//                $data['cargo_auto_fee'] + $data['port_service_fee'] + $data['custom_clearance_fee'] + $data['transport_fues_fee'] + $data['aqis_fee'] +
//                $data['insurance_fee'] + $data['dec_processing_fee'] + $data['misc_fee'] + $data['other_fee_1'] + $data['other_fee_2'] + $data['other_fee_3'];
        $data['AvgUnitCost'] = 0;//$data['TotalProductCost']/ $data['Qty'];
        $data['MarkUpOnCost'] = ( $imp_prod_basic->MarkUpOnCost / 100 ) ;//* $data['AvgUnitCost'] ;
        $data['WholeSalePrice'] = 0; //$data['AvgUnitCost'] + $data['MarkUpOnCost'];
        $data['GrossProfit'] = 0;//$data['WholeSalePrice'] - $data['AvgUnitCost'];
        $data['TotalRevenue'] = 0;//$data['WholeSalePrice'] * $data['Qty'];
        $data['TotalCost'] = 0;//$data['AvgUnitCost'] * $data['Qty'] ;
        $data['GrossProfit'] = $data['TotalRevenue'] - $data['TotalCost'];
        
        echo json_encode($data);
    }

    public function ajax_addimported() {
        $var = $this->session->userdata;
        $user_id = $var['user']->id;

        $maxid = 0;
        //SELECT id FROM localproduct ORDER BY id DESC LIMIT 1
        $row = $this->db->query('SELECT MAX(ServiceID) AS `maxid` FROM `service_income`')->row();
        if ($row) {
            $maxid = $row->maxid + 1;
        }

        $data = array(
            //'Description' => $user_id,
            'ServiceID' => $this->input->post('impser_serid'),
            'Contractors' => $this->input->post('impser_contractors'),
            'Description' => $this->input->post('impser_desc'),
            'HoursWorked' => $this->input->post('impser_hrswork'),
            'RatePerHour' => $this->input->post('impser_rateperhr'),
            'CallOutFee' => $this->input->post('impser_callourfee'),
            'SubTotal' => '',
            'VAT_GST' => '',
            //'Total' => $this->input->post('impser_total')
                'Total' => ($this->input->post('impser_hrswork') * $this->input->post('impser_rateperhr') ) + $this->input->post('impser_callourfee')
        );
        $insert = $this->importedservice->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_updateimported() {

        $id = $this->input->post('importedid');
        

        $data = array(
           'ServiceID' => $this->input->post('impser_serid'),
            'Contractors' => $this->input->post('impser_contractors'),
            'Description' => $this->input->post('impser_desc'),
            'HoursWorked' => $this->input->post('impser_hrswork'),
            'RatePerHour' => $this->input->post('impser_rateperhr'),
            'CallOutFee' => $this->input->post('impser_callourfee'),
            'Total' => ($this->input->post('impser_hrswork') * $this->input->post('impser_rateperhr') ) + $this->input->post('impser_callourfee')
        );
        $this->importedservice->update(array('ServiceID' => $this->input->post('impser_serid')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_deleteimported($id) {
        $this->importedservice->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

  

}
