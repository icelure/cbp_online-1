<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Person extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('person_model','person');
	}

	public function index()
	{
		$this->load->helper('url');
		$this->load->view('person_view');
	}

	public function ajax_list()
	{
		$list = $this->person->get_datatables();
		$var = $this->session->userdata;
		$user_id = $var['user']->id;

		$query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;

        $cur = '';

        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $person) {
			$no++;
			$row = array();
			
			$row[] = $person->cost_description;
			$row[] = $person->months;
			$row[] = $cur.number_format($person->monthly_cost);
			$row[] = $cur.number_format($person->one_time_cost);
			$row[] = $person->vat_gst .'%';
			$row[] = $cur.number_format($person->total_vat_gst);
			$row[] = $cur.number_format($person->total_cost);

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$person->id."'".')"><i class="glyphicon glyphicon-pencil"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$person->id."'".')"><i class="glyphicon glyphicon-trash"></i></a>';
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->person->count_all(),
			"recordsFiltered" => $this->person->count_filtered(),
			"data" => $data,
			"currency"=>$cur,
		);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->person->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$var = $this->session->userdata;		
		$user_id = $var['user']->id;	
		$cost = $this->input->post('monthly_cost');
		$month = $this->input->post('no_months');
		$one_time_cost = $cost*$month;
		$vat_gst = 10;
		$total_vat_gst = (10*$one_time_cost)/100;
		$total_cost = $one_time_cost + $total_vat_gst;
		$data = array(
				'user_id' => $user_id,
				'cost_description' => $this->input->post('cost_item_description'),
				'months' => $this->input->post('no_months'),
				'monthly_cost' => $this->input->post('monthly_cost'),
				'one_time_cost' => $one_time_cost,
				'vat_gst' => $vat_gst,
				'total_vat_gst' => $total_vat_gst,
				'total_cost' => $total_cost,
			);
		$insert = $this->person->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$var = $this->session->userdata;		
		$user_id = $var['user']->id;	
		$cost = $this->input->post('monthly_cost');
		$month = $this->input->post('no_months');
		$one_time_cost = $cost*$month;
		$vat_gst = 10;
		$total_vat_gst = (10*$one_time_cost)/100;
		$total_cost = $one_time_cost + $total_vat_gst;
		$data = array(
				'user_id' => $user_id,
				'cost_description' => $this->input->post('cost_item_description'),
				'months' => $this->input->post('no_months'),
				'monthly_cost' => $this->input->post('monthly_cost'),
				'one_time_cost' => $one_time_cost,
				'vat_gst' => $vat_gst,
				'total_vat_gst' => $total_vat_gst,
				'total_cost' => $total_cost,
			);		$this->person->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->person->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

}
