<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Company_setup extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        $this->load->model('Company_setup_model');
        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
   		}

   		$this->load->model('profile_model');
        
    }

	public function index()
	{	
		if(isset($_POST['submtForm']) && $_POST['submtForm']=='cmpnySettings'){
			$this->setupCompany();
		}

		$data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
		
		$data['page'] = $this->uri->segment(1);
		$data['company_detail'] = $this->Company_setup_model->get_company_detail();
		$data['directors'] = $this->Company_setup_model->get_directors($data['company_detail']['id']);
		$data['general_assumptions'] = $this->Company_setup_model->get_general_assumptions();
		$data['import_duty_assumptions'] = $this->Company_setup_model->get_import_duty_assumptions();
		$data['australian_payroll'] = $this->Company_setup_model->get_australian_payroll();
		unset($data['general_assumptions']['id']);
		unset($data['general_assumptions']['company_id']);
		unset($data['general_assumptions']['user_id']);
		unset($data['import_duty_assumptions']['id']);
		unset($data['import_duty_assumptions']['company_id']);
		unset($data['import_duty_assumptions']['user_id']);
		unset($data['australian_payroll']['id']);
		unset($data['australian_payroll']['company_id']);
		unset($data['australian_payroll']['user_id']);
		
		$this->load->template_left_nav('Company_setup',$data);
	}

	public function setupCompany(){
		$company_id = (int)$this->input->post('company_id');
		$type = 'update';
		if($company_id == 0){
			$type = 'insert';
		}
		$user_id = $this->session->userdata('user')->id;
		
		extract($this->input->post());


		
		$Company_setup = array(
			"user_id" => $user_id,
			"company_name" => $company_name,
			"start_date" => date("Y-m-d", strtotime($start_date)),
			"abn_no" => $abn_no,
			"street_no" => $street_no,
			"street_name" => $street_name,
			"suburb" => $suburb,
			"state" => $state,
			"zipcode" => $zipcode,
			"country" => $country,
			"telephone" => $telephone,
			"fax" => $fax,
			"company_email" => $company_email,
			"website" => $website,
			"currency" => $currency,
			"financial_year" => $financial_year
		);

		
		$company_id	= $this->Company_setup_model->inser_update_company_detail($Company_setup,$company_id);

		if(isset($_FILES['company_logo']) && $_FILES['company_logo']['name'] !== ""){
			$response = $this->do_upload('company_logo');
			if($response !== false){
				$logo = array(
					"company_logo" => $response['upload_data']['file_name']
				);
				$this->db->where('id',$company_id);
				$this->db->update('company_detail',$logo);
			}
		}

		$files = $_FILES;
	    
	    $cpt = count($_FILES['director_logo']['name']);

	    if($cpt > 0){
		    $directors=array();
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES['director_logo']['name']= $files['director_logo']['name'][$i];
		        $_FILES['director_logo']['type']= $files['director_logo']['type'][$i];
		        $_FILES['director_logo']['tmp_name']= $files['director_logo']['tmp_name'][$i];
		        $_FILES['director_logo']['error']= $files['director_logo']['error'][$i];
		        $_FILES['director_logo']['size']= $files['director_logo']['size'][$i];    

		        $response = $this->do_upload('director_logo');
		        
				if($response !== false){
					$directors[] = array(
						"company_id" => $company_id,
						"name" => $this->input->post('director_name['.$i.']'),
						"image" => $response['upload_data']['file_name']
					);
				}
		    }
		    $this->db->where('company_id', $company_id);
      		if($this->db->delete('directors')){
      			$this->db->insert_batch('directors', $directors); 
      		}
	    }
	   
	
		$General_assumptions = array(
			"company_id" => $company_id,
			"user_id" => $user_id,
			"company_tax" => $company_tax,
			"company_vat" => $company_vat,
			"rsvbl_in_days" => $rsvbl_in_days,
			"pybl_in_days" => $pybl_in_days,
			"vat_paid_in_days" => $vat_paid_in_days,
			"cmpnytx_paid_in_days" => $cmpnytx_paid_in_days,
			"opening_cash_balance" => $opening_cash_balance,
			"opening_debtors_balance" => $opening_debtors_balance,
			"closing_creditors_balance" => $closing_creditors_balance,
			"sales_income_increase" => $sales_income_increase,
			"services_income" => $services_income,
			"sales_cost_increase" => $sales_cost_increase,
			"service_cost_increase" => $service_cost_increase,
			"marketing_increase" => $marketing_increase,
			"public_reactions" => $public_reactions,
			"administration_cost" => $administration_cost,
			"other_increse" => $other_increse,
			"depreciation_on_equipment" => $depreciation_on_equipment
		);

		$this->Company_setup_model->inser_update_general_assumptions($General_assumptions,$type);

		$Import_duty_assumptions = array(
			"company_id" => $company_id,
			"user_id" => $user_id,
			"r1_imd" => $r1_imd,
			"import_duty" => $import_duty,
			"r1_do" => $r1_do,
			"delivery_order" => $delivery_order,
			"r1_cmcf" => $r1_cmcf,
			"cmr_comp_fee" => $cmr_comp_fee,
			"r1_ltf" => $r1_ltf,
			"lcl_transport_fee" => $lcl_transport_fee,
			"r1_caf" => $r1_caf,
			"cargo_auto_fee" => $cargo_auto_fee,
			"r1_psf" => $r1_psf,
			"port_service_fee" => $port_service_fee,
			"r1_ccf" => $r1_ccf,
			"custom_clearance_fee" => $custom_clearance_fee,
			"r1_tff" => $r1_tff,
			"transport_fues_fee" => $transport_fues_fee,
			"r1_aqis" => $r1_aqis,
			"aqis_fee" => $aqis_fee,
			"r1_if" => $r1_if,
			"insurance_fee" => $insurance_fee,
			"r1_dpf" => $r1_dpf,
			"dec_processing_fee" => $dec_processing_fee,
			"r1_mf" => $r1_mf,
			"misc_fee" => $misc_fee,
			"r1_of1" => $r1_of1,
			"other_fee_1" => $other_fee_1,
			"r1_of2" => $r1_of2,
			"other_fee_2" => $other_fee_2,
			"r1_of3" => $r1_of3,
			"other_fee_3" => $other_fee_3
		);

		$this->Company_setup_model->inser_update_import_duty_assumptions($Import_duty_assumptions,$type);

		$Australian_payroll = array(
			"company_id" => $company_id,
			"user_id" => $user_id,
			"r1_pgt" => $r1_pgt,
			"payg_tax" => $payg_tax,
			"r1_s" => $r1_s,
			"superannuation" => $superannuation,
			"r1_wc" => $r1_wc,
			"work_cover" => $work_cover,
			"r1_uf" => $r1_uf,
			"union_fee" => $union_fee,
			"r1_hp" => $r1_hp,
			"holiday_pay" => $holiday_pay,
			"r1_sl" => $r1_sl,
			"sick_leave" => $sick_leave,
			"r1_lsf" => $r1_lsf,
			"long_service_fee" => $long_service_fee,
			"r1_ot1" => $r1_ot1,
			"other_ap1" => $other_ap1,
			"r1_ot2" => $r1_ot2,
			"other_ap2" => $other_ap2,
			"r1_ot3" => $r1_ot3,
			"other_ap3" => $other_ap3,
			"r1_pri" => $r1_pri,
			"payrate_increase" => $payrate_increase			
		);

		$this->Company_setup_model->inser_update_aus_payroll_assumptions($Australian_payroll,$type);
	}

	public function do_upload($file_name)
    {


        	$config['upload_path']     = $this->config->item('base_upload_path').'company_logo/';
	        $config['allowed_types']        = 'gif|jpg|png';
	        $config['max_size']             = 1000;
	        $config['max_width']            = 3068;
	        $config['max_height']           = 2014;

	        $this->load->library('upload', $config);

	        if (!$this->upload->do_upload($file_name))
	        {
	               $error = array('error' => $this->upload->display_errors());

	                //$this->load->view('upload_form', $error);
	        }
	        else
	        {
	                $data = array('upload_data' => $this->upload->data());
	                return $data;
	                //$this->load->view('upload_success', $data);
	        }
    }
}
