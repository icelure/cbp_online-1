<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ImportedProducts extends CI_Controller {

    public function __construct() {
        parent::__construct();
        if (!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url() . 'login');
        }
        $this->load->model('ImportedProduct_model', 'importedproduct');
         $this->load->model('profile_model');
    }

    public function index() {

        $data['page'] = $this->uri->segment(1);
        $this->load->model('Company_setup_model');
        $this->db->where(array('user_id'=>$this->session->userdata('user')->id));
        $data['company_detail'] = $this->Company_setup_model->get_company_detail();

        $data['user'] = $this->profile_model->get_detail_by_id($this->session->userdata('user')->id);
        //     var_dump($data['page']);
        $query = $this->db->query('select currency from company_detail where user_id="' . $data['user']->id . '"');
        $data['currency'] = $query->row()->currency;

        $this->load->template_left_nav('ImportedProducts', $data);
    }

    public function ajax_list() {

        $list = $this->importedproduct->get_datatables();


        $var = $this->session->userdata;
        $user_id = $var['user']->id;
        $query = $this->db->query('select sales_income_increase from general_assumption where user_id="' . $user_id . '"');
        $Service_income = $query->row()->sales_income_increase;
        if ($Service_income === NULL) {
            $Service_income = 0;
        }
        // Currency
        $query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;

        $cur = '';

        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }

        $data = array();
        $no = isset($_POST['start']);
        foreach ($list as $product) {
            $no++;
            //$Total = $product->Qty * $product->UnitCost;
            $row = array();
            $row["importedproductid"] = $product->id;
            $row["importedproductthumbnail"] = $product->ThumbNail; //'<img src=' . $product->ThumbNail . ' width="50" height="50">';
            $row["importedproductdescription"] = $product->Description; //'<a onclick="loaddetail(' . $product->id . ')">' . $product->Description . '</a>';
            $row["qty"] = $product->Qty;
            $row["unitcost"] = $cur . $product->UnitCost;
            $row["totalcost"] = $cur . $product->Qty * $product->UnitCost;
            ;
            $row["exchangerate"] = $product->ExchangeRate;
            //$TotalLandedCost = $product->ExchangeRate *$product->Qty * $product->UnitCost;
            $row["totallandedcost"] = $cur . ($product->ExchangeRate * $product->Qty * $product->UnitCost); //$TotalLandedCost;

            $data[] = $row;
        }
        // echo $data[];
        $output = array(
            "draw" => isset($_POST['draw']),
            "recordsTotal" => $this->importedproduct->count_all(),
            "recordsFiltered" => $this->importedproduct->count_filtered(),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function importedproductsummary() {

        $var = $this->session->userdata;
        $user_id = $var['user']->id;
        $query = $this->db->query('select sales_income_increase from general_assumption where user_id="' . $user_id . '"');
        $Service_income = $query->row()->sales_income_increase;
        if ($Service_income === NULL) {
            $Service_income = 0;
        }
        // Currency
        $query = $this->db->query('select currency from company_detail where user_id="' . $user_id . '"');
        $currency = $query->row()->currency;

        $cur = '';

        if ($currency == "AUD" || $currency == "USD") {
            $cur = "$";
        } else if ($currency == "EUR") {
            $cur = "€";
        } else if ($currency == "INR") {
            $cur = "₹";
        }

        $list = $this->importedproduct->get_datatables();

        $data = array();
        $no = isset($_POST['start']);
        $sumTotal = 0;
        foreach ($list as $product) {
            $no++;
            $row = array();
            $row[] = '<img src=' . $product->ThumbNail . ' width="50" height="50" class="img-circle">'; //$product->ThumbNail;
            $row[] = $product->id;
            $row[] = $product->Description;
            $row[] = $product->Qty;
            $Weekly = ($product->ExchangeRate * $product->Qty * $product->UnitCost);
            $row[] = $cur . number_format($Weekly);

            $row[] = $cur . number_format(round((($Weekly * 52) / 12), 0)); // Monthly
            $row[] = $cur . number_format(($Weekly * 52) / 4); // Quaterly
            $row[] = $cur . number_format($Weekly * 52);    // Year1
            $row[] = number_format($Weekly * 52);   // Year2
            $row[] = number_format($Weekly * 52);   // Year3
            $sumTotal += (int) ($Weekly * 52);
            //add html for action

            $data[] = $row;
        }
        // echo $data[];
        $output = array(
            "draw" => isset($_POST['draw']),
            "recordsTotal" => $this->importedproduct->count_all(),
            "recordsFiltered" => $this->importedproduct->count_filtered(),
            "data" => $data,
            "Service_income" => $Service_income,
            "sumTotal" => number_format($sumTotal),
            "currency" => $cur
        );
        //output to json format
        echo json_encode($output);
    }
    public function update_sales_income_increase() {
        $sincome = $_POST['si'];
        //$email = $this->session->userdata('user')->email;
        $var = $this->session->userdata;
        $id = $var['user']->id;
        $query1 = $this->db->query('update general_assumption set sales_income_increase="' . $sincome . '" where user_id="' . $id . '"');
        if (!$query1['error']) {
            $result = array('status' => 'yes');
            echo json_encode($result, TRUE);
        } else {
            $result = array('status' => 'no');
            echo json_encode($result, TRUE);
        }
    }
    public function ajax_editimported($id) {
        $data = $this->importedproduct->get_by_id($id);

        echo json_encode($data);
    }

    public function ajax_get_imported_product($id) {

        $user_id = $this->session->userdata('user')->id;
        //$imp_product_cost = $this->importedproduct->get_import_cost($user_id);
        $imp_prod_basic = $this->importedproduct->get_by_id($id);


        $data['user_id'] = $user_id;
        $data['ThumbNail'] = $imp_prod_basic->ThumbNail;
        $data['Description'] = $imp_prod_basic->Description;
        $data['Qty'] = $imp_prod_basic->Qty;
        $data['UnitCost'] = $imp_prod_basic->UnitCost;
        $TotalCost = $imp_prod_basic->Qty * $imp_prod_basic->UnitCost;
        $data['TotalCost'] = $imp_prod_basic->Qty * $imp_prod_basic->UnitCost; //$TotalCost;
        $data['ExchangeRate'] = $imp_prod_basic->ExchangeRate;

        $TotalLandedCost = $imp_prod_basic->ExchangeRate * $TotalCost;
        $data['TotalLandedCost'] = $TotalLandedCost; //$imp_prod_basic->Qty * $imp_prod_basic->UnitCost;
        $data['import_duty'] = ($imp_prod_basic->import_duty / 100); //* ( $data['TotalLandedCost'] );
        $data['delivery_order'] = ($imp_prod_basic->delivery_order / 100); //* ( $data['TotalLandedCost'] );
        $data['cmr_comp_fee'] = ($imp_prod_basic->comp_fee / 100); //* ( $data['TotalLandedCost'] );
        $data['lcl_transport_fee'] = ($imp_prod_basic->lcl_transport_fee / 100); // * ( $data['TotalLandedCost'] );
        $data['cargo_auto_fee'] = ($imp_prod_basic->cargo_auto / 100); //* ( $data['TotalLandedCost'] );
        $data['port_service_fee'] = ($imp_prod_basic->port_service_fee / 100); // * ( $data['TotalLandedCost'] );
        $data['custom_clearance_fee'] = ($imp_prod_basic->custom_clearance_fee / 100); // * ( $data['TotalLandedCost'] );
        $data['transport_fues_fee'] = ($imp_prod_basic->transport_fues_fee / 100); //* ( $data['TotalLandedCost'] );
        $data['aqis_fee'] = ($imp_prod_basic->aqis_fee / 100); //* ( $data['TotalLandedCost'] );
        $data['insurance_fee'] = ($imp_prod_basic->insurance_fee / 100); //* ( $data['TotalLandedCost'] );
        $data['dec_processing_fee'] = ($imp_prod_basic->dec_processing_fee / 100); //* ( $data['TotalLandedCost'] );
        $data['misc_fee'] = ($imp_prod_basic->misc_fee / 100); // * ( $data['TotalLandedCost'] );
        $data['other_fee_1'] = ($imp_prod_basic->other_fee_1 / 100); // * ( $data['TotalLandedCost'] );
        $data['other_fee_2'] = ($imp_prod_basic->other_fee_2 / 100); // * ( $data['TotalLandedCost'] );
        $data['other_fee_3'] = ($imp_prod_basic->other_fee_3 / 100); // * ( $data['TotalLandedCost'] );
        $data['TotalProductCost'] = 0;
//                $data['TotalLandedCost'] + $data['import_duty'] + $data['delivery_order'] + $data['cmr_comp_fee'] + $data['lcl_transport_fee'] +
//                $data['cargo_auto_fee'] + $data['port_service_fee'] + $data['custom_clearance_fee'] + $data['transport_fues_fee'] + $data['aqis_fee'] +
//                $data['insurance_fee'] + $data['dec_processing_fee'] + $data['misc_fee'] + $data['other_fee_1'] + $data['other_fee_2'] + $data['other_fee_3'];
        $data['AvgUnitCost'] = 0; //$data['TotalProductCost']/ $data['Qty'];
        $data['MarkUpOnCost'] = ( $imp_prod_basic->MarkUpOnCost / 100 ); //* $data['AvgUnitCost'] ;
        $data['WholeSalePrice'] = 0; //$data['AvgUnitCost'] + $data['MarkUpOnCost'];
        $data['GrossProfit'] = 0; //$data['WholeSalePrice'] - $data['AvgUnitCost'];
        $data['TotalRevenue'] = 0; //$data['WholeSalePrice'] * $data['Qty'];
        $data['TotalCost'] = 0; //$data['AvgUnitCost'] * $data['Qty'] ;
        $data['GrossProfit'] = $data['TotalRevenue'] - $data['TotalCost'];

        echo json_encode($data);
    }

    public function ajax_addimported() {
        $var = $this->session->userdata;
        $user_id = $var['user']->id;
        $ThumbNail;
        $maxid = 0;
        //SELECT id FROM localproduct ORDER BY id DESC LIMIT 1
        $row = $this->db->query('SELECT MAX(id) AS `maxid` FROM `importedproduct`')->row();
        if ($row) {
            $maxid = $row->maxid + 1;
        }
        if (!($_FILES["importedflnFile"]["name"] == NULL)) {
            $target_dir = "imported_product_thumbnails/";
            if (!file_exists($target_dir)) {
                mkdir($target_dir);
            }
            $target_file = $target_dir . $user_id . "_" . $maxid . "_" . basename($_FILES["importedflnFile"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["importedflnFile"]["tmp_name"]);
            if ($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                //echo "File is not an image.";
                $uploadOk = 0;
            }
            if ($_FILES["importedflnFile"]["size"] > 500000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            } else {
                if (move_uploaded_file($_FILES["importedflnFile"]["tmp_name"], $target_file)) {
                    //echo "The file " . basename($_FILES["flnFile"]["name"]) . " has been uploaded.";
                } else {
                    //echo "Sorry, there was an error uploading your file.";
                }
                $ThumbNail = $target_file;
            }
        } else {
            $ThumbNail = 'assets/images/no_image.png';
        }

        $data = array(
            //'Description' => $user_id,
            'Description' => $this->input->post('importedproduct_description'),
            'Qty' => $this->input->post('ImportQuantity'),
            'UnitCost' => $this->input->post('imported_unit_cost'),
            'ExchangeRate' => $this->input->post('importedexchange_range'),
            'MarkUpOnCost' => $this->input->post('imported_markup_on_cost'),
            'ThumbNail' => $ThumbNail,
            'import_duty'=>$this->input->post('import_duty'),
            'comp_fee'=>$this->input->post('comp_fee'),
            'cargo_auto'=>$this->input->post('cargo_auto'),
            'custom_clearance_fee'=>$this->input->post('custom_clearance_fee'),
            'aqis_fee'=>$this->input->post('aqis_fee'),
            'dec_processing_fee'=>$this->input->post('dec_processing_fee'),
            'delivery_order'=>$this->input->post('delivery_order'),
            'lcl_transport_fee'=>$this->input->post('lcl_transport_fee'),
            'port_service_fee'=>$this->input->post('port_service_fee'),
            'transport_fues_fee'=>$this->input->post('transport_fues_fee'),
            'insurance_fee'=>$this->input->post('insurance_fee'),
            'misc_fee'=>$this->input->post('misc_fee'),
            'other_fee_1'=>$this->input->post('other_fee_1'),
            'other_fee_2'=>$this->input->post('other_fee_2'),
            'other_fee_3'=>$this->input->post('other_fee_3'),
        );
        $insert = $this->importedproduct->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function ajax_updateimported() {

        //$id = $this->input->post('importedid');
        $id = 41;
        $ThumbnailPath = $this->db->query('SELECT Thumbnail  FROM `importedproduct` where id = ' . $id)->row();
        $data;

        if (!($_FILES["importedflnFile"]["name"] == NULL)) {
            $var = $this->session->userdata;
            $user_id = $var['user']->id;

            $target_dir = "imported_product_thumbnails/";
            if (!file_exists($target_dir)) {
                mkdir($target_dir);
            }
            $target_file = $target_dir . $user_id . "_" . $id . "_" . basename($_FILES["importedflnFile"]["name"]);
            $uploadOk = 1;
            $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
            $check = getimagesize($_FILES["importedflnFile"]["tmp_name"]);
            if ($check !== false) {
                //echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                //echo "File is not an image.";
                $uploadOk = 0;
            }
            if ($_FILES["importedflnFile"]["size"] > 500000) {
                //echo "Sorry, your file is too large.";
                $uploadOk = 0;
            }
            if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
                //echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
                $uploadOk = 0;
            }

            if ($uploadOk == 0) {
                //echo "Sorry, your file was not uploaded.";
            } else {

                if ($ThumbnailPath->Thumbnail != "assets/images/no_image.png"){
                    unlink($ThumbnailPath->Thumbnail);
                }

                move_uploaded_file($_FILES["importedflnFile"]["tmp_name"], $target_file);
            }
            $data = array(
                'Description' => $this->input->post('importedproduct_description'),
                'Qty' => $this->input->post('ImportQuantity'),
                'UnitCost' => $this->input->post('imported_unit_cost'),
                'ExchangeRate' => $this->input->post('importedexchange_range'),
                'MarkUpOnCost' => $this->input->post('imported_markup_on_cost'),
                'ThumbNail' => $target_file,
                'import_duty'=>$this->input->post('import_duty'),
                'comp_fee'=>$this->input->post('comp_fee'),
                'cargo_auto'=>$this->input->post('cargo_auto'),
                'custom_clearance_fee'=>$this->input->post('custom_clearance_fee'),
                'aqis_fee'=>$this->input->post('aqis_fee'),
                'dec_processing_fee'=>$this->input->post('dec_processing_fee'),
                'delivery_order'=>$this->input->post('delivery_order'),
                'lcl_transport_fee'=>$this->input->post('lcl_transport_fee'),
                'port_service_fee'=>$this->input->post('port_service_fee'),
                'transport_fues_fee'=>$this->input->post('transport_fues_fee'),
                'insurance_fee'=>$this->input->post('insurance_fee'),
                'misc_fee'=>$this->input->post('misc_fee'),
                'other_fee_1'=>$this->input->post('other_fee_1'),
                'other_fee_2'=>$this->input->post('other_fee_2'),
                'other_fee_3'=>$this->input->post('other_fee_3'),
            );
        } else {

            $data = array(
                'Description' => $this->input->post('importedproduct_description'),
                'Qty' => $this->input->post('ImportQuantity'),
                'UnitCost' => $this->input->post('imported_unit_cost'),
                'ExchangeRate' => $this->input->post('importedexchange_range'),
                'MarkUpOnCost' => $this->input->post('imported_markup_on_cost'),
                'import_duty'=>$this->input->post('import_duty'),
                'comp_fee'=>$this->input->post('comp_fee'),
                'cargo_auto'=>$this->input->post('cargo_auto'),
                'custom_clearance_fee'=>$this->input->post('custom_clearance_fee'),
                'aqis_fee'=>$this->input->post('aqis_fee'),
                'dec_processing_fee'=>$this->input->post('dec_processing_fee'),
                'delivery_order'=>$this->input->post('delivery_order'),
                'lcl_transport_fee'=>$this->input->post('lcl_transport_fee'),
                'port_service_fee'=>$this->input->post('port_service_fee'),
                'transport_fues_fee'=>$this->input->post('transport_fues_fee'),
                'insurance_fee'=>$this->input->post('insurance_fee'),
                'misc_fee'=>$this->input->post('misc_fee'),
                'other_fee_1'=>$this->input->post('other_fee_1'),
                'other_fee_2'=>$this->input->post('other_fee_2'),
                'other_fee_3'=>$this->input->post('other_fee_3'),
            );
        }

        $this->importedproduct->update(array('id' => $this->input->post('importedid')), $data);

        echo json_encode(array("status" => TRUE));
    }

    public function ajax_deleteimported($id) {

        $ThumbnailPath = $this->db->query('SELECT Thumbnail  FROM `importedproduct` where id = ' . $id)->row();
        if ($ThumbnailPath->Thumbnail != "assets/images/no_image.png"){
            unlink($ThumbnailPath->Thumbnail);
        }

        $this->importedproduct->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }

}
