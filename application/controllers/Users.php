<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct() {
        parent::__construct();
        
        if(!isset($this->session->userdata('user')->logged_in) || $this->session->userdata('user')->logged_in !== true) {
            redirect(base_url().'login');
   		}else{
            if($this->session->userdata('user')->type == "u"){
              redirect(base_url().'Dashboard');
            }
        }
        
    }

	public function index()
	{
		/*if(isset($_POST['registerForm']) && $_POST['registerForm']=="postForm"){
			$this->doRegister();
		}*/
		$this->load->model('User_model');
		$data['page'] = $this->uri->segment(1);
		$data['users'] = $this->User_model->get_all_users();
		$this->load->template_left_nav('Users',$data);
	}

	function chng_status(){
		$user_id=$this->input->post('uid');
		$status=$this->input->post('status');
		if($user_id > 0 && $status !='')
		{
			$data = array(
               'isActive'=>$status
            );

			$this->db->where('id', $user_id);
			$this->db->update('user', $data);
		}	
	}
}
